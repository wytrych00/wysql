package org.wytrych.WySQL.db;

import org.wytrych.QueryResult.QueryResult;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.EqualsExpression;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.WhereExpression;
import org.wytrych.WySQL.SQLParser.WhereParser.values.ColumnValue;
import org.wytrych.WySQL.SQLParser.WhereParser.values.DatabaseValue;
import org.wytrych.WySQL.SQLParser.WhereParser.values.NumberValue;
import org.wytrych.WySQL.db.table.Table;
import org.wytrych.WySQL.db.table.TableException;
import org.wytrych.WySQL.db.table.TableIndexerFactory;
import org.wytrych.WySQL.shared.Column;
import org.wytrych.WySQL.shared.TableWithAssignedColumns;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Database implements IDatabase, Serializable {
    private static final long serialVersionUID = 1;
    private HashMap<String, Table> tables = new HashMap<>();
    private static final String OK = "ok";
    private final TableIndexerFactory indexerFactory;

    public Database() {
         indexerFactory = new TableIndexerFactory();
    }

    Database(TableIndexerFactory indexerFactory) {
        this.indexerFactory = indexerFactory;
    }

    @Override
    public Map<String, List<Column>> getTableDescriptions() {
        return tables.entrySet()
                .stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        entry -> entry.getValue().getColumns()
                ));
    }

    @Override
    public QueryResult createTable(String name, List<Column> columns) {
        Table table = new Table(name, indexerFactory, this::checkForExistingValue);
        columns.forEach(table::addColumn);
        tables.put(name, table);
        return new QueryResult(OK);
    }

    private Boolean checkForExistingValue(TableWithAssignedColumns tableWithAssignedColumns, Object o) {
        DatabaseValue valueToCheck = new NumberValue((Number) o);
        String columnName = tableWithAssignedColumns.getColumnsWithAliases().get(0).getName();
        String tableName = tableWithAssignedColumns.tableName;
        ColumnValue columnValue = new ColumnValue(tableWithAssignedColumns.tableName + "." + columnName);
        WhereExpression equalsExpression = new EqualsExpression(columnValue, valueToCheck);
        QueryResult result = selectFromSingleTable(tableName, equalsExpression, false);
        return result.size() > 0;
    }

    @Override
    public QueryResult select(String[] tableNames, WhereExpression whereExpression, Boolean shouldLockTable) {
        QueryResult combinedResults = Arrays.stream(tableNames)
                .map(singleTableColumns -> selectFromSingleTable(singleTableColumns, whereExpression, shouldLockTable))
                .reduce(new QueryResult(OK), QueryResult::cartesian);
        return QueryResultFilter.filter(combinedResults, whereExpression);
    }

    private QueryResult selectFromSingleTable(String tableName, WhereExpression whereExpression, Boolean shouldLockTable) {
        Table table = tables.get(tableName);
        if (shouldLockTable)
            table.lock();
        return table.select(whereExpression);
    }

    @Override
    public QueryResult insert(String tableName, Map<String, Object> parametrisedRow) throws TableException {
        Table table = tables.get(tableName);
        return table.insert(parametrisedRow);
    }

    @Override
    public QueryResult dropTables(List<String> tableNames) {
        for (String tableName : tableNames)
            tables.remove(tableName);
        return new QueryResult("OK");
    }

    @Override
    public QueryResult createIndex(String tableName, String columnName) {
        Table table = tables.get(tableName);
        return table.createIndex(columnName);
    }

    @Override
    public QueryResult update(String tableName, Map<String, Object> newValues, WhereExpression whereExpression) {
        Table table = tables.get(tableName);
        return table.update(newValues, whereExpression);
    }

    @Override
    public QueryResult releaseLocksFromThread() {
        for (Map.Entry<String, Table> tableEntry : tables.entrySet())
            try {
                tableEntry.getValue().unlock();
            } catch (IllegalMonitorStateException ignored) {}

        return new QueryResult("OK");
    }

    @Override
    public QueryResult dump(OutputStream out) {
        try {
            ObjectOutputStream output = new ObjectOutputStream(out);
            output.writeObject(this);
        } catch (IOException e) {
            e.printStackTrace();
            return new QueryResult("ERROR", "Saving database problem \n" + e.getMessage(), null);
        }
        return new QueryResult("OK");
    }

    @Override
    public void linkForeignKeyMethod() {
        tables.forEach((key, table) -> table.setCheckForForeignValue(this::checkForExistingValue));
    }

    @Override
    public QueryResult delete(String tableName, WhereExpression whereExpression) {
        Table table = tables.get(tableName);
        return table.delete(whereExpression);
    }

}

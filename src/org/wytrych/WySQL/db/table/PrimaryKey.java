package org.wytrych.WySQL.db.table;

import org.wytrych.WySQL.SQLParser.WhereParser.values.CompositeDatabaseValue;

import java.io.Serializable;

class PrimaryKey implements Serializable {
    private CompositeDatabaseValue primaryKeyValues;

    void addValues(CompositeDatabaseValue primaryKeyValues) {
        this.primaryKeyValues = primaryKeyValues;
    }

    CompositeDatabaseValue getPrimaryKeyValues() {
        return primaryKeyValues;
    }
}

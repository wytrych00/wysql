package org.wytrych.WySQL.db.table;

import org.wytrych.QueryResult.QueryResult;
import org.wytrych.WySQL.SQLParser.WhereParser.*;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.EqualsExpression;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.WhereExpression;
import org.wytrych.WySQL.SQLParser.WhereParser.values.*;
import org.wytrych.WySQL.shared.*;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.wytrych.WySQL.shared.COLUMN_TYPE.PRIMARY_KEY;

public class Table implements Serializable {
    private static final long serialVersionUID = 1;
    private final String tableName;
    private final TableIndexerFactory indexerFactory;
    transient private BiFunction<TableWithAssignedColumns, Object, Boolean> checkForForeignValue;
    private ArrayList<ArrayList<Object>> table = new ArrayList<>();
    private Map<String, Integer> columnNameToPositionMapping = new HashMap<>();
    private Map<String, TableIndexer> columnIndexers = new HashMap<>();
    private List<Column> columns = new ArrayList<>();
    private int columnCounter = 0;
    private final String OK = "OK";
    private final String NULL = "NULL";
    private List<Integer> primaryKeyColumnIndexes = new ArrayList<>() ;
    private Map<String, Constraint> foreignKeyConstraints = new HashMap<>();
    private ReentrantLock tableLock = new ReentrantLock();

    public Table(String tableName, TableIndexerFactory indexerFactory, BiFunction<TableWithAssignedColumns, Object, Boolean> checkForForeignValue) {
        this.tableName = tableName;
        this.indexerFactory = indexerFactory;
        this.checkForForeignValue = checkForForeignValue;
        createPrimaryKeyColumn();
    }

    public void setCheckForForeignValue(BiFunction<TableWithAssignedColumns, Object, Boolean> checkForForeignValue) {
        this.checkForForeignValue = checkForForeignValue;
    }

    public List<Column> getColumns() {
        return columns.subList(1, columns.size());
    }

    private void createPrimaryKeyColumn() {
        Column primaryKeyColumn = new Column(TableIndexer.PRIMARY_KEY_INDEX_COLUMN_NAME, new ColumnType(PRIMARY_KEY));
        primaryKeyColumn.setIndexed(true);
        primaryKeyColumn.setPrimaryKey(true);
        addColumn(primaryKeyColumn);
    }

    synchronized public QueryResult addColumn(Column column) {
        columnNameToPositionMapping.put(column.name, columnCounter);
        columns.add(column);
        if (column.isIndexed())
            createIndex(column.name);
        if (column.isPrimary() && !column.name.equals(TableIndexer.PRIMARY_KEY_INDEX_COLUMN_NAME)) {
            primaryKeyColumnIndexes.add(columnCounter);
            columnIndexers.get(TableIndexer.PRIMARY_KEY_INDEX_COLUMN_NAME).setActive(true);
        }
        column.getConstraints().stream()
            .filter(constraint -> constraint.getConstraintType().equals(Constraint.ConstraintType.FOREIGN_KEY))
            .forEach(constraint -> foreignKeyConstraints.put(column.name, constraint));
        columnCounter++;
        return new QueryResult(OK);
    }

    synchronized public QueryResult createIndex(String columnName) {
        Integer columnIndex = columnNameToPositionMapping.get(columnName);
        TableIndexer indexer = indexerFactory.create(columnIndex);
        if (columnName.equals(TableIndexer.PRIMARY_KEY_INDEX_COLUMN_NAME))
            indexer.setActive(false);
        columnIndexers.put(columnName, indexer);
        return new QueryResult(OK);
    }

    synchronized public QueryResult insert(Map<String, Object> incomingRow) throws TableException {
        ArrayList<Object> row = IntStream.range(1, columnNameToPositionMapping.size())
            .boxed()
            .map(i -> getColumnValue(i, incomingRow))
            .collect(Collectors.toCollection(ArrayList::new));

        insertPrimaryKeyColumn(row);

        checkForDuplicatePrimaryKeyValue(row);
        checkForNullValues(row);
        checkForForeignKeys(row);

        table.add(row);

        if (primaryKeyColumnIndexes.size() > 0 || columnIndexers.size() > 1)
            columnIndexers.forEach((columnName, indexer) -> indexer.indexNewRow(row));

        return new QueryResult(OK);
    }

    private void insertPrimaryKeyColumn(ArrayList<Object> row) {
        PrimaryKey primaryKey = new PrimaryKey();
        row.add(0, primaryKey);

        if (primaryKeyColumnIndexes.size() > 0) {
            CompositeDatabaseValue primaryKeyComposite = createPrimaryKeyValue(row);
            primaryKey.addValues(primaryKeyComposite);
        }
    }

    private CompositeDatabaseValue createPrimaryKeyValue(ArrayList<Object> row) {
        List<DatabaseValue> primaryKeyValues = primaryKeyColumnIndexes.stream()
                .map(row::get)
                .map(value -> new NumberValue((Number) value))
                .collect(Collectors.toList());
        return new CompositeDatabaseValue(primaryKeyValues);
    }

    private void checkForDuplicatePrimaryKeyValue(ArrayList<Object> row) throws DuplicateEntryException {
        if (primaryKeyColumnIndexes.size() == 0)
            return;
        DatabaseValue primaryKeyValue = createPrimaryKeyValue(row);
        WhereExpression equalsExpression = new EqualsExpression(new ColumnValue(TableIndexer.PRIMARY_KEY_INDEX_COLUMN_NAME), primaryKeyValue);
        List<Range> range = equalsExpression.interpretRange(TableIndexer.PRIMARY_KEY_INDEX_COLUMN_NAME);
        ArrayList<ArrayList<Object>> rowWithExistingPrimaryKey = getIndexedEntriesFromIndexer(TableIndexer.PRIMARY_KEY_INDEX_COLUMN_NAME, range, false);

        if (rowWithExistingPrimaryKey.size() > 0) {
            PrimaryKey existingPrimaryKeyValue = (PrimaryKey) rowWithExistingPrimaryKey.get(0).get(0);
            if (existingPrimaryKeyValue.getPrimaryKeyValues().compareTo(primaryKeyValue) == 0)
                throw new DuplicateEntryException("Duplicate entry");
        }
    }

    private void checkForNullValues(ArrayList<Object> row) throws NullViolationException {
        for (int i = 0; i < columns.size(); i++) {
            Column column = columns.get(i);
            if (!column.isNullable() && row.get(i) instanceof NullValue)
                throw new NullViolationException("Null value for not-null column: " + column.name);
        }
    }

    private void checkForForeignKeys(ArrayList<Object> row) throws ForeignKeyViolationException {
        for (int i = 1; i < columns.size(); i++) {
            Column column = columns.get(i);
            if (foreignKeyConstraints.containsKey(column.name)) {
                Constraint foreignKeyConstraint = foreignKeyConstraints.get(column.name);
                Object columnValue = row.get(i);
                TableWithAssignedColumns targetColumn = new TableWithAssignedColumns(foreignKeyConstraint.getForeignTableName(), List.of(new Alias(foreignKeyConstraint.getForeignColumnName())));
                Boolean foreignKeyExists = checkForForeignValue.apply(targetColumn, columnValue);
                if (!foreignKeyExists)
                    throw new ForeignKeyViolationException("Foreign constraint violation", foreignKeyConstraint.getName());
            }
        }
    }

    private Object getColumnValue(Integer i, Map<String, Object> incomingRow) {
        String columnName = columns.get(i).name;
        return incomingRow.get(columnName);
    }

    synchronized public QueryResult select(WhereExpression whereExpression) {
        List<Map<String, Object>> result = getRowsStreamMatchingExpression(whereExpression)
                .collect(Collectors.toList());

        result.forEach(row -> row.remove(tableName + "." + TableIndexer.PRIMARY_KEY_INDEX_COLUMN_NAME));

        return new QueryResult(result);
    }

    private Stream<Map<String, Object>> getRowsStreamMatchingExpression(WhereExpression whereExpression) {
        return getFullTableOrIndexedRows(whereExpression, false)
                .stream()
                .filter(row -> rowMatches(whereExpression, row))
                .map(this::addSelectedColumnNamesAndCheckForNulls);
    }

    private ArrayList<ArrayList<Object>> getFullTableOrIndexedRows(WhereExpression whereExpression, Boolean forDelete) {
        String indexedColumn = findIndexedColumn();

        if (indexedColumn != null && columnIndexers.size() > 0)
            return getIndexedRows(indexedColumn, whereExpression, forDelete);

        return table;
    }

    private String findIndexedColumn() {
        for (Column column : columns)
            if (!column.name.equals(TableIndexer.PRIMARY_KEY_INDEX_COLUMN_NAME) && column.isIndexed())
                return column.name;

        return null;
    }

    private ArrayList<ArrayList<Object>> getIndexedRows(String indexedColumn, WhereExpression whereExpression, Boolean forDelete) {
        List<Range> range = whereExpression.interpretRange(tableName + "." + indexedColumn);
        return getIndexedEntriesFromIndexer(indexedColumn, range, forDelete);
    }

    private ArrayList<ArrayList<Object>> getIndexedEntriesFromIndexer(String indexedColumn, List<Range> range, Boolean forDelete) {
        TableIndexer indexer = columnIndexers
                .get(indexedColumn);

        if (forDelete)
            return indexer.deleteEntries(range);

        return indexer.getEntries(range);
    }

    private Map<String, Object> addSelectedColumnNamesAndCheckForNulls(ArrayList<Object> row) {
        return this.columns.stream()
            .collect(
                Collectors.toMap(
                        column -> tableName + "." + column.name,
                        column -> getColumnValueOrNull(columnNameToPositionMapping.get(column.name), row)
                ));
    }

    private Object getColumnValueOrNull(Integer columnIndex, ArrayList<Object> row) {
        Object columnValue = row.get(columnIndex);
        if (columnValue == null)
            return NULL;
        return columnValue;
    }

    synchronized public QueryResult delete(WhereExpression whereExpression) {
        ArrayList<ArrayList<Object>> rows = selectMatchingRows(whereExpression, true);

        table.removeAll(rows);

        return new QueryResult(OK);
    }

    private ArrayList<ArrayList<Object>> selectMatchingRows(WhereExpression whereExpression, Boolean forDelete) {
        return getFullTableOrIndexedRows(whereExpression, forDelete)
                    .stream()
                    .filter(row -> rowMatches(whereExpression, row))
                    .collect(Collectors.toCollection(ArrayList::new));
    }

    private boolean rowMatches(WhereExpression whereExpression, ArrayList<Object> row) {
        Map<String, Object> mappedRow = addSelectedColumnNamesAndCheckForNulls(row);
        return whereExpression.interpret(new Row(mappedRow));
    }

    public QueryResult update(Map<String, Object> newValues, WhereExpression whereExpression) {
        if (!tableLock.isHeldByCurrentThread())
            tableLock.lock();
        try {
            for (Map.Entry<String, Object> newValueMapping : newValues.entrySet()) {
                String columnName = newValueMapping.getKey();
                Object newValue = newValueMapping.getValue();

                Integer columnIndex = columnNameToPositionMapping.get(columnName);
                table.forEach(row -> {
                    Map<String, Object> rowValuesMapping = IntStream.range(0, row.size()).boxed()
                            .collect(Collectors.toMap(
                                    i -> tableName + "." + columns.get(i).name,
                                    row::get
                            ));

                    Row mappedRow = new Row(rowValuesMapping);
                    if (whereExpression.interpret(mappedRow)) {
                        row.remove(columnIndex.intValue());
                        row.add(columnIndex, newValue);
                    }
                });
            }
        } finally {
            tableLock.unlock();
        }

        return new QueryResult(OK);
    }

    public void lock() {
        tableLock.lock();
    }

    public void unlock() {
        tableLock.unlock();
    }

}

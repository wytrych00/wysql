package org.wytrych.WySQL.db.table;

public class NullViolationException extends TableException {
    public NullViolationException(String message) {
        super(message);
    }
}

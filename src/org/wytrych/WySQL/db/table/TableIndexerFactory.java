package org.wytrych.WySQL.db.table;

import java.io.Serializable;

public class TableIndexerFactory implements Serializable {
    public TableIndexer create(int i) {
        return new TableIndexer(i);
    }
}

package org.wytrych.WySQL.db.table;

public class ForeignKeyViolationException extends TableException {
    private final String constraintName;

    public ForeignKeyViolationException(String message, String constraintName) {
        super(message);
        this.constraintName = constraintName;
    }

    public String getConstraintName() {
        return constraintName;
    }
}

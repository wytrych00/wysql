package org.wytrych.WySQL.db.table;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.wytrych.WySQL.SQLParser.WhereParser.values.NumberValue;
import org.wytrych.WySQL.SQLParser.WhereParser.values.StringValue;
import org.wytrych.WySQL.SQLParser.WhereParser.Range;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.wytrych.WySQL.SQLParser.WhereParser.Range.EMPTY_RANGE;

class TableIndexerTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    private TableIndexer createIndexerWithData(Object... data) {
        ArrayList<ArrayList<Object>> tableData = createTableData(data);

        TableIndexer indexer = new TableIndexer(0);
        indexer.index(tableData);

        return indexer;
    }

    private ArrayList<ArrayList<Object>> createTableData(Object... dataPoints) {
        ArrayList<ArrayList<Object>> tableData = new ArrayList<>();

        for (Object dataPoint : dataPoints) {
            ArrayList<Object> row = new ArrayList(List.of(dataPoint));
            tableData.add(row);
        }

        return tableData;
    }

    @Test
    void sortsTableEntries() {
        TableIndexer indexer = createIndexerWithData(10, 5, 7, 1);

        Range range = new Range(new NumberValue(0), new NumberValue(Double.POSITIVE_INFINITY));
        ArrayList<ArrayList<Object>> entries = indexer.getEntries(List.of(range));

        assertEquals(4, entries.size());
        assertEquals(1, entries.get(0).get(0));
        assertEquals(5, entries.get(1).get(0));
        assertEquals(7, entries.get(2).get(0));
        assertEquals(10, entries.get(3).get(0));
    }

    @Test
    void returnsOnlyValuesWithinRange() {
        TableIndexer indexer = createIndexerWithData(10, 5, 7, 1);

        Range range = new Range(new NumberValue(4), new NumberValue(6));

        ArrayList<ArrayList<Object>> entries = indexer.getEntries(List.of(range));

        assertEquals(1, entries.size());
        assertEquals(5, entries.get(0).get(0));
    }

    @Test
    void getsValuesIfRangeIsAPoint() {
        TableIndexer indexer = createIndexerWithData(10);

        Range range = new Range(new NumberValue(10), new NumberValue(10));

        ArrayList<ArrayList<Object>> entries = indexer.getEntries(List.of(range));

        assertEquals(1, entries.size());
        assertEquals(10, entries.get(0).get(0));
    }

    @Test
    void getsAllValuesWhenRangeIsAPoint() {
        TableIndexer indexer = createIndexerWithData(1, 2, 10, 5, 11, 10, 10, 10, 10, 10, 7, 20, 10);

        Range range = new Range(new NumberValue(10), new NumberValue(10));

        ArrayList<ArrayList<Object>> entries = indexer.getEntries(List.of(range));

        assertEquals(7, entries.size());
        assertEquals(10, entries.get(0).get(0));
        assertEquals(10, entries.get(1).get(0));
        assertEquals(10, entries.get(2).get(0));
        assertEquals(10, entries.get(3).get(0));
        assertEquals(10, entries.get(4).get(0));
        assertEquals(10, entries.get(5).get(0));
        assertEquals(10, entries.get(6).get(0));
    }

    @Test
    void combinesRanges() {
        TableIndexer indexer = createIndexerWithData(10, 5, 7, 1, 3, 8, 15);
        Range range1 = new Range(new NumberValue(3), new NumberValue(9));
        Range range2 = new Range(new NumberValue(12), new NumberValue(Double.POSITIVE_INFINITY));

        ArrayList<ArrayList<Object>> entries = indexer.getEntries(List.of(range1, range2));

        assertEquals(5, entries.size());
        assertEquals(3, entries.get(0).get(0));
        assertEquals(5, entries.get(1).get(0));
        assertEquals(7, entries.get(2).get(0));
        assertEquals(8, entries.get(3).get(0));
        assertEquals(15, entries.get(4).get(0));
    }

    @Test
    void worksOnStrings() {
        TableIndexer indexer = createIndexerWithData("caaa", "zaaa", "aaaa", "daaa");
        Range range = new Range(new StringValue("b"), new StringValue("p"));

        ArrayList<ArrayList<Object>> entries = indexer.getEntries(List.of(range));

        assertEquals(2, entries.size());
        assertEquals("caaa", entries.get(0).get(0));
        assertEquals("daaa", entries.get(1).get(0));
    }

    @Test
    void indexesNewData() {
        TableIndexer indexer = createIndexerWithData(10, 5, 7, 1, 3, 8, 15);
        ArrayList<Object> row = new ArrayList<>();
        row.add(2);
        indexer.indexNewRow(row);

        Range range = new Range(new NumberValue(0), new NumberValue(Double.POSITIVE_INFINITY));
        ArrayList<ArrayList<Object>> entries = indexer.getEntries(List.of(range));

        assertEquals(8, entries.size());
        assertEquals(2, entries.get(1).get(0));
    }

    @Test
    void returnsEmptyArrayListWhenRangeIsEmpty() {
        TableIndexer indexer = createIndexerWithData(1, 2, 3);
        ArrayList<ArrayList<Object>> entries = indexer.getEntries(List.of(EMPTY_RANGE));

        assertEquals(0, entries.size());
    }
}


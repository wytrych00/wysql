package org.wytrych.WySQL.db.table;

import org.wytrych.WySQL.SQLParser.WhereParser.values.DatabaseValue;
import org.wytrych.WySQL.SQLParser.WhereParser.values.NumberValue;
import org.wytrych.WySQL.SQLParser.WhereParser.values.StringValue;
import org.wytrych.WySQL.SQLParser.WhereParser.Range;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

import static org.wytrych.WySQL.SQLParser.WhereParser.Range.EMPTY_RANGE;

public class TableIndexer implements Serializable {
    private class RowPointer implements Comparable<RowPointer>, Serializable {
        private final DatabaseValue indexedValue;
        private final ArrayList<Object> row;

        ArrayList<Object> getRow() {
            return row;
        }

        RowPointer(DatabaseValue indexedValue, ArrayList<Object> row) {
            this.indexedValue = indexedValue;
            this.row = row;
        }

        @Override
        public int compareTo(RowPointer b) {
            return indexedValue.compareTo(b.indexedValue);
        }
    }

    static final String PRIMARY_KEY_INDEX_COLUMN_NAME = "PRIMARY_KEY_INDEX_COLUMN_NAME";
    private Boolean active = true;

    void setActive(boolean active) {
        this.active = active;
    }

    private TreeMap<DatabaseValue, List<RowPointer>> tableData = new TreeMap<>();
    private Integer indexedColumn;

    public TableIndexer(Integer indexedColumn) {
        this.indexedColumn = indexedColumn;
    }

    void indexNewRow(ArrayList<Object> row) {
        if (!active)
            return;;
        RowPointer rowPointer = createRowPointer(row);
        tableData.computeIfAbsent(rowPointer.indexedValue, v -> new ArrayList<>()).add(rowPointer);
    }

    void index(ArrayList<ArrayList<Object>> tableData) {
        tableData.forEach(this::indexNewRow);
    }

    private RowPointer createRowPointer(ArrayList<Object> row) {
        return new RowPointer(sanitize(row.get(indexedColumn)), row);
    }

    private DatabaseValue sanitize(Object rawValue) {
        if (rawValue instanceof Number)
            return new NumberValue((Number) rawValue);
        if (rawValue instanceof PrimaryKey)
            return ((PrimaryKey) rawValue).getPrimaryKeyValues();
        return new StringValue((String) rawValue);
    }

    ArrayList<ArrayList<Object>> deleteEntries(List<Range> rangeList) {
        rangeList.stream()
                .map(this::getEntries)
                .forEach(this::removeEntries);

        return new ArrayList<>();
    }

    private void removeEntries(Map<DatabaseValue,List<RowPointer>> databaseValueListMap) {
        databaseValueListMap.keySet()
                .forEach(tableData::remove);
    }

    ArrayList<ArrayList<Object>> getEntries(List<Range> rangeList) {
        return rangeList.stream()
            .map(this::getEntries)
            .map(this::toRawTableFormat)
            .flatMap(List::stream)
            .collect(Collectors.toCollection(ArrayList::new));
    }

    private Map<DatabaseValue, List<RowPointer>> getEntries (Range range) {
        if (tableData.size() == 0 || range.equals(EMPTY_RANGE))
            return new TreeMap<>();
        if (Range.isFullRange(range))
            return tableData;

        return tableData.subMap(range.getMin(), true, range.getMax(), true);
    }

    private ArrayList<ArrayList<Object>> toRawTableFormat(Map<DatabaseValue, List<RowPointer>> tableData) {
        return tableData.entrySet()
                .stream()
                .map(Map.Entry::getValue)
                .flatMap(List::stream)
                .map(RowPointer::getRow)
                .collect(Collectors.toCollection(ArrayList::new));
    }

}

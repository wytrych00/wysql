package org.wytrych.WySQL.db.table;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.wytrych.QueryResult.QueryResult;
import org.wytrych.WySQL.SQLParser.WhereParser.values.NullValue;
import org.wytrych.WySQL.SQLParser.WhereParser.values.StringValue;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.TrueExpression;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.WhereExpression;
import org.wytrych.WySQL.SQLParser.WhereParser.Range;
import org.wytrych.WySQL.shared.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.*;
import static org.wytrych.WySQL.shared.COLUMN_TYPE.INTEGER;
import static org.wytrych.WySQL.shared.COLUMN_TYPE.VARCHAR;

class TableTest {
    private Table table;
    private TableIndexerFactory indexerFactory;

    @BeforeEach
    void setUp() {
        indexerFactory = mock(TableIndexerFactory.class);
        when(indexerFactory.create(isA(Integer.class))).thenReturn(mock(TableIndexer.class));
        table = new Table("table", indexerFactory, null);
        table.addColumn(new Column("column1", new ColumnType(VARCHAR)));
        table.addColumn(new Column("column2", new ColumnType(VARCHAR)));
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void addingColumnsAndData() throws TableException {
        Map<String, Object> row = new HashMap<>();
        row.put("column1", "Dupa");
        row.put("column2", 3);

        table.insert(row);

        QueryResult result = table.select(new TrueExpression());
        assertEquals("Dupa", result.get(0).get("table.column1"));
        assertEquals(3, result.get(0).get("table.column2"));
    }

    private List<Alias> createAliasedColumns(String[] columnNames) {
        return Arrays.stream(columnNames)
                .map(Alias::new)
                .collect(Collectors.toList());
    }

    @Test
    void addingOneColumn() throws TableException {
        Map<String, Object> row = new HashMap<>();
        row.put("column1", null);
        row.put("column2", 3);

        table.insert(row);

        QueryResult result = table.select(new TrueExpression());
        assertEquals("NULL", result.get(0).get("table.column1"));
        assertEquals(3, result.get(0).get("table.column2"));
    }

    @Test
    void returnsColumnNamesList() {
        List<Column> columnNames = table.getColumns();

        assertEquals("column1", columnNames.get(0).name);
        assertEquals("column2", columnNames.get(1).name);
    }

    @Test
    void createIndex() {
        table = new Table("table", indexerFactory, null);
        table.addColumn(new Column("column1", new ColumnType(VARCHAR), true));

        verify(indexerFactory).create(1);
    }

    @Test
    void usesIndex() {
        TableIndexer mockIndexer = mock(TableIndexer.class);
        Mockito.when(indexerFactory.create(1)).thenReturn(mockIndexer);

        table = new Table("table", indexerFactory, null);
        table.addColumn(new Column("column1", new ColumnType(VARCHAR), true));
        table.addColumn(new Column("column2", new ColumnType(VARCHAR), false));

        List<Alias> aliases = createAliasedColumns(new String[]{"column1"});
        WhereExpression whereExpressionMock = mock(WhereExpression.class);
        when(whereExpressionMock.interpretRange("table.column1")).thenReturn(List.of(new Range(new StringValue("a"), new StringValue("z"))));
        table.select(whereExpressionMock);

        ArgumentCaptor<List<Range>> rangeCaptor = ArgumentCaptor.forClass((Class) List.class);
        verify(mockIndexer).getEntries(rangeCaptor.capture());

        List<Range> capturedRange = rangeCaptor.getValue();

        assertEquals(1, capturedRange.size());
        assertEquals("a", capturedRange.get(0).getMin().getValue());
        assertEquals("z", capturedRange.get(0).getMax().getValue());
    }

    @Test
    void insertsRowToIndexer() throws TableException {
        TableIndexer mockIndexer = mock(TableIndexer.class);
        Mockito.when(indexerFactory.create(1)).thenReturn(mockIndexer);

        table = new Table("table", indexerFactory, null);
        table.addColumn(new Column("column1", new ColumnType(VARCHAR), true));
        table.addColumn(new Column("column2", new ColumnType(VARCHAR), false));

        Map<String, Object> row = new HashMap<>();
        row.put("column1", "Dupa");
        row.put("column2", 3);

        table.insert(row);

        ArgumentCaptor<ArrayList<Object>> listArgumentCaptor = ArgumentCaptor.forClass((Class) ArrayList.class);

        verify(mockIndexer).indexNewRow(listArgumentCaptor.capture());

        ArrayList<Object> arguments = listArgumentCaptor.getValue();
        assertTrue(arguments.get(0) instanceof PrimaryKey);
        assertEquals("Dupa", arguments.get(1));
        assertEquals(3, arguments.get(2));
    }

    @Test
    void throwsWhenPrimaryKeyIsNotUnique() throws TableException {
        table = new Table("table", new TableIndexerFactory(), null);
        Column column = new Column("A", new ColumnType(INTEGER));
        column.setPrimaryKey(true);
        table.addColumn(column);

        Map<String, Object> incomingRow = new HashMap<>();
        incomingRow.put("A", 1);
        table.insert(incomingRow);

        assertThrows(DuplicateEntryException.class, () -> table.insert(incomingRow));
    }

    @Test
    void throwsWhenCompositeKeyIsNotUnique() throws TableException {
        table = new Table("table", new TableIndexerFactory(), null);
        Column column = new Column("A", new ColumnType(INTEGER), false);
        column.setPrimaryKey(true);
        table.addColumn(column);
        Column column2 = new Column("B", new ColumnType(INTEGER), false);
        column2.setPrimaryKey(true);
        table.addColumn(column2);

        Map<String, Object> incomingRow = new HashMap<>();
        incomingRow.put("A", 1);
        incomingRow.put("B", 3);

        table.insert(incomingRow);

        Map<String, Object> incomingRow2 = new HashMap<>();
        incomingRow2.put("A", 1);
        incomingRow2.put("B", 2);

        table.insert(incomingRow2);

        Map<String, Object> incomingRow3 = new HashMap<>();
        incomingRow3.put("A", 1);
        incomingRow3.put("B", 5);

        table.insert(incomingRow3);

        Map<String, Object> incomingRow4 = new HashMap<>();
        incomingRow4.put("A", 2);
        incomingRow4.put("B", 3);

        table.insert(incomingRow4);

        QueryResult result = table.select(new TrueExpression());

        assertEquals(4, result.size());

        assertThrows(DuplicateEntryException.class, () -> table.insert(incomingRow));
    }

    @Test
    void throwsWhenNullValueIsPutIntoNonNullableColumn() {
        table = new Table("table", indexerFactory, null);
        Column column = new Column("A", new ColumnType(INTEGER), false);
        column.setNullable(false);
        table.addColumn(column);

        Map<String, Object> row = new HashMap<>();
        row.put("A", new NullValue());

        assertThrows(NullViolationException.class, () -> table.insert(row));
    }

    @Test
    void selectAlwaysReturnsAllRows() throws TableException {
        Map<String, Object> row = new HashMap<>();
        row.put("column1", 10);
        row.put("column2", 20);
        table.insert(row);

        QueryResult result = table.select(new TrueExpression());

        assertEquals(10, result.get(0).get("table.column1"));
        assertEquals(20, result.get(0).get("table.column2"));

    }

    private Boolean mappingFunction(TableWithAssignedColumns a, Object b) {
        return true;
    }

    @Test
    void isSerializable() throws TableException {
        Table table = new Table("table", new TableIndexerFactory(), (Serializable & BiFunction<TableWithAssignedColumns, Object, Boolean>) this::mappingFunction);
        Column column = new Column("columnA", new ColumnType(COLUMN_TYPE.INTEGER));
        table.addColumn(column);

        Map<String, Object> row = new HashMap<>();
        row.put("columnA", 10);
        table.insert(row);
        Boolean threw = false;

        try {
            new ObjectOutputStream(new ByteArrayOutputStream()).writeObject(table);
        } catch (IOException e) {
            e.printStackTrace();
            threw = true;
        }
        
        assertFalse(threw);
    }

}
package org.wytrych.WySQL.db.table;

public class DuplicateEntryException extends TableException {
    public DuplicateEntryException(String message) {
        super(message);
    }
}

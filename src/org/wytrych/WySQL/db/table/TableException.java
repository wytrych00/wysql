package org.wytrych.WySQL.db.table;

public class TableException extends Exception {
    public TableException (String message) {
        super(message);
    }
}

package org.wytrych.WySQL.db;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.wytrych.QueryResult.QueryResult;
import org.wytrych.WySQL.SQLParser.WhereParser.*;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.EqualsExpression;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.TrueExpression;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.WhereExpression;
import org.wytrych.WySQL.SQLParser.WhereParser.values.ColumnValue;
import org.wytrych.WySQL.SQLParser.WhereParser.values.NumberValue;
import org.wytrych.WySQL.db.table.ForeignKeyViolationException;
import org.wytrych.WySQL.db.table.TableException;
import org.wytrych.WySQL.db.table.TableIndexer;
import org.wytrych.WySQL.db.table.TableIndexerFactory;
import org.wytrych.WySQL.shared.*;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.*;

class DatabaseTest {
    private IDatabase database;

    @BeforeEach
    void setUp() {
        database = new Database();
    }

    @AfterEach
    void tearDown() {
    }

    private List<TableWithAssignedColumns> createColumns(String tableName, String[] columns) {
        List<Alias> columnsList = Arrays.stream(columns).map(Alias::new).collect(Collectors.toList());
        TableWithAssignedColumns tableWithAssignedColumns = new TableWithAssignedColumns(tableName, columnsList);
        List<TableWithAssignedColumns> tableWithAssignedColumnsList = new ArrayList<>();
        tableWithAssignedColumnsList.add(tableWithAssignedColumns);
        return tableWithAssignedColumnsList;
    }

    private List<Column> createColumnDescriptions(String[] columnNames) {
        return Arrays
                .stream(columnNames)
                .map(columnName -> new Column(columnName, new ColumnType(COLUMN_TYPE.VARCHAR)))
                .collect(Collectors.toList());
    }

    @Test
    void createTable() {
        database.createTable("myTable", createColumnDescriptions(new String[]{"column"}));
        QueryResult result = database.select(new String[]{"myTable"}, null, false);
        assertEquals(0, result.size());
    }

    @Test
    void createTableWithIndexes() {
        TableIndexerFactory mockTableIndexerFactory = mock(TableIndexerFactory.class);
        when(mockTableIndexerFactory.create(isA(Integer.class))).thenReturn(new TableIndexer(0));
        database = new Database(mockTableIndexerFactory);
        List<Column> columns = createColumnDescriptions(new String[]{"column"});
        columns.get(0).setIndexed(true);
        database.createTable("myTable", columns);
        verify(mockTableIndexerFactory).create(1);
    }

    @Test
    void insert() throws TableException {
        database.createTable("myTable", createColumnDescriptions(new String[]{"column"}));
        Map<String, Object> values = new HashMap<>();
        values.put("column", "value");
        database.insert("myTable", values);
        QueryResult result = database.select(new String[]{"myTable"}, new TrueExpression(), false);
        assertEquals(1, result.size());
        assertEquals("value", result.get(0).get("myTable.column"));
    }

    private class ColumnCreator {
        private String[] columnNames;

        ColumnCreator(String[] columnNames) {
            this.columnNames = columnNames;
        }

        Map<String, Object> createColumnValues(Object... values) {
            return IntStream.range(0, columnNames.length)
                    .boxed()
                    .collect(Collectors.toMap(
                            i -> columnNames[i],
                            i -> (values[i] != null) ? values[i] : "NULL"
                    ));
        }
    }

    @Test
    void insertWithManyColumns() throws TableException {
        String[] columnNames = new String[]{"columnA", "columnB", "columnC"};
        database.createTable("myTable", createColumnDescriptions(columnNames));
        ColumnCreator cc = new ColumnCreator(columnNames);

        database.insert("myTable", cc.createColumnValues("value", null, null));
        database.insert("myTable", cc.createColumnValues(null, "123", null));
        database.insert("myTable", cc.createColumnValues(null, null, "XXX"));

        QueryResult result = database.select(new String[]{"myTable"}, new TrueExpression(), false);

        assertEquals(3, result.size());
        assertEquals("value", result.get(0).get("myTable.columnA"));
        assertEquals("123", result.get(1).get("myTable.columnB"));
        assertEquals("XXX", result.get(2).get("myTable.columnC"));
        assertEquals("NULL", result.get(2).get("myTable.columnA"));
    }

    @Test
    void insertManyValues() throws TableException {
        String[] columnNames = new String[]{"columnA", "columnB", "columnC"};
        database.createTable("myTable", createColumnDescriptions(columnNames));
        ColumnCreator cc = new ColumnCreator(columnNames);
        Object[] values = {"XXX", 123, "A value"};
        database.insert("myTable", cc.createColumnValues(values));

        QueryResult result = database.select(new String[]{"myTable"}, new TrueExpression(), false);

        assertEquals(1, result.size());
        assertEquals("XXX", result.get(0).get("myTable.columnA"));
        assertEquals(123, result.get(0).get("myTable.columnB"));
        assertEquals("A value", result.get(0).get("myTable.columnC"));
    }

    @Test
    void insertSomeValues() throws TableException {
        database.createTable("myTable", createColumnDescriptions(new String[]{"columnA", "columnB", "columnC"}));
        Map<String, Object> values = new HashMap<>();
        values.put("columnB", "bla");
        database.insert("myTable", values);

        QueryResult result = database.select(new String[]{"myTable"}, new TrueExpression(), false);

        assertEquals(1, result.size());
        assertEquals("NULL", result.get(0).get("myTable.columnA"));
        assertEquals("bla", result.get(0).get("myTable.columnB"));
        assertEquals("NULL", result.get(0).get("myTable.columnC"));
    }

    @Test
    void selectFromManyTables() throws TableException {
        String[] columnNamesA = new String[]{"a", "b"};
        database.createTable("A", createColumnDescriptions(columnNamesA));
        ColumnCreator ccA = new ColumnCreator(columnNamesA);

        String[] columnNamesB = new String[]{"c", "d"};
        database.createTable("B", createColumnDescriptions(columnNamesB));
        ColumnCreator ccB = new ColumnCreator(columnNamesB);

        Object[] values1 = {0, 1};
        Object[] values2 = {1, 0};
        Object[] values3 = {1, 1};
        Object[] values4 = {0, 0};

        database.insert("A", ccA.createColumnValues(values1));
        database.insert("A", ccA.createColumnValues(values2));
        database.insert("B", ccB.createColumnValues(values3));
        database.insert("B", ccB.createColumnValues(values4));

        QueryResult result = database.select(new String[]{"A", "B"}, new TrueExpression(), false);

        assertEquals(4, result.size());
        QueryResult.RowAccessor row1 = result.get(0);
        QueryResult.RowAccessor row2 = result.get(1);
        QueryResult.RowAccessor row3 = result.get(2);
        QueryResult.RowAccessor row4 = result.get(3);

        /*
        Cartesian product:

        0 1 1 1
        1 0 1 1
        0 1 0 0
        1 0 0 0

         */

        // ROW 1
        assertEquals(0, row1.get("A.a"));
        assertEquals(1, row1.get("A.b"));
        assertEquals(1, row1.get("B.c"));
        assertEquals(1, row1.get("B.d"));

        // ROW 2
        assertEquals(1, row2.get("A.a"));
        assertEquals(0, row2.get("A.b"));
        assertEquals(1, row2.get("B.c"));
        assertEquals(1, row2.get("B.d"));

        // ROW 3
        assertEquals(0, row3.get("A.a"));
        assertEquals(1, row3.get("A.b"));
        assertEquals(0, row3.get("B.c"));
        assertEquals(0, row3.get("B.d"));

        // ROW 4
        assertEquals(1, row4.get("A.a"));
        assertEquals(0, row4.get("A.b"));
        assertEquals(0, row4.get("B.c"));
        assertEquals(0, row4.get("B.d"));
    }

    @Test
    void filterColumnsFromResultsSet() throws TableException {
        String[] columnNamesA = new String[]{"a", "b"};
        database.createTable("A", createColumnDescriptions(columnNamesA));
        ColumnCreator ccA = new ColumnCreator(columnNamesA);

        String[] columnNamesB = new String[]{"c", "d"};
        database.createTable("B", createColumnDescriptions(columnNamesB));
        ColumnCreator ccB = new ColumnCreator(columnNamesB);

        Object[] values1 = {0, 1};
        Object[] values2 = {1, 0};

        database.insert("A", ccA.createColumnValues(values1));
        database.insert("B", ccB.createColumnValues(values2));

        QueryResult result = database.select(new String[]{"A", "B"}, new TrueExpression(), false);

        assertEquals(1, result.size());

        QueryResult.RowAccessor row = result.get(0);
        assertEquals(1, row.get("A.b"));
    }

    @Test
    void correctlyFiltersOutPrefixedColumns() throws TableException {
        database.createTable("A", createColumnDescriptions(new String[]{"a", "b"}));
        database.createTable("B", createColumnDescriptions(new String[]{"a", "b"}));

        String[] columnNamesA = new String[]{"a", "b"};
        database.createTable("A", createColumnDescriptions(columnNamesA));
        ColumnCreator ccA = new ColumnCreator(columnNamesA);

        String[] columnNamesB = new String[]{"a", "b"};
        database.createTable("A", createColumnDescriptions(columnNamesB));
        ColumnCreator ccB = new ColumnCreator(columnNamesB);

        Object[] values1 = {0, 1};
        Object[] values2 = {1, 0};
        Object[] values3 = {1, 1};
        Object[] values4 = {2, 1};
        Object[] values5 = {2, 2};

        database.insert("A", ccA.createColumnValues(values1));
        database.insert("A", ccA.createColumnValues(values2));
        database.insert("B", ccA.createColumnValues(values3));
        database.insert("B", ccA.createColumnValues(values4));
        database.insert("B", ccA.createColumnValues(values5));

        QueryResult result = database.select(new String[]{"A", "B"}, new TrueExpression(), false);

        assertEquals(6, result.size());
        assertEquals(0, result.get(0).get("A.a"));
        assertEquals(1, result.get(0).get("B.b"));
        assertEquals(1, result.get(5).get("A.a"));
        assertEquals(2, result.get(5).get("B.b"));
    }

    @Test
    void returnsAllTablesDescriptions() {
        database.createTable("A", createColumnDescriptions(new String[]{"a", "b"}));
        database.createTable("B", createColumnDescriptions(new String[]{"c", "d", "e"}));

        Map<String, List<Column>> tablesDescriptions = database.getTableDescriptions();
        List<Column> tableA = tablesDescriptions.get("A");
        List<Column> tableB = tablesDescriptions.get("B");

        assertEquals("a", tableA.get(0).name);
        assertEquals("b", tableA.get(1).name);
        assertEquals("c", tableB.get(0).name);
        assertEquals("d", tableB.get(1).name);
        assertEquals("e", tableB.get(2).name);
    }

    @Test
    void storesValuesWithCorrectTypes() throws TableException {
        Column columnA = new Column("A", new ColumnType(COLUMN_TYPE.INTEGER));
        Column columnB = new Column("B", new ColumnType(COLUMN_TYPE.VARCHAR));
        Column columnC = new Column("C", new ColumnType(COLUMN_TYPE.BIT));
        List<Column> columns = Arrays.asList(columnA, columnB, columnC);

        database.createTable("A", columns);

        ColumnCreator cc = new ColumnCreator(new String[]{"A", "B", "C"});

        database.insert("A", cc.createColumnValues(20, "Blabla", true));


        List<TableWithAssignedColumns> resultColumns = createColumns("A", new String[]{"A", "B", "C"});
        QueryResult result = database.select(new String[]{"A"}, new TrueExpression(), false);

        assertEquals(20, result.get(0).get("A.A"));
        assertEquals("Blabla", result.get(0).get("A.B"));
        assertEquals(true, result.get(0).get("A.C"));
    }

    @Test
    void createsTableIndex() {
        TableIndexerFactory mockTableIndexerFactory = mock(TableIndexerFactory.class);
        when(mockTableIndexerFactory.create(isA(Integer.class))).thenReturn(new TableIndexer(0));
        database = new Database(mockTableIndexerFactory);

        Column column = new Column("column", new ColumnType(COLUMN_TYPE.INTEGER));
        column.setIndexed(true);

        List<Column> columns = List.of(column);
        database.createTable("A", columns);

        verify(mockTableIndexerFactory).create(1);
    }

    @Test
    void checksTheIndexRanges() throws TableException {
        WhereExpression whereExpressionMock = mock(WhereExpression.class);
        when(whereExpressionMock.interpretRange("A.columnA")).thenReturn(List.of(new Range(new NumberValue(-1), new NumberValue(2))));
        when(whereExpressionMock.interpret(isA(Row.class))).thenReturn(true);

        ArgumentCaptor<String> columnNameCaptor = ArgumentCaptor.forClass(String.class);
        Column columnA = new Column("columnA", new ColumnType(COLUMN_TYPE.INTEGER));
        columnA.setIndexed(true);
        Column columnB = new Column("columnB", new ColumnType(COLUMN_TYPE.INTEGER));
        List<Column> columns = List.of(columnA, columnB);

        database.createTable("A", columns);

        ColumnCreator cc = new ColumnCreator(new String[]{"columnA", "columnB"});
        database.insert("A", cc.createColumnValues(0, 1));

        QueryResult result = database.select(new String[]{"A"}, whereExpressionMock, false);
        verify(whereExpressionMock).interpretRange(columnNameCaptor.capture());
        assertEquals("A.columnA", columnNameCaptor.getValue());
        assertEquals(1, result.size());
    }

    @Test
    void dropsTables() {
        database.createTable("myTable", createColumnDescriptions(new String[]{"column"}));
        database.dropTables(List.of("myTable"));
        assertThrows(NullPointerException.class, () -> database.select(new String[]{"myTable"}, null, false));
    }

    @Test
    void createIndex() {
        TableIndexerFactory mockTableIndexerFactory = mock(TableIndexerFactory.class);
        when(mockTableIndexerFactory.create(isA(Integer.class))).thenReturn(new TableIndexer(0));
        database = new Database(mockTableIndexerFactory);
        List<Column> columns = createColumnDescriptions(new String[]{"column"});
        database.createTable("myTable", columns);
        verify(mockTableIndexerFactory).create(0);
        verifyNoMoreInteractions(mockTableIndexerFactory);

        database.createIndex("myTable", "column");

        verify(mockTableIndexerFactory).create(1);
    }

    @Test
    void foreignKeyConstraint() throws TableException {
        database = new Database(new TableIndexerFactory());

        List<Column> columnsA = createColumnDescriptions(new String[]{"columnA"});
        Constraint constraint = new Constraint(Constraint.ConstraintType.FOREIGN_KEY,"myConstraint", "columnA","tableB", "columnB");
        columnsA.get(0).addConstraint(constraint);
        database.createTable("tableA", columnsA);

        List<Column> columnsB = createColumnDescriptions(new String[]{"columnB"});
        columnsB.get(0).setPrimaryKey(true);
        columnsB.get(0).setIndexed(true);
        database.createTable("tableB", columnsB);

        Map<String, Object> tableBRow = new HashMap<>();
        tableBRow.put("columnB", 10);

        Map<String, Object> tableACorrectRow = new HashMap<>();
        tableACorrectRow.put("columnA", 10);

        Map<String, Object> tableAIncorrectRow = new HashMap<>();
        tableAIncorrectRow.put("columnA", 11);

        database.insert("tableB", tableBRow);
        database.insert("tableA", tableACorrectRow);

        assertThrows(ForeignKeyViolationException.class, () -> database.insert("tableA", tableAIncorrectRow));
    }

    @Test
    void multipleForeignKeyConstraints() throws TableException {
        database = new Database(new TableIndexerFactory());

        List<Column> columnsA = createColumnDescriptions(new String[]{"B_link", "C_link"});
        Constraint constraint1 = new Constraint(Constraint.ConstraintType.FOREIGN_KEY,"myConstraint_B", "B_link","tableB", "columnB");
        Constraint constraint2 = new Constraint(Constraint.ConstraintType.FOREIGN_KEY,"myConstraint_C", "C_link","tableC", "columnC");
        columnsA.get(0).addConstraint(constraint1);
        columnsA.get(1).addConstraint(constraint2);
        database.createTable("tableA", columnsA);

        List<Column> columnsB = createColumnDescriptions(new String[]{"columnB"});
        columnsB.get(0).setPrimaryKey(true);
        columnsB.get(0).setIndexed(true);
        database.createTable("tableB", columnsB);

        List<Column> columnsC = createColumnDescriptions(new String[]{"columnC"});
        columnsC.get(0).setPrimaryKey(true);
        columnsC.get(0).setIndexed(true);
        database.createTable("tableC", columnsC);

        Map<String, Object> tableBRow = new HashMap<>();
        tableBRow.put("columnB", 10);

        Map<String, Object> tableCRow = new HashMap<>();
        tableCRow.put("columnC", 20);

        Map<String, Object> tableACorrectRow = new HashMap<>();
        tableACorrectRow.put("B_link", 10);
        tableACorrectRow.put("C_link", 20);

        Map<String, Object> tableAIncorrectRow = new HashMap<>();
        tableAIncorrectRow.put("B_link", 10);
        tableAIncorrectRow.put("C_link", 21);

        database.insert("tableB", tableBRow);
        database.insert("tableC", tableCRow);
        database.insert("tableA", tableACorrectRow);

        assertThrows(ForeignKeyViolationException.class, () -> database.insert("tableA", tableAIncorrectRow));
    }

    @Test
    void update() throws TableException {
        database.createTable("myTable", createColumnDescriptions(new String[]{"columnA", "columnB"}));
        Map<String, Object> row = new HashMap<>();
        row.put("columnA", 1);
        row.put("columnB", 2);
        database.insert("myTable", row);
        row = new HashMap<>();
        row.put("columnA", 3);
        row.put("columnB", 5);
        database.insert("myTable", row);

        ColumnValue columnValue = new ColumnValue("myTable.columnB");
        NumberValue numberValue = new NumberValue(5);

        Map<String, Object> newValues = new HashMap<>();
        newValues.put("columnB", 15);

        WhereExpression whereExpression = new EqualsExpression(columnValue, numberValue);
        database.update("myTable",  newValues, whereExpression);

        QueryResult result = database.select(new String[]{"myTable"}, new TrueExpression(), false);

        assertEquals(15, result.get(1).get("myTable.columnB"));
    }

    @Test
    void isSerializable() throws IOException {
        database.createTable("A", createColumnDescriptions(new String[]{"columnA", "columnB"}));

        OutputStream out = mock(OutputStream.class);
        database.dump(out);

        verify(out, atLeastOnce()).write(any(byte[].class), any(int.class), any(int.class));
    }

    @Test
    void canBeDeserialized() throws TableException, IOException, ClassNotFoundException {
        Column columnA = new Column("columnA", new ColumnType(COLUMN_TYPE.INTEGER));
        Constraint foreignKeyConstraint = new Constraint(Constraint.ConstraintType.FOREIGN_KEY, "constraint", "columnA", "tableB", "columnB");
        columnA.addConstraint(foreignKeyConstraint);

        database.createTable("tableA", List.of(columnA));

        Column columnB = new Column("columnB", new ColumnType(COLUMN_TYPE.INTEGER));
        columnB.setIndexed(true);
        database.createTable("tableB", List.of(columnB));

        Map<String, Object> row1 = new HashMap<>();
        row1.put("columnA", 42);

        Map<String, Object> row2 = new HashMap<>();
        row2.put("columnB", 42);

        database.insert("tableB", row2);
        database.insert("tableA", row1);

        OutputStream out = new ByteArrayOutputStream();

        database.dump(out);

        byte[] buf = ((ByteArrayOutputStream) out).toByteArray();
        InputStream in = new ByteArrayInputStream(buf);

        ObjectInputStream objectInputStream = new ObjectInputStream(in);

        IDatabase reconstructedDb = (IDatabase) objectInputStream.readObject();
        reconstructedDb.linkForeignKeyMethod();

        QueryResult result;

        result = reconstructedDb.select(new String[]{"tableA", "tableB"}, new TrueExpression(), false);
        assertEquals(1, result.size());
        assertEquals(42, result.get(0).get("tableA.columnA"));
        assertEquals(42, result.get(0).get("tableB.columnB"));

        Map<String, Object> row3 = new HashMap<>();
        row3.put("columnA", 50);

        assertThrows(ForeignKeyViolationException.class, () -> reconstructedDb.insert("tableA", row3));
    }

    @Test
    void deleteChosenRowWithoutIndexing() throws TableException {
        String tableName = "myTable";
        List<Column> columns = createColumnDescriptions(new String[]{"A", "B"});

        testDelete(tableName, columns);
    }

    @Test
    void deleteChosenRowWithIndexing() throws TableException {
        String tableName = "myTable";

        List<Column> columns = createColumnDescriptions(new String[]{"A", "B"});
        columns.get(0).setIndexed(true);

        testDelete(tableName, columns);
    }

    private void testDelete(String tableName, List<Column> columns) throws TableException {
        database.createTable(tableName, columns);

        Map<String, Object> row = new HashMap<>();
        row.put("A", 10);
        row.put("B", 12);
        database.insert(tableName, row);

        row.clear();

        row.put("A", 20);
        row.put("B", 22);

        database.insert(tableName, row);

        ColumnValue columnValue = new ColumnValue("myTable.A");
        NumberValue numberValue = new NumberValue(10);
        WhereExpression whereExpression = new EqualsExpression(columnValue, numberValue);

        database.delete(tableName, whereExpression);

        QueryResult result = database.select(new String[]{tableName}, new TrueExpression(), false);

        assertEquals(1, result.size());
        assertEquals(20, result.get(0).get("myTable.A"));
        assertEquals(22, result.get(0).get("myTable.B"));
    }

}
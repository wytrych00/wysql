package org.wytrych.WySQL.db;

import org.junit.jupiter.api.Test;
import org.wytrych.QueryResult.QueryResult;
import org.wytrych.WySQL.SQLParser.WhereParser.Range;
import org.wytrych.WySQL.SQLParser.WhereParser.Row;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.WhereExpression;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SimpleFilter implements WhereExpression {
    private final String columnName;

    SimpleFilter(String columnName) {
        this.columnName = columnName;
    }

    @Override
    public Boolean interpret(Row context) {
        return context.get(columnName).equals(true);
    }

    @Override
    public List<Range> interpretRange(String columnName) {
        return null;
    }
}

class QueryResultTest {

    @Test
    void filtering() {
        Map<String, Object> row1 = new HashMap<>();
        Map<String, Object> row2 = new HashMap<>();

        row1.put("A", true);
        row2.put("A", false);

        List<Map<String, Object>> result = List.of(row1, row2);
        QueryResult qr = new QueryResult(result);

        QueryResult filteredResult = QueryResultFilter.filter(qr, new SimpleFilter("A"));

        assertEquals(1, filteredResult.size());
        assertEquals(true, filteredResult.get(0).get("A"));
    }

}
package org.wytrych.WySQL.db;

import org.wytrych.QueryResult.QueryResult;
import org.wytrych.WySQL.SQLParser.WhereParser.Row;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.WhereExpression;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class QueryResultFilter {
    public static QueryResult filter(QueryResult result, WhereExpression filter) {
        List<Map<String, Object>> filteredResult = new ArrayList<>();

        for (int i = 0; i < result.size(); i++) {
            QueryResult.RowAccessor row = result.get(i);
            if (filter.interpret(new Row(row)))
                filteredResult.add(result.result.get(i));
        }

        return new QueryResult(filteredResult, result.getColumnMapping());
    }
}

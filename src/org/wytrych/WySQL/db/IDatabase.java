package org.wytrych.WySQL.db;

import org.wytrych.QueryResult.QueryResult;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.WhereExpression;
import org.wytrych.WySQL.shared.Column;
import org.wytrych.WySQL.db.table.TableException;

import java.io.OutputStream;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

public interface IDatabase {
    DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    ZoneId projectTimeZone = ZoneId.of("UTC");

    QueryResult createTable(String name, List<Column> columns);
    QueryResult insert(String tableName, Map<String,Object> parametrisedRow) throws TableException;
    QueryResult select(String[] tableNames, WhereExpression whereExpression, Boolean shouldLockTable);
    QueryResult dropTables(List<String> tableNames);
    QueryResult createIndex(String table, String column);
    QueryResult update(String myTable, Map<String,Object> newValues, WhereExpression whereExpression);
    QueryResult releaseLocksFromThread();
    QueryResult dump(OutputStream out);
    QueryResult delete(String tableName, WhereExpression whereExpression);

    void linkForeignKeyMethod();

    Map<String,List<Column>> getTableDescriptions();

}
package org.wytrych.WySQL.SQLParser;

public enum OPERATION {
    CREATE,
    INSERT,
    SELECT,
    DROP,
    UPDATE,
    ROLLBACK,
    DUMP,
    DELETE
}

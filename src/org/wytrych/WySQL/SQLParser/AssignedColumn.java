package org.wytrych.WySQL.SQLParser;

import org.wytrych.WySQL.shared.Alias;

public class AssignedColumn {
    public final String tableName;
    public final Alias column;

    public AssignedColumn(String tableName, Alias column) {
        this.tableName = tableName;
        this.column = column;
    }
}

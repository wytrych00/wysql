package org.wytrych.WySQL.SQLParser;

import org.wytrych.QueryResult.QueryStatement;
import org.wytrych.WySQL.Commands.Command;
import org.wytrych.WySQL.Commands.ICommandFactory;
import org.wytrych.WySQL.SQLParser.queryParsers.*;

import java.util.*;

public class SQLParser implements ISQLParser {

    private final ICommandFactory commandFactory;

    public SQLParser(ICommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    @Override
    public Command parseStatement(QueryStatement statement) throws UnsupportedCommandException {
        QueryParser parser = parseQuery(statement.sql, statement.parameters);
        return parser.createCommand(commandFactory);
    }

    @Override
    public Command parseQuery(String sqlQuery) throws UnsupportedCommandException {
        QueryParser parser = parseQuery(sqlQuery, new LinkedList<>());
        return parser.createCommand(commandFactory);
    }

    private QueryParser parseQuery(String sqlQuery, Queue<Object> parameters) throws UnsupportedCommandException {
        StringParser parser = new StringParser(sqlQuery);
        String commandString = parser.getNextWord();
        OPERATION command;
        try {
            command = OPERATION.valueOf(commandString.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new UnsupportedCommandException(commandString);
        }

        switch (command) {
            case SELECT:
                return new SelectParser(parser, parameters);
            case INSERT:
                return new InsertParser(parser, parameters);
            case CREATE:
                return new CreateParser(parser);
            case DROP:
                return new DropParser(parser);
            case UPDATE:
                return new UpdateParser(parser, parameters);
            case ROLLBACK:
                return new RollbackParser(parser);
            case DUMP:
                return new DumpParser(parser);
            case DELETE:
                return new DeleteParser(parser, parameters);
            default:
                throw new UnsupportedCommandException(command.toString());
        }
    }

}


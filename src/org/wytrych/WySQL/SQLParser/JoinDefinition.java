package org.wytrych.WySQL.SQLParser;

import org.wytrych.WySQL.SQLParser.WhereParser.expressions.WhereExpression;
import org.wytrych.WySQL.shared.Alias;

public class JoinDefinition {
    private final Alias table;
    private final String ownColumnName;
    private final Alias foreignColumn;
    private WhereExpression whereExpression;

    public JoinDefinition(Alias table, String ownColumnName, Alias foreignColumn, WhereExpression whereExpression) {
        this.table = table;
        this.ownColumnName = ownColumnName;
        this.foreignColumn = foreignColumn;
        this.whereExpression = whereExpression;
    }

    public Alias getTable() {
        return table;
    }

    public String getOwnColumnName() {
        return ownColumnName;
    }

    public Alias getForeignColumn() {
        return foreignColumn;
    }

    public WhereExpression getWhereExpression() {
        return whereExpression;
    }

    public void setWhereExpression(WhereExpression whereExpression) {
        this.whereExpression = whereExpression;
    }
}

package org.wytrych.WySQL.SQLParser;

public enum KeywordTypes {
    BLANK,                  // Used for empty words like INTO, FROM, etc
    WORD,                   // Single word
    PARAMETERS,             // "(a, b, c, d)" - parameters inside parenthesis
    OPEN_PARAMETERS_LIST,   // "a, b, c, d" - parameters without parenthesis
    WORD_WITH_PARAMETER,    // varchar (255) - word with parameter inside parenthesis
    EXPRESSION,             // reads everything up to semicolon (;) - used for reading WHERE expression
    TABLE_NAMES_LIST,       // reads everything up to WHERE - used for reading table lists
    JOINS,                  // for parsing join definitions
}

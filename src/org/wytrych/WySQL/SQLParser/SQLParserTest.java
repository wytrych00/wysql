package org.wytrych.WySQL.SQLParser;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.wytrych.QueryResult.QueryStatement;
import org.wytrych.WySQL.Commands.ICommandFactory;
import org.wytrych.WySQL.SQLParser.WhereParser.Row;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.TrueExpression;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.WhereExpression;
import org.wytrych.WySQL.shared.*;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class SQLParserTest {
    private SQLParser parser;
    private ArgumentCaptor<String> stringCaptor;
    private ArgumentCaptor<String> stringCaptor2;
    private ArgumentCaptor<List<Column>> columnListCaptor;
    private ArgumentCaptor<List<TableWithAssignedColumns>> assignedColumnsListCaptor;
    private ArgumentCaptor<String[]> stringArrayCaptor;
    private ArgumentCaptor<Object[]> objectArrayCaptor;
    private ArgumentCaptor<Map<String, Object>> mapCaptor;
    private ArgumentCaptor<List<Alias>> columnAliasListCaptor;
    private ArgumentCaptor<WhereExpression> whereExpressionArgumentCaptor;
    private ArgumentCaptor<List<String>> stringListCaptor;
    private ArgumentCaptor<List<JoinDefinition>> joinListCaptor;
    private ArgumentCaptor<Boolean> booleanCaptor;

    private ICommandFactory mockedInterpreter;
    private ArgumentCaptor<String> stringCaptor3;


    @BeforeEach
    void setUp() {
        stringCaptor = ArgumentCaptor.forClass(String.class);
        stringCaptor2 = ArgumentCaptor.forClass(String.class);
        stringCaptor3 = ArgumentCaptor.forClass(String.class);
        columnListCaptor = ArgumentCaptor.forClass((Class) List.class);
        assignedColumnsListCaptor = ArgumentCaptor.forClass((Class) List.class);
        stringArrayCaptor = ArgumentCaptor.forClass(String[].class);
        objectArrayCaptor = ArgumentCaptor.forClass(Object[].class);
        mapCaptor = ArgumentCaptor.forClass((Class) Map.class);
        columnAliasListCaptor = ArgumentCaptor.forClass((Class) List.class);
        whereExpressionArgumentCaptor = ArgumentCaptor.forClass(WhereExpression.class);
        stringListCaptor = ArgumentCaptor.forClass((Class) List.class);
        joinListCaptor = ArgumentCaptor.forClass((Class) List.class);
        booleanCaptor = ArgumentCaptor.forClass(Boolean.class);

        mockedInterpreter = mock(ICommandFactory.class);

        parser = new SQLParser(mockedInterpreter);
    }

    @AfterEach
    void tearDown() {
    }

    private void executeQuery(String query) throws ISQLParser.UnsupportedCommandException {
        parser.parseQuery(query);
    }

    private void executeStatement(String query, Queue<Object> parameters) throws ISQLParser.UnsupportedCommandException {
        parser.parseStatement(new QueryStatement(query, parameters));
    }

    private void verifyCreateCreate() {
        verify(mockedInterpreter).createCreate(stringCaptor.capture(), columnListCaptor.capture());
    }

    private void verifyCreateInsertAll() {
        verify(mockedInterpreter).createInsertAll(stringCaptor.capture(), stringArrayCaptor.capture());
    }

    private void verifyCreateInsertSome() {
        verify(mockedInterpreter).createInsertSome(stringCaptor.capture(), mapCaptor.capture());
    }

    private void verifyCreateSelect() {
        verify(mockedInterpreter).createSelect(stringArrayCaptor.capture(), columnAliasListCaptor.capture(), assignedColumnsListCaptor.capture(), whereExpressionArgumentCaptor.capture(), joinListCaptor.capture(), booleanCaptor.capture());
    }

    private void verifyCreateUpdate() {
        verify(mockedInterpreter).createUpdate(stringCaptor.capture(), mapCaptor.capture(), whereExpressionArgumentCaptor.capture());
    }

    @Test
    void createsTableWithOneColumn() throws ISQLParser.UnsupportedCommandException {
        executeQuery("create table TABLE (id integer);");

        verifyCreateCreate();
        assertEquals("TABLE", stringCaptor.getValue());
        assertEquals(1, columnListCaptor.getValue().size());
        assertEquals("id", columnListCaptor.getValue().get(0).name);
        assertEquals(COLUMN_TYPE.INTEGER, columnListCaptor.getValue().get(0).type.getBaseType());
    }

    @Test
    void parsesNotNullClauseWithSimpleType() throws ISQLParser.UnsupportedCommandException {
        executeQuery("create table TABLE (id integer not null);");

        verifyCreateCreate();
        assertEquals("TABLE", stringCaptor.getValue());
        assertEquals(1, columnListCaptor.getValue().size());
        Column column = columnListCaptor.getValue().get(0);
        assertEquals("id", column.name);
        assertEquals(COLUMN_TYPE.INTEGER, column.type.getBaseType());
        assertFalse(column.isNullable());
    }

    @Test
    void parsesNotNullClauseWithComplexType() throws ISQLParser.UnsupportedCommandException {
        executeQuery("create table TABLE (id varchar(255) not null);");

        verifyCreateCreate();
        assertEquals("TABLE", stringCaptor.getValue());
        assertEquals(1, columnListCaptor.getValue().size());
        Column column = columnListCaptor.getValue().get(0);

        assertEquals("id", column.name);
        assertEquals(COLUMN_TYPE.VARCHAR, column.type.getBaseType());
        assertEquals( Long.valueOf(255), column.type.getLength());
        assertFalse(column.isNullable());
    }

    @Test
    void createsTableWithManyColumns() throws ISQLParser.UnsupportedCommandException {
        executeQuery("create table TABLE (id integer, name varchar(42), status bit);");

        verifyCreateCreate();

        List<Column> columnsArgument = columnListCaptor.getValue();
        assertEquals("TABLE", stringCaptor.getValue());
        assertEquals(3, columnsArgument.size());
        assertEquals("id", columnsArgument.get(0).name);
        assertEquals(COLUMN_TYPE.INTEGER, columnsArgument.get(0).type.getBaseType());
        assertEquals("name", columnsArgument.get(1).name);
        assertEquals(COLUMN_TYPE.VARCHAR, columnsArgument.get(1).type.getBaseType());
        assertEquals(Long.valueOf(42), columnsArgument.get(1).type.getLength());
        assertEquals("status", columnsArgument.get(2).name);
        assertEquals(COLUMN_TYPE.BIT, columnsArgument.get(2).type.getBaseType());
    }

    @Test
    void createTableWithIndex() throws ISQLParser.UnsupportedCommandException {
        executeQuery("create table X (id integer, index index_name (id));");

        verifyCreateCreate();
        assertEquals(1, columnListCaptor.getValue().size());
        Column column = columnListCaptor.getValue().get(0);
        assertEquals("id", column.name);
        assertTrue(column.isIndexed());
    }

    @Test
    void createsTableWithPrimaryKey() throws ISQLParser.UnsupportedCommandException {
        executeQuery("create table A (a integer, b integer, primary key (a));");

        verifyCreateCreate();

        assertEquals(2, columnListCaptor.getValue().size());

        Column column = columnListCaptor.getValue().get(0);
        assertEquals("a", column.name);
        assertTrue(column.isPrimary());

        column = columnListCaptor.getValue().get(1);
        assertEquals("b", column.name);
        assertFalse(column.isPrimary());
    }

    @Test
    void parsesPrimaryKeyAndIndex() throws ISQLParser.UnsupportedCommandException {
        executeQuery("create table Word (WORD_ID integer(11) not null, word varchar(255), primary key (WORD_ID), index word_index (word));");

        verifyCreateCreate();

        assertEquals(2, columnListCaptor.getValue().size());

        Column column = columnListCaptor.getValue().get(0);
        assertEquals("WORD_ID", column.name);
        assertTrue(column.isPrimary());
        assertFalse(column.isIndexed());

        column = columnListCaptor.getValue().get(1);
        assertEquals("word", column.name);
        assertFalse(column.isPrimary());
        assertTrue(column.isIndexed());
    }

    @Test
    void createsTableWithCompositePrimaryKey() throws ISQLParser.UnsupportedCommandException {
        executeQuery("create table A (a integer, b integer, primary key (a, b));");

        verifyCreateCreate();
        assertEquals(2, columnListCaptor.getValue().size());
        Column column = columnListCaptor.getValue().get(0);
        assertEquals("a", column.name);
        assertTrue(column.isPrimary());

        column = columnListCaptor.getValue().get(1);
        assertEquals("b", column.name);
        assertTrue(column.isPrimary());
    }

    @Test
    void parsesForeignKeyConstraint() throws ISQLParser.UnsupportedCommandException {
        executeQuery("create table A (a integer, constraint myConstraint foreign key (a) references B (someColumn));");

        verifyCreateCreate();
        assertEquals(1, columnListCaptor.getValue().size());
        Column column = columnListCaptor.getValue().get(0);
        assertEquals("a", column.name);
        List<Constraint> constraints = column.getConstraints();
        assertEquals(1, constraints.size());
        Constraint constraint = constraints.get(0);
        assertEquals("myConstraint", constraint.getName());
        assertEquals("a", constraint.getOwnColumn());
        assertEquals("B", constraint.getForeignTableName());
        assertEquals("someColumn", constraint.getForeignColumnName());
    }

    @Test
    void parsesMultipleForeignKeyConstraints() throws ISQLParser.UnsupportedCommandException {
        executeQuery(
                "create table A (" +
                "a integer, " +
                "b integer, " +
                "constraint constraintA foreign key (a) references B (someColumn), " +
                "constraint constraintB foreign key (b) references C (someOtherColumn)" +
        ");");

        verifyCreateCreate();
        assertEquals(2, columnListCaptor.getValue().size());

        Column columnA = columnListCaptor.getValue().get(0);
        assertEquals("a", columnA.name);
        List<Constraint> constraints = columnA.getConstraints();
        assertEquals(1, constraints.size());
        Constraint constraint = constraints.get(0);
        assertEquals("constraintA", constraint.getName());
        assertEquals("a", constraint.getOwnColumn());
        assertEquals("B", constraint.getForeignTableName());
        assertEquals("someColumn", constraint.getForeignColumnName());

        Column columnB = columnListCaptor.getValue().get(1);
        assertEquals("b", columnB.name);
        constraints = columnB.getConstraints();
        assertEquals(1, constraints.size());
        constraint = constraints.get(0);
        assertEquals("constraintB", constraint.getName());
        assertEquals("b", constraint.getOwnColumn());
        assertEquals("C", constraint.getForeignTableName());
        assertEquals("someOtherColumn", constraint.getForeignColumnName());
    }

    @Test
    void insertValues() throws ISQLParser.UnsupportedCommandException {
        executeQuery("insert into TEST_TABLE values (2, \"some-text\", \"some other text\");");

        verifyCreateInsertAll();

        assertEquals("TEST_TABLE", stringCaptor.getValue());
        assertArrayEquals(new String[]{"2", "some-text", "some other text"}, stringArrayCaptor.getValue());
    }

    @Test
    void insertWithParameters() throws ISQLParser.UnsupportedCommandException {
        Queue<Object> parameters = new LinkedList<>();
        parameters.add(1);
        parameters.add("hi");
        executeStatement("insert into X values (?, ?);", parameters);

        verify(mockedInterpreter).createInsertAll(stringCaptor.capture(), objectArrayCaptor.capture());

        assertEquals("X", stringCaptor.getValue());
        assertArrayEquals(new Object[]{1, "hi"}, objectArrayCaptor.getValue());
    }

    @Test
    void insertValuesWithUnmatchedParenthesisInStrings() throws ISQLParser.UnsupportedCommandException {
        executeQuery("insert into X values (\"foo(bar\");");

        verifyCreateInsertAll();

        assertEquals("X", stringCaptor.getValue());
        assertArrayEquals(new String[]{"foo(bar"}, stringArrayCaptor.getValue());
    }

    @Test
    void insertValuesWithSingleQuoteSymbolInStrings() throws ISQLParser.UnsupportedCommandException {
        executeQuery("insert into X values (\"foo'bar\");");

        verifyCreateInsertAll();

        assertEquals("X", stringCaptor.getValue());
        assertArrayEquals(new String[]{"foo'bar"}, stringArrayCaptor.getValue());
    }

    @Test
    void insertsValuesWithExclamations() throws ISQLParser.UnsupportedCommandException {
        executeQuery("insert into X values (\"!!!!\");");

        verifyCreateInsertAll();

        assertEquals("X", stringCaptor.getValue());
        assertArrayEquals(new String[]{"!!!!"}, stringArrayCaptor.getValue());
    }

    @Test
    void insertSomeValues() throws ISQLParser.UnsupportedCommandException {
        executeQuery("insert into TEST_TABLE (x, y) values (200, \"some-text\");");

        verifyCreateInsertSome();

        assertEquals("TEST_TABLE", stringCaptor.getValue());
        assertEquals("200", mapCaptor.getValue().get("x"));
        assertEquals("some-text", mapCaptor.getValue().get("y"));
    }

    @Test
    void selectAll() throws ISQLParser.UnsupportedCommandException {
        executeQuery("select * from TEST_TABLE;");

        verifyCreateSelect();

        assertArrayEquals(new String[]{"TEST_TABLE"}, stringArrayCaptor.getValue());
        assertEquals("*", columnAliasListCaptor.getValue().get(0).getName());
        assertEquals(0, assignedColumnsListCaptor.getValue().size());
    }

    @Test
    void selectWithParameters() throws ISQLParser.UnsupportedCommandException {
        Queue<Object> parameters = new LinkedList<>();
        parameters.add(1);
        executeStatement("select X.x from X where X.x=?;", parameters);

        verifyCreateSelect();

        Row row = new Row("X.x");
        row.addValues(1);

        WhereExpression whereExpression = whereExpressionArgumentCaptor.getValue();
        assertTrue(whereExpression.interpret(row));
    }

    @Test
    void selectWithWhereCheckingExclamationMarks() throws ISQLParser.UnsupportedCommandException {
        executeQuery("select x from X where X.x=\"!!!\";");

        verifyCreateSelect();

        WhereExpression whereExpression = whereExpressionArgumentCaptor.getValue();

        Map<String, Object> rawRow = new HashMap<>();
        rawRow.put("X.x", "!!!");

        assertTrue(whereExpression.interpret(new Row(rawRow)));
    }

    @Test
    void select() throws ISQLParser.UnsupportedCommandException {
        executeQuery("select id, name from TEST_TABLE;");

        verifyCreateSelect();

        assertArrayEquals(new String[]{"TEST_TABLE"}, stringArrayCaptor.getValue());
        assertEquals("id", columnAliasListCaptor.getValue().get(0).getName());
        assertEquals("name", columnAliasListCaptor.getValue().get(1).getName());
        assertEquals(0, assignedColumnsListCaptor.getValue().size());
    }

    @Test
    void selectFromManyTables() throws ISQLParser.UnsupportedCommandException {
        executeQuery("select * from A, B;");

        verifyCreateSelect();

        assertArrayEquals(new String[]{"A", "B"}, stringArrayCaptor.getValue());
        assertEquals("*", columnAliasListCaptor.getValue().get(0).getName());
        assertEquals(0, assignedColumnsListCaptor.getValue().size());
    }

    @Test
    void columnsPrefixedWithTables() throws ISQLParser.UnsupportedCommandException {
        executeQuery("select A.b, c from A, B;");

        verifyCreateSelect();

        assertArrayEquals(new String[]{"A", "B"}, stringArrayCaptor.getValue());
        assertEquals("c", columnAliasListCaptor.getValue().get(0).getName());
        assertEquals(1, assignedColumnsListCaptor.getValue().size());
        String tablePrefix = assignedColumnsListCaptor.getValue().get(0).tableName;
        String prefixedColumn = assignedColumnsListCaptor.getValue().get(0).getColumnsWithAliases().get(0).getName();
        String columnAlias = assignedColumnsListCaptor.getValue().get(0).getColumnsWithAliases().get(0).getAlias();
        assertEquals("A", tablePrefix);
        assertEquals("b", prefixedColumn);
        assertEquals("A.b", columnAlias);
    }

    @Test
    void onePrefixedColumn() throws ISQLParser.UnsupportedCommandException {
        executeQuery("select A.b from A, B;");

        verifyCreateSelect();

        assertArrayEquals(new String[]{"A", "B"}, stringArrayCaptor.getValue());
        assertEquals(0, columnAliasListCaptor.getValue().size());
        assertEquals(1, assignedColumnsListCaptor.getValue().size());
        String tablePrefix = assignedColumnsListCaptor.getValue().get(0).tableName;
        String prefixedColumn = assignedColumnsListCaptor.getValue().get(0).getColumnsWithAliases().get(0).getName();
        assertEquals("A", tablePrefix);
        assertEquals("b", prefixedColumn);
    }

    @Test
    void manyPrefixedColumns() throws ISQLParser.UnsupportedCommandException {
        executeQuery("select A.a, A.b, B.c from A, B;");

        verifyCreateSelect();

        assertArrayEquals(new String[]{"A", "B"}, stringArrayCaptor.getValue());
        assertEquals(0, columnAliasListCaptor.getValue().size());
        assertEquals(2, assignedColumnsListCaptor.getValue().size());

        String tablePrefixA = assignedColumnsListCaptor.getValue().get(0).tableName;
        String prefixedColumnA = assignedColumnsListCaptor.getValue().get(0).getColumnsWithAliases().get(0).getName();
        assertEquals("A", tablePrefixA);
        assertEquals("a", prefixedColumnA);

        String prefixedColumnAa = assignedColumnsListCaptor.getValue().get(0).getColumnsWithAliases().get(1).getName();
        assertEquals("b", prefixedColumnAa);

        String tablePrefixB = assignedColumnsListCaptor.getValue().get(1).tableName;
        String prefixedColumnB = assignedColumnsListCaptor.getValue().get(1).getColumnsWithAliases().get(0).getName();
        assertEquals("B", tablePrefixB);
        assertEquals("c", prefixedColumnB);
    }

    @Test
    void throwsExceptionForUnknownCommands() {
        assertThrows(ISQLParser.UnsupportedCommandException.class, () ->
                parser.parseQuery("select_columns a, b from A;")
        );
    }

    @Test
    void selectWithAliases() throws ISQLParser.UnsupportedCommandException {
        executeQuery("select a as firstColumn, b from A;");

        verifyCreateSelect();

        String[] tables = stringArrayCaptor.getValue();
        assertArrayEquals(new String[]{"A"}, tables);

        assertEquals(0, assignedColumnsListCaptor.getValue().size());
        List<Alias> aliasedColumns = columnAliasListCaptor.getValue();

        assertEquals(2, aliasedColumns.size());
        assertEquals("a", aliasedColumns.get(0).getName());
        assertEquals("firstColumn", aliasedColumns.get(0).getAlias());
        assertEquals("b", aliasedColumns.get(1).getName());
        assertEquals("b", aliasedColumns.get(1).getAlias());
    }

    @Test
    void selectWithAliasesFromTwoTables() throws ISQLParser.UnsupportedCommandException {
        executeQuery("select a as firstColumn, b, B.c as thirdColumn from A, B;");

        verifyCreateSelect();

        String[] tables = stringArrayCaptor.getValue();
        assertArrayEquals(new String[]{"A", "B"}, tables);

        List<TableWithAssignedColumns> assignedColumns = assignedColumnsListCaptor.getValue();
        assertEquals(1, assignedColumns.size());
        assertEquals("B", assignedColumns.get(0).tableName);
        assertEquals(1, assignedColumns.get(0).getColumnsWithAliases().size());
        assertEquals("c", assignedColumns.get(0).getColumnsWithAliases().get(0).getName());
        assertEquals("thirdColumn", assignedColumns.get(0).getColumnsWithAliases().get(0).getAlias());

        List<Alias> aliasedColumns = columnAliasListCaptor.getValue();

        assertEquals(2, aliasedColumns.size());
        assertEquals("a", aliasedColumns.get(0).getName());
        assertEquals("firstColumn", aliasedColumns.get(0).getAlias());
        assertEquals("b", aliasedColumns.get(1).getName());
        assertEquals("b", aliasedColumns.get(1).getAlias());
    }

    @Test
    void selectForUpdate() throws ISQLParser.UnsupportedCommandException {
        executeQuery("select * from X for update;");

        verifyCreateSelect();

        assertTrue(booleanCaptor.getValue());
    }

    @Test
    void parsesWhere() throws ISQLParser.UnsupportedCommandException {
        executeQuery("select * from X where X.a > 0");

        verifyCreateSelect();

        WhereExpression parsedExpression = whereExpressionArgumentCaptor.getValue();

        Row row = new Row("X.a");
        row.addValues(10);

        assertTrue(parsedExpression.interpret(row));
    }

    @Test
    void parsesWhereWithDotNotation() throws ISQLParser.UnsupportedCommandException {
        executeQuery("select X.z as column from X where X.z = 10");

        verifyCreateSelect();

        WhereExpression parsedExpression = whereExpressionArgumentCaptor.getValue();

        Row row = new Row("X.z");
        row.addValues(10);

        assertTrue(parsedExpression.interpret(row));
    }

    @Test
    void parsesWhereWithDotNotationWithoutSpecifiedAlias() throws ISQLParser.UnsupportedCommandException {
        executeQuery("select X.z from X where X.z = 10");

        verifyCreateSelect();

        WhereExpression parsedExpression = whereExpressionArgumentCaptor.getValue();

        Row row = new Row("X.z");
        row.addValues(10);

        assertTrue(parsedExpression.interpret(row));
    }

    @Test
    void createIndex() throws ISQLParser.UnsupportedCommandException {
        executeQuery("create index INDEX_ID on Table (column);");

        verify(mockedInterpreter).createCreateIndex(stringCaptor.capture(), stringCaptor2.capture());

        assertEquals("Table", stringCaptor.getValue());
        assertEquals("column", stringCaptor2.getValue());
    }

    @Test
    void parsesDrop() throws ISQLParser.UnsupportedCommandException {
        executeQuery("drop table A, B;");

        verify(mockedInterpreter).createInterpretDrop(stringListCaptor.capture(), booleanCaptor.capture());

        List<String> droppedTables = stringListCaptor.getValue();

        assertEquals(2, droppedTables.size());
        assertEquals("A", droppedTables.get(0));
        assertEquals("B", droppedTables.get(1));
        assertFalse(booleanCaptor.getValue());
    }

    @Test
    void parsesDropIfExists() throws ISQLParser.UnsupportedCommandException {
        executeQuery("drop table if exists A, B;");

        verify(mockedInterpreter).createInterpretDrop(stringListCaptor.capture(), booleanCaptor.capture());

        List<String> droppedTables = stringListCaptor.getValue();

        assertEquals(2, droppedTables.size());
        assertEquals("A", droppedTables.get(0));
        assertEquals("B", droppedTables.get(1));
        assertTrue(booleanCaptor.getValue());
    }

    @Test
    void tableAliases() throws ISQLParser.UnsupportedCommandException {
        executeQuery("select tableAlias1.column as columnAlias1, tableAlias2.column as columnAlias2 from table1 tableAlias1, table2 tableAlias2;");

        String tableName1 = "table1";
        String tableName2 = "table2";
        String columnName = "column";
        String columnAlias1 = "columnAlias1";
        String columnAlias2 = "columnAlias2";

        verifyCreateSelect();

        assertEquals(2, stringArrayCaptor.getValue().length);
        assertEquals(tableName1, stringArrayCaptor.getValue()[0]);
        assertEquals(tableName2, stringArrayCaptor.getValue()[1]);
        assertEquals(0, columnAliasListCaptor.getValue().size());
        assertEquals(2, assignedColumnsListCaptor.getValue().size());

        TableWithAssignedColumns tableWithAssignedColumns1 = assignedColumnsListCaptor.getValue().get(0);
        assertEquals(tableName1, tableWithAssignedColumns1.tableName);
        assertEquals(1, tableWithAssignedColumns1.getColumnsWithAliases().size());
        assertEquals(columnName, tableWithAssignedColumns1.getColumnsWithAliases().get(0).getName());
        assertEquals(columnAlias1, tableWithAssignedColumns1.getColumnsWithAliases().get(0).getAlias());

        TableWithAssignedColumns tableWithAssignedColumns2 = assignedColumnsListCaptor.getValue().get(1);
        assertEquals(tableName2, tableWithAssignedColumns2.tableName);
        assertEquals(1, tableWithAssignedColumns2.getColumnsWithAliases().size());
        assertEquals(columnName, tableWithAssignedColumns2.getColumnsWithAliases().get(0).getName());
        assertEquals(columnAlias2, tableWithAssignedColumns2.getColumnsWithAliases().get(0).getAlias());

        assertTrue(whereExpressionArgumentCaptor.getValue() instanceof TrueExpression);
    }

    @Test
    void joins() throws ISQLParser.UnsupportedCommandException {
        executeQuery("select table.a as column from X table inner join Y table2 on table.id=table2.x_id where table2.active=1");

        verifyCreateSelect();

        assertEquals(1, stringArrayCaptor.getValue().length);
        assertEquals("X", stringArrayCaptor.getValue()[0]);

        assertEquals(0, columnAliasListCaptor.getValue().size());

        assertEquals(1, assignedColumnsListCaptor.getValue().size());
        assertEquals("X", assignedColumnsListCaptor.getValue().get(0).tableName);
        assertEquals(1, assignedColumnsListCaptor.getValue().get(0).getColumnsWithAliases().size());
        assertEquals("a", assignedColumnsListCaptor.getValue().get(0).getColumnsWithAliases().get(0).getName());
        assertEquals("column", assignedColumnsListCaptor.getValue().get(0).getColumnsWithAliases().get(0).getAlias());

        assertEquals(1, joinListCaptor.getValue().size());
        assertEquals("Y", joinListCaptor.getValue().get(0).getTable().getName());

        WhereExpression joinWhereExpression = joinListCaptor.getValue().get(0).getWhereExpression();
        Row row = new Row("X.id", "Y.x_id");
        row.addValues(3, 3);
        assertTrue(joinWhereExpression.interpret(row));
        row = new Row("X.id", "Y.x_id");
        row.addValues(3, 2);
        assertFalse(joinWhereExpression.interpret(row));

        WhereExpression whereExpression = whereExpressionArgumentCaptor.getValue();
        row = new Row("Y.active");
        row.addValues(1);
        assertTrue(whereExpression.interpret(row));
        row = new Row("Y.active");
        row.addValues(0);
        assertFalse(whereExpression.interpret(row));
    }

    @Test
    void update() throws ISQLParser.UnsupportedCommandException {
        executeQuery("update X set word=\"one\" where word=\"two\";");

        verifyCreateUpdate();

        assertEquals("X", stringCaptor.getValue());
        assertEquals("one", mapCaptor.getValue().get("word"));

        WhereExpression whereExpression = whereExpressionArgumentCaptor.getValue();

        Row row = new Row("X.word");
        row.addValues("two");
        assertTrue(whereExpression.interpret(row));
    }

    @Test
    void updateWithParameters() throws ISQLParser.UnsupportedCommandException {
        Queue<Object> parameters = new LinkedList<>();
        parameters.add("one");
        parameters.add("two");
        String query = "update X set word=? where word=?";
        executeStatement(query, parameters);

        verifyCreateUpdate();

        assertEquals("X", stringCaptor.getValue());
        assertEquals("one", mapCaptor.getValue().get("word"));

        WhereExpression whereExpression = whereExpressionArgumentCaptor.getValue();

        Row row = new Row("X.word");
        row.addValues("two");
        assertTrue(whereExpression.interpret(row));
    }

    @Test
    void multiValueParametrisedUpdate() throws ISQLParser.UnsupportedCommandException {
        Queue<Object> parameters = new LinkedList<>();
        parameters.add(1);
        parameters.add(2);
        parameters.add("two");
        String query = "update X set size_a=?, size_b=? where word=?";
        executeStatement(query, parameters);

        verifyCreateUpdate();

        assertEquals("X", stringCaptor.getValue());
        assertEquals(1, mapCaptor.getValue().get("size_a"));
        assertEquals(2, mapCaptor.getValue().get("size_b"));

        WhereExpression whereExpression = whereExpressionArgumentCaptor.getValue();

        Row row = new Row("X.word");
        row.addValues("two");
        assertTrue(whereExpression.interpret(row));
    }

    @Test
    void dumpCommand() throws ISQLParser.UnsupportedCommandException {
        executeQuery("dump /var/db.out;");

        verify(mockedInterpreter).createDump(stringCaptor.capture());

        assertEquals("/var/db.out", stringCaptor.getValue());
    }

    @Test
    void deleteCommand() throws ISQLParser.UnsupportedCommandException {
        executeQuery("delete from myTable where x=1");

        verify(mockedInterpreter).createDelete(stringCaptor.capture(), whereExpressionArgumentCaptor.capture());

        WhereExpression whereExpression = whereExpressionArgumentCaptor.getValue();

        assertEquals("myTable", stringCaptor.getValue());

        Row row = new Row("myTable.x");
        row.addValues(1);
        assertTrue(whereExpression.interpret(row));
    }

    @Test
    void deleteWithParameters() throws ISQLParser.UnsupportedCommandException {
        Queue<Object> parameters = new LinkedList<>();
        parameters.add(1);

        executeStatement("delete from myTable where x=?", parameters);

        verify(mockedInterpreter).createDelete(stringCaptor.capture(), whereExpressionArgumentCaptor.capture());

        WhereExpression whereExpression = whereExpressionArgumentCaptor.getValue();

        assertEquals("myTable", stringCaptor.getValue());

        Row row = new Row("myTable.x");
        row.addValues(1);
        assertTrue(whereExpression.interpret(row));
    }
}
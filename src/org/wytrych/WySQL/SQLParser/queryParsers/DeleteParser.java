package org.wytrych.WySQL.SQLParser.queryParsers;

import org.wytrych.WySQL.Commands.Command;
import org.wytrych.WySQL.Commands.ICommandFactory;
import org.wytrych.WySQL.SQLParser.KeywordTarget;
import org.wytrych.WySQL.SQLParser.KeywordTypes;
import org.wytrych.WySQL.SQLParser.StringParser;
import org.wytrych.WySQL.SQLParser.WhereParser.Lexer;
import org.wytrych.WySQL.SQLParser.WhereParser.WhereParser;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.WhereExpression;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

public class DeleteParser extends QueryParser {
    private StringParser parser;
    private Queue<Object> parameters;
    private KeywordTarget tableNameGroup;
    private KeywordTarget rawWhereExpression;
    private String tableName;
    private WhereExpression whereExpression;
    private Map<String, String> tableAliasToNameMapping = new HashMap<>();

    public DeleteParser(StringParser parser, Queue<Object> parameters) {
        this.parser = parser;
        this.parameters = parameters;
    }

    @Override
    public Command createCommand(ICommandFactory commandFactory) {
        parseKeywords();
        processTableName();
        processWhere();

        return commandFactory.createDelete(tableName, whereExpression);
    }

    private void parseKeywords() {
        tableNameGroup = new KeywordTarget(KeywordTypes.TABLE_NAMES_LIST);
        rawWhereExpression = new KeywordTarget(KeywordTypes.EXPRESSION);
        parser.parseElements(BLANK_TARGET, tableNameGroup, rawWhereExpression);
    }

    private void processTableName() {
        tableName = tableNameGroup.contents.get(0);
        tableAliasToNameMapping.put(tableName, tableName);
    }

    private void processWhere() {
        Lexer lexer = new Lexer(rawWhereExpression.contents.get(0));
        WhereParser whereParser = new WhereParser(lexer, tableAliasToNameMapping, parameters);
        whereExpression = whereParser.parse();
    }
}

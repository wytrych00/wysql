package org.wytrych.WySQL.SQLParser.queryParsers;

import org.wytrych.WySQL.Commands.Command;
import org.wytrych.WySQL.Commands.ICommandFactory;
import org.wytrych.WySQL.SQLParser.KeywordTarget;
import org.wytrych.WySQL.SQLParser.StringParser;
import org.wytrych.WySQL.SQLParser.WhereParser.Lexer;
import org.wytrych.WySQL.SQLParser.WhereParser.WhereParser;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.WhereExpression;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

import static org.wytrych.WySQL.SQLParser.KeywordTypes.EXPRESSION;
import static org.wytrych.WySQL.SQLParser.KeywordTypes.TABLE_NAMES_LIST;
import static org.wytrych.WySQL.SQLParser.KeywordTypes.WORD;

public class UpdateParser extends QueryParser {
    private final StringParser parser;
    private final Queue<Object> parameters;
    private KeywordTarget tableNameTarget;
    private KeywordTarget newValueTarget;
    private KeywordTarget whereExpressionTarget;
    private String tableName;
    private Object newValue;
    private WhereExpression whereExpression;
    private String columnName;
    private String[] newValueExpression;
    private Map<String, Object> newValuesMapping = new HashMap<>();

    public UpdateParser(StringParser parser, Queue<Object> parameters) {
        this.parser = parser;
        this.parameters = parameters;
    }

    @Override
    public Command createCommand(ICommandFactory commandFactory) {
        parseKeywords();
        parseNewValues();
        parseTablesGroup();
        parseWhere();

        return commandFactory.createUpdate(tableName, newValuesMapping, whereExpression);
    }

    private void parseKeywords() {
        tableNameTarget = new KeywordTarget(WORD);
        newValueTarget = new KeywordTarget(TABLE_NAMES_LIST);
        whereExpressionTarget = new KeywordTarget(EXPRESSION);
        parser.parseElements(tableNameTarget, BLANK_TARGET, newValueTarget, whereExpressionTarget);
    }

    private void parseTablesGroup() {
        tableName = tableNameTarget.contents.get(0);
    }

    private void parseWhere() {
        Map<String, String> tableAliasToNameMapping = new HashMap<>();
        tableAliasToNameMapping.put(tableName, tableName);
        Lexer lexer = new Lexer(whereExpressionTarget.contents.get(0));
        WhereParser whereParser = new WhereParser(lexer, tableAliasToNameMapping, parameters);
        whereExpression = whereParser.parse();
    }

    private void parseNewValues() {
        String[] valuesGroups = newValueTarget.contents.get(0).split(",");

        for (String valueGroup : valuesGroups) {
            newValueExpression = valueGroup.split(String.valueOf(Lexer.EQUALS));
            columnName = newValueExpression[0];
            newValue = newValueExpression[1];
            if (newValue.equals(String.valueOf(Lexer.QUESTION)))
                newValue = parameters.remove();
            else
                newValue = ((String) newValue).replace("\"", "");

            newValuesMapping.put(columnName, newValue);
        }

    }

}

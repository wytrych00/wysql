package org.wytrych.WySQL.SQLParser.queryParsers;

import org.wytrych.WySQL.Commands.Command;
import org.wytrych.WySQL.Commands.ICommandFactory;
import org.wytrych.WySQL.SQLParser.KeywordTarget;
import org.wytrych.WySQL.SQLParser.KeywordTypes;
import org.wytrych.WySQL.SQLParser.StringParser;

import java.util.List;

import static org.wytrych.WySQL.SQLParser.OPERATION.INSERT;

public class DropParser extends QueryParser {
    private final StringParser parser;
    private KeywordTarget ifTarget;
    private KeywordTarget existsTarget;
    private StringParser parserLookahead;
    private StringParser parserToUse;
    private Boolean shouldCheckTableExistence;
    private List<String> tableNames;

    public DropParser(StringParser parser) {
        this.parser = parser;
    }

    @Override
    public Command createCommand(ICommandFactory commandFactory) {
        parseKeywords();
        parseExistsClause();
        parseTablesGroup();

        return commandFactory.createInterpretDrop(tableNames, shouldCheckTableExistence);
    }

    private void parseKeywords() {
        parser.parseElements(BLANK_TARGET);
        parserLookahead = parser.copy();
        ifTarget = new KeywordTarget(KeywordTypes.WORD);
        existsTarget = new KeywordTarget(KeywordTypes.WORD);
        parserLookahead.parseElements(ifTarget, existsTarget);
    }

    private void parseExistsClause() {
        if (ifTarget.contents.get(0).equals("if") && existsTarget.contents.get(0).equals("exists")) {
            parserToUse = parserLookahead;
            shouldCheckTableExistence = true;
        } else {
            parserToUse = parser;
            shouldCheckTableExistence = false;
        }
    }

    private void parseTablesGroup() {
        KeywordTarget tableNamesTarget = new KeywordTarget(KeywordTypes.OPEN_PARAMETERS_LIST);
        parserToUse.parseElements(tableNamesTarget);
        tableNames = getColumnNames(tableNamesTarget, INSERT);
    }

}

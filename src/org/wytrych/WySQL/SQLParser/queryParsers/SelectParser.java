package org.wytrych.WySQL.SQLParser.queryParsers;

import org.wytrych.WySQL.Commands.Command;
import org.wytrych.WySQL.Commands.ICommandFactory;
import org.wytrych.WySQL.SQLParser.*;
import org.wytrych.WySQL.SQLParser.WhereParser.Lexer;
import org.wytrych.WySQL.SQLParser.WhereParser.WhereParser;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.EqualsExpression;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.WhereExpression;
import org.wytrych.WySQL.SQLParser.WhereParser.values.ColumnValue;
import org.wytrych.WySQL.SQLParser.WhereParser.values.DatabaseValue;
import org.wytrych.WySQL.shared.Alias;
import org.wytrych.WySQL.shared.TableWithAssignedColumns;

import java.util.*;
import java.util.stream.Collectors;

import static org.wytrych.WySQL.SQLParser.KeywordTypes.WORD;
import static org.wytrych.WySQL.SQLParser.OPERATION.INSERT;

public class SelectParser extends QueryParser {
    private final StringParser parser;
    private final Queue<Object> parameters;

    private static final String DOT = ".";
    private KeywordTarget joinDefinitionsGroup;
    private KeywordTarget columnNamesGroup;
    private KeywordTarget tableNamesGroup;
    private KeywordTarget rawWhereExpression;
    private List<Alias> aliasedColumns;
    private List<TableWithAssignedColumns> tableWithAssignedColumns;
    private List<Alias> tableAliases;
    private String[] tableIdentifiers;
    private Map<String, String> tableAliasToNameMapping;
    private List<JoinDefinition> joinDefinitionsList;
    private WhereExpression whereExpression;
    private Boolean shouldLockTable;

    public SelectParser(StringParser parser, Queue<Object> parameters) {
        this.parser = parser;
        this.parameters = parameters;
    }

    @Override
    public Command createCommand(ICommandFactory commandFactory) {
        checkForUpdateClause();
        parseKeywords();
        processColumnsGroup();
        processTablesGroup();
        processJoins();
        processWhere();
        translateTableAliasesIntoNames();

        return commandFactory.createSelect(tableIdentifiers, aliasedColumns, tableWithAssignedColumns, whereExpression, joinDefinitionsList, shouldLockTable);
    }

    private void checkForUpdateClause() {
        shouldLockTable =  parser.checkIfExistsAndReplace("for update");
    }

    private void parseKeywords() {
        columnNamesGroup = new KeywordTarget(KeywordTypes.OPEN_PARAMETERS_LIST);
        tableNamesGroup = new KeywordTarget(KeywordTypes.TABLE_NAMES_LIST);
        joinDefinitionsGroup = new KeywordTarget(KeywordTypes.JOINS);
        rawWhereExpression = new KeywordTarget(KeywordTypes.EXPRESSION);
        parser.parseElements(columnNamesGroup, BLANK_TARGET, tableNamesGroup, joinDefinitionsGroup, rawWhereExpression);
    }

    private void processColumnsGroup() {
        List<String> columnNames = getColumnNames(columnNamesGroup, INSERT);
        tableWithAssignedColumns = columnNames
                .stream()
                .filter(name -> name.contains(DOT))
                .map(this::extractTableAndColumn)
                .collect(Collectors.toMap(
                        entry -> entry.tableName,
                        entry -> new ArrayList<>(Collections.singletonList(entry.column)),
                        (a, b) -> {a.addAll(b); return a;}
                ))
                .entrySet()
                .stream()
                .map(TableWithAssignedColumns::new)
                .collect(Collectors.toList());

        aliasedColumns = columnNames
                .stream()
                .filter(name -> !name.contains(DOT))
                .map(this::parseColumnStringIntoColumnAlias)
                .collect(Collectors.toList());
    }

    private void processTablesGroup() {
        List<String> tableNames = getColumnNames(tableNamesGroup, INSERT);
        tableAliases = tableNames.stream()
                .map(this::parseTableAlias)
                .collect(Collectors.toList());

        tableIdentifiers = tableAliases.stream()
                .map(Alias::getName)
                .toArray(String[]::new);

        tableAliasToNameMapping = tableAliases.stream()
                .collect(Collectors.toMap(
                        Alias::getAlias,
                        Alias::getName
                ));
    }

    private void processJoins() {
        joinDefinitionsList = parseJoins();

        for (JoinDefinition joinDefinition : joinDefinitionsList) {
            addTableToTableLists(joinDefinition);
            createJoinWhereExpression(joinDefinition);
        }

    }

    private List<JoinDefinition> parseJoins() {
        return joinDefinitionsGroup.contents.stream()
                .map(this::parseJoinDefinition)
                .collect(Collectors.toList());
    }

    private JoinDefinition parseJoinDefinition(String joinGroup) {
        StringParser parser = new StringParser(joinGroup);
        KeywordTarget tableNameTarget = new KeywordTarget(WORD);
        KeywordTarget tableAliasTarget = new KeywordTarget(WORD);
        KeywordTarget linkTarget = new KeywordTarget(WORD);
        parser.parseElements(tableNameTarget, tableAliasTarget, BLANK_TARGET, linkTarget);
        String[] link = linkTarget.contents.get(0).split("=");
        Alias table = new Alias(tableNameTarget.contents.get(0), tableAliasTarget.contents.get(0));
        return new JoinDefinition(table, link[1], new Alias(link[0]), null);
    }

    private void addTableToTableLists(JoinDefinition joinDefinition) {
        tableAliasToNameMapping.put(joinDefinition.getTable().getAlias(), joinDefinition.getTable().getName());
        tableAliases.add(joinDefinition.getTable());
    }

    private void createJoinWhereExpression(JoinDefinition joinDefinition) {
        String leftColumn = joinDefinition.getOwnColumnName();
        DatabaseValue left = new ColumnValue(leftColumn, tableAliasToNameMapping);
        String rightColumn = joinDefinition.getForeignColumn().getName();
        DatabaseValue right = new ColumnValue(rightColumn, tableAliasToNameMapping);
        WhereExpression joinLink = new EqualsExpression(left, right);
        joinDefinition.setWhereExpression(joinLink);
    }

    private void processWhere() {
        Lexer lexer = new Lexer(rawWhereExpression.contents.get(0));
        WhereParser whereParser = new WhereParser(lexer, tableAliasToNameMapping, parameters);
        whereExpression = whereParser.parse();
    }

    private void translateTableAliasesIntoNames() {
        tableWithAssignedColumns = tableWithAssignedColumns.stream()
                .map(this::translateTableAliasesIntoNames)
                .collect(Collectors.toList());
    }

    private TableWithAssignedColumns translateTableAliasesIntoNames(TableWithAssignedColumns table) {
        for (Alias tableAlias : tableAliases)
            if (tableAlias.getAlias().equals(table.tableName))
                return new TableWithAssignedColumns(tableAlias.getName(), table.getColumnsWithAliases());

        return table;
    }

}

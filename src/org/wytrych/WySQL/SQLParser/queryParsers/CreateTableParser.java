package org.wytrych.WySQL.SQLParser.queryParsers;

import org.wytrych.WySQL.Commands.Command;
import org.wytrych.WySQL.Commands.ICommandFactory;
import org.wytrych.WySQL.SQLParser.KeywordTarget;
import org.wytrych.WySQL.SQLParser.KeywordTypes;
import org.wytrych.WySQL.SQLParser.StringParser;
import org.wytrych.WySQL.shared.COLUMN_TYPE;
import org.wytrych.WySQL.shared.Column;
import org.wytrych.WySQL.shared.ColumnType;
import org.wytrych.WySQL.shared.Constraint;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.wytrych.WySQL.SQLParser.KeywordTypes.PARAMETERS;
import static org.wytrych.WySQL.SQLParser.KeywordTypes.WORD;
import static org.wytrych.WySQL.SQLParser.OPERATION.CREATE;

public class CreateTableParser extends QueryParser {
    private final StringParser parser;
    private static final String indexCommandRegex = "^index.*";
    private static final String primaryKeyCommandRegex = "primary key[ ]?\\([^)]*\\),?";
    private static final String constraintCommandRegex = "constraint [^)]*\\)[^)]*\\)";
    private KeywordTarget tableNameTarget;
    private KeywordTarget columnsParameters;
    private List<Column> columns;
    private String tableName;
    private List<Constraint> constraints = new ArrayList<>();

    CreateTableParser(StringParser parser) {
        this.parser = parser;
    }

    @Override
    public Command createCommand(ICommandFactory commandFactory) {
        parseKeywords();
        parseColumnsGroup();
        parseTableGroup();

        return commandFactory.createCreate(tableName, columns);
    }

    private void parseKeywords() {
        tableNameTarget = new KeywordTarget(WORD);
        columnsParameters = new KeywordTarget(PARAMETERS);
        parser.parseElements(tableNameTarget, columnsParameters);
    }

    private void parseColumnsGroup() {
        String primaryKeyCommand = extractAndRemovePrimaryKeyCommand();
        extractAndRemoveConstraintCommands();

        List<String> columnNames = getColumnNames(columnsParameters, CREATE);
        columns = columnNames
                .stream()
                .filter(s -> !s.matches(indexCommandRegex))
                .map(this::parseColumnsWithTypes)
                .collect(Collectors.toList());

        columnNames.stream()
                .filter(s -> s.matches(indexCommandRegex))
                .findFirst()
                .ifPresent(this::setIndexedFlags);

        setPrimaryKeys(primaryKeyCommand);
        setConstraints();
    }

    private String extractAndRemovePrimaryKeyCommand() {
        String columnInformation = columnsParameters.contents.get(0);
        Pattern primaryKeyPattern = Pattern.compile(primaryKeyCommandRegex);
        Matcher matcher = primaryKeyPattern.matcher(columnInformation);

        String outTarget = "";
        if (!matcher.find())
            return outTarget;

        outTarget = matcher.group();
        String clearedParameters = columnsParameters.contents.get(0).replaceFirst(primaryKeyCommandRegex, "");
        columnsParameters.replaceContents(List.of(clearedParameters));

        return outTarget;
    }

    private void extractAndRemoveConstraintCommands() {
        String columnInformation = columnsParameters.contents.get(0);
        Pattern primaryKeyPattern = Pattern.compile(constraintCommandRegex);
        Matcher matcher = primaryKeyPattern.matcher(columnInformation);

        while (matcher.find())
            parseConstraint(matcher.group());
    }

    private void parseConstraint(String rawConstraint) {
        StringParser singleConstraintParser = new StringParser(rawConstraint);

        KeywordTarget constraintNameTarget = new KeywordTarget(WORD);
        KeywordTarget ownColumnNameTarget = new KeywordTarget(PARAMETERS);
        KeywordTarget foreignTableNameTarget = new KeywordTarget(WORD);
        KeywordTarget foreignColumnNameTarget = new KeywordTarget(PARAMETERS);

        singleConstraintParser.parseElements(BLANK_TARGET, constraintNameTarget, BLANK_TARGET, BLANK_TARGET, ownColumnNameTarget, BLANK_TARGET, foreignTableNameTarget, foreignColumnNameTarget);

        String constraintName = constraintNameTarget.contents.get(0);
        String ownColumnName = getColumnNames(ownColumnNameTarget, CREATE).get(0);
        String foreignTableName = foreignTableNameTarget.contents.get(0);
        String foreignColumnName = getColumnNames(foreignColumnNameTarget, CREATE).get(0);

        Constraint constraint = new Constraint(Constraint.ConstraintType.FOREIGN_KEY, constraintName, ownColumnName, foreignTableName, foreignColumnName);
        constraints.add(constraint);

        String clearedParameters = columnsParameters.contents.get(0).replaceFirst(constraintCommandRegex, "");
        columnsParameters.replaceContents(List.of(clearedParameters));
    }

    private void setIndexedFlags(String indexedColumns) {
        List<String> indexedColumnsNames = getIndexedColumnsNames(indexedColumns);
        columns.forEach(column -> {
            Integer index = indexedColumnsNames.indexOf(column.name);
            if (index != -1)
                column.setIndexed(true);
        });
    }

    private List<String> getIndexedColumnsNames(String indexCommand) {
        StringParser indexCommandParser = new StringParser(indexCommand);
        KeywordTarget indexedColumns = new KeywordTarget(PARAMETERS);
        indexCommandParser.parseElements(BLANK_TARGET, BLANK_TARGET, indexedColumns);
        return getColumnNames(indexedColumns, CREATE);
    }

    private void setPrimaryKeys(String primaryKeyDefinition) {
        if (primaryKeyDefinition.length() == 0)
            return;
        List<String> primaryKeyColumn = parsePrimaryKeyNames(primaryKeyDefinition);
        columns.forEach(column -> {
            if (primaryKeyColumn.indexOf(column.name) != -1)
                column.setPrimaryKey(true);
        });
    }

    private void setConstraints() {
        for (Constraint constraint : constraints)
            for (Column column : columns)
                if (column.name.equals(constraint.getOwnColumn()))
                    column.addConstraint(constraint);
    }

    private List<String> parsePrimaryKeyNames(String primaryKeyDefinition) {
        StringParser primaryKeyDefinitionParser = new StringParser(primaryKeyDefinition);
        KeywordTarget primaryKeyColumn = new KeywordTarget(PARAMETERS);
        primaryKeyDefinitionParser.parseElements(BLANK_TARGET, BLANK_TARGET, primaryKeyColumn);
        return getColumnNames(primaryKeyColumn, CREATE);
    }

    private Column parseColumnsWithTypes(String columnWithType) {
        String notNullClause = " not null";
        Boolean isNullable = true;
        if (columnWithType.contains(notNullClause)) {
            isNullable = false;
            columnWithType = columnWithType.replace(notNullClause, "");
        }

        StringParser columnWithTypeParser = new StringParser(columnWithType);
        KeywordTarget columnName = new KeywordTarget(KeywordTypes.WORD);
        KeywordTarget typeDescription = new KeywordTarget(KeywordTypes.WORD_WITH_PARAMETER);
        columnWithTypeParser.parseElements(columnName, typeDescription);

        COLUMN_TYPE columnBaseType = COLUMN_TYPE.valueOf(typeDescription.contents.get(0));
        ColumnType columnType = new ColumnType(columnBaseType, typeDescription.contents.get(1));

        Column column = new Column(columnName.contents.get(0), columnType);
        column.setNullable(isNullable);
        return column;
    }

    private void parseTableGroup() {
        tableName = tableNameTarget.contents.get(0);
    }

}

package org.wytrych.WySQL.SQLParser.queryParsers;

import org.wytrych.WySQL.Commands.Command;
import org.wytrych.WySQL.Commands.ICommandFactory;
import org.wytrych.WySQL.SQLParser.KeywordTarget;
import org.wytrych.WySQL.SQLParser.KeywordTypes;
import org.wytrych.WySQL.SQLParser.StringParser;

public class DumpParser extends QueryParser {
    private final StringParser parser;
    private String path;

    public DumpParser(StringParser parser) {
        super();
        this.parser = parser;
    }

    @Override
    public Command createCommand(ICommandFactory commandFactory) {
        parsePath();
        return commandFactory.createDump(path);
    }

    private void parsePath() {
        KeywordTarget pathTarget = new KeywordTarget(KeywordTypes.EXPRESSION);
        parser.parseElements(pathTarget);
        path = pathTarget.contents.get(0);
    }
}

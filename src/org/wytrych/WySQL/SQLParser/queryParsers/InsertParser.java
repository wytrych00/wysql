package org.wytrych.WySQL.SQLParser.queryParsers;

import org.wytrych.WySQL.Commands.Command;
import org.wytrych.WySQL.Commands.ICommandFactory;
import org.wytrych.WySQL.SQLParser.KeywordTarget;
import org.wytrych.WySQL.SQLParser.KeywordTypes;
import org.wytrych.WySQL.SQLParser.StringParser;
import org.wytrych.WySQL.SQLParser.WhereParser.Lexer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.stream.Collectors;

import static org.wytrych.WySQL.SQLParser.OPERATION.INSERT;

public class InsertParser extends QueryParser {
    private final StringParser parser;
    private final Queue<Object> parameters;
    private KeywordTarget tableName;
    private KeywordTarget rawColumnNames;
    private KeywordTarget values;
    private List<String> columnNames;
    private List<Object> columnValues;
    private Map<String, Object> parametrisedRow;
    private String tableNames;

    public InsertParser(StringParser parser, Queue<Object> parameters) {
        this.parser = parser;
        this.parameters = parameters;
    }

    @Override
    public Command createCommand(ICommandFactory commandFactory) {
        parseKeywords();
        parseColumnsGroup();
        parseTablesGroup();

        if (columnNames.size() == 0)
            return commandFactory.createInsertAll(tableNames, columnValues.toArray(new Object[0]));
        return commandFactory.createInsertSome(tableNames, parametrisedRow);
    }

    private void parseKeywords() {
        tableName = new KeywordTarget(KeywordTypes.WORD);
        rawColumnNames = new KeywordTarget(KeywordTypes.PARAMETERS);
        values = new KeywordTarget(KeywordTypes.PARAMETERS);
        parser.parseElements(BLANK_TARGET, tableName, rawColumnNames, BLANK_TARGET, values);
    }

    private void parseColumnsGroup() {
        columnNames = getColumnNames(rawColumnNames, INSERT);
        columnValues = getColumnNames(values, INSERT).stream()
                .map(value -> {
                    if (value.equals(String.valueOf(Lexer.QUESTION)))
                        return parameters.remove();
                    return value;
                })
                .collect(Collectors.toList());

        parametrisedRow = new HashMap<>();
        for (int i = 0; i < columnNames.size(); i++)
            parametrisedRow.put(columnNames.get(i), columnValues.get(i));
    }

    private void parseTablesGroup() {
        tableNames = tableName.contents.get(0);
    }

}

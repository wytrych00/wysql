package org.wytrych.WySQL.SQLParser.queryParsers;

import org.wytrych.WySQL.Commands.Command;
import org.wytrych.WySQL.Commands.ICommandFactory;
import org.wytrych.WySQL.SQLParser.*;
import org.wytrych.WySQL.shared.Alias;

import java.util.Collections;
import java.util.List;

import static org.wytrych.WySQL.SQLParser.KeywordTypes.BLANK;
import static org.wytrych.WySQL.SQLParser.StringParser.AS_DIVIDER;

public abstract class QueryParser {
    public abstract Command createCommand(ICommandFactory commandFactory) throws ISQLParser.UnsupportedCommandException;

    final KeywordTarget BLANK_TARGET = new KeywordTarget(BLANK);
    private static final String DOT = ".";

    final List<String> getColumnNames(KeywordTarget parameters, OPERATION operation) {
        String parametersContents = parameters.contents.get(0);
        if (parametersContents.length() == 0)
            return Collections.emptyList();
        StringParser parameterParser = new StringParser(parametersContents);
        return parameterParser.getParametersValues(operation);
    }

    final Alias parseTableAlias(String tableWithAlias) {
        String[] splitTableWithAlias = tableWithAlias.split(" ");
        String tableName = splitTableWithAlias[0];
        String tableAlias = splitTableWithAlias.length > 1 ? splitTableWithAlias[1] : tableName;
        return new Alias(tableName, tableAlias);
    }

    final AssignedColumn extractTableAndColumn(String prefixedColumnName) {
        String[] splitName = prefixedColumnName.split("\\" + DOT);
        Alias alias = parseColumnStringIntoColumnAlias(splitName[1]);
        if (alias.getName().equals(alias.getAlias()))
            alias = new Alias(alias.getName(), prefixedColumnName);
        return new AssignedColumn(splitName[0], alias);
    }

    final Alias parseColumnStringIntoColumnAlias(String columnString) {
        String[] splitAliasedColumn = columnString.split("\\" + AS_DIVIDER);
        String columnName = splitAliasedColumn[0];
        String alias = (splitAliasedColumn.length > 1) ? splitAliasedColumn[2] : columnName;
        return new Alias(columnName, alias);
    }

}

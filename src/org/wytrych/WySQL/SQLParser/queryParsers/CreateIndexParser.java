package org.wytrych.WySQL.SQLParser.queryParsers;

import org.wytrych.WySQL.Commands.Command;
import org.wytrych.WySQL.Commands.ICommandFactory;
import org.wytrych.WySQL.SQLParser.KeywordTarget;
import org.wytrych.WySQL.SQLParser.StringParser;

import java.util.List;

import static org.wytrych.WySQL.SQLParser.KeywordTypes.PARAMETERS;
import static org.wytrych.WySQL.SQLParser.KeywordTypes.WORD;
import static org.wytrych.WySQL.SQLParser.OPERATION.CREATE;

public class CreateIndexParser extends QueryParser {
    private final StringParser parser;
    private KeywordTarget tableNameTarget;
    private KeywordTarget columns;
    private List<String> columnNames;
    private String tableName;

    CreateIndexParser(StringParser parser) {
        this.parser = parser;
    }

    @Override
    public Command createCommand(ICommandFactory commandFactory) {
        parseKeywords();
        parseColumnGroup();
        parseTableGroup();
        return commandFactory.createCreateIndex(tableName, columnNames.get(0));
    }

    private void parseKeywords() {
        KeywordTarget indexNameTarget = new KeywordTarget(WORD); // Unused
        tableNameTarget = new KeywordTarget(WORD);
        columns = new KeywordTarget(PARAMETERS);
        parser.parseElements(indexNameTarget, BLANK_TARGET, tableNameTarget, columns);
    }

    private void parseColumnGroup() {
        columnNames = getColumnNames(columns, CREATE);
    }

    private void parseTableGroup() {
        tableName = tableNameTarget.contents.get(0);
    }

}

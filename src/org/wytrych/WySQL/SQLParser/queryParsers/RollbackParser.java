package org.wytrych.WySQL.SQLParser.queryParsers;

import org.wytrych.WySQL.Commands.Command;
import org.wytrych.WySQL.Commands.ICommandFactory;
import org.wytrych.WySQL.SQLParser.StringParser;

public class RollbackParser extends QueryParser {
    public RollbackParser(StringParser parser) {
    }

    @Override
    public Command createCommand(ICommandFactory commandFactory) {
        return commandFactory.createRollback();
    }
}

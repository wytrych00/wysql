package org.wytrych.WySQL.SQLParser.queryParsers;

import org.wytrych.WySQL.Commands.Command;
import org.wytrych.WySQL.Commands.ICommandFactory;
import org.wytrych.WySQL.SQLParser.ISQLParser;
import org.wytrych.WySQL.SQLParser.KeywordTarget;
import org.wytrych.WySQL.SQLParser.StringParser;

import static org.wytrych.WySQL.SQLParser.KeywordTypes.WORD;

public class CreateParser extends QueryParser {
    private final StringParser parser;

    public CreateParser(StringParser parser) {
        this.parser = parser;
    }

    @Override
    public Command createCommand(ICommandFactory commandFactory) throws ISQLParser.UnsupportedCommandException {
        KeywordTarget targetObject = new KeywordTarget(WORD);
        parser.parseElements(targetObject);

        String createTarget = targetObject.contents.get(0);
        switch (createTarget) {
            case "index":
                return new CreateIndexParser(parser).createCommand(commandFactory);
            case "table":
                return new CreateTableParser(parser).createCommand(commandFactory);
            default:
                throw new ISQLParser.UnsupportedCommandException("create " + createTarget);
        }
    }
}

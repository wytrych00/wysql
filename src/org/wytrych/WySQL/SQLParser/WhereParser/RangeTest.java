package org.wytrych.WySQL.SQLParser.WhereParser;

import org.junit.jupiter.api.Test;
import org.wytrych.WySQL.SQLParser.WhereParser.values.ColumnValue;
import org.wytrych.WySQL.SQLParser.WhereParser.values.NumberValue;
import org.wytrych.WySQL.SQLParser.WhereParser.values.StringValue;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.wytrych.WySQL.SQLParser.WhereParser.Range.EMPTY_RANGE;

class RangeTest {

    @Test
    void createIntersectionWhenSectionsOverlap() {
        Range range1 = new Range(new NumberValue(10), new NumberValue(Double.POSITIVE_INFINITY));
        Range range2 = new Range(new NumberValue(Double.NEGATIVE_INFINITY), new NumberValue(15));

        List<Range> combinedRanges = Range.createIntersection(List.of(range1), List.of(range2));

        assertEquals(1, combinedRanges.size());

        Range combinedRange = combinedRanges.get(0);
        assertEquals(10, combinedRange.getMin().getValue());
        assertEquals(15, combinedRange.getMax().getValue());

        // Switch order
        combinedRanges = Range.createIntersection(List.of(range2), List.of(range1));

        assertEquals(1, combinedRanges.size());

        combinedRange = combinedRanges.get(0);
        assertEquals(10, combinedRange.getMin().getValue());
        assertEquals(15, combinedRange.getMax().getValue());
    }

    @Test
    void createIntersectionWhenOneSectionIsWithinOther() {
        Range range1 = new Range(new NumberValue(10), new NumberValue(20));
        Range range2 = new Range(new NumberValue(Double.NEGATIVE_INFINITY), new NumberValue(Double.POSITIVE_INFINITY));

        List<Range> combinedRanges = Range.createIntersection(List.of(range1), List.of(range2));

        assertEquals(1, combinedRanges.size());

        Range combinedRange = combinedRanges.get(0);
        assertEquals(10, combinedRange.getMin().getValue());
        assertEquals(20, combinedRange.getMax().getValue());

        // Switch order
        combinedRanges = Range.createIntersection(List.of(range2), List.of(range1));

        assertEquals(1, combinedRanges.size());

        combinedRange = combinedRanges.get(0);
        assertEquals(10, combinedRange.getMin().getValue());
        assertEquals(20, combinedRange.getMax().getValue());
    }

    @Test
    void createIntersectionWhenSectionsAreSeparate() {
        Range range1 = new Range(new NumberValue(0), new NumberValue(20));
        Range range2 = new Range(new NumberValue(30), new NumberValue(50));

        List<Range> combinedRanges = Range.createIntersection(List.of(range1), List.of(range2));

        assertEquals(1, combinedRanges.size());

        Range combinedRange = combinedRanges.get(0);
        assertEquals(EMPTY_RANGE, combinedRange);

        // Switch order
        combinedRanges = Range.createIntersection(List.of(range2), List.of(range1));
        assertEquals(1, combinedRanges.size());

        combinedRange = combinedRanges.get(0);
        assertEquals(EMPTY_RANGE, combinedRange);
    }

    @Test
    void createIntersectionWithMultipleRanges() {
        Range range1 = new Range(new NumberValue(Double.NEGATIVE_INFINITY), new NumberValue(5));
        Range range2 = new Range(new NumberValue(8), new NumberValue(13));
        Range range3 = new Range(new NumberValue(-4), new NumberValue(10));
        Range range4 = new Range(new NumberValue(12), new NumberValue(43));

        List<Range> combinedRanges = Range.createIntersection(List.of(range1, range2), List.of(range3, range4));

        assertEquals(3, combinedRanges.size());

        Range combinedRange = combinedRanges.get(0);
        assertEquals(-4, combinedRange.getMin().getValue());
        assertEquals(5, combinedRange.getMax().getValue());
        combinedRange = combinedRanges.get(1);
        assertEquals(8, combinedRange.getMin().getValue());
        assertEquals(10, combinedRange.getMax().getValue());
        combinedRange = combinedRanges.get(2);
        assertEquals(12, combinedRange.getMin().getValue());
        assertEquals(13, combinedRange.getMax().getValue());
    }

    @Test
    void createTwoRangesWhenTheyAreSeparate() {
        Range range1 = new Range(new NumberValue(1), new NumberValue(1));
        Range range2 = new Range(new NumberValue(3), new NumberValue(3));

        List<Range> combinedRanges = Range.createUnion(List.of(range1), List.of(range2));

        assertEquals(2, combinedRanges.size());

        Range combinedRange = combinedRanges.get(0);
        assertEquals(1, combinedRange.getMin().getValue());
        assertEquals(1, combinedRange.getMax().getValue());

        combinedRange = combinedRanges.get(1);
        assertEquals(3, combinedRange.getMin().getValue());
        assertEquals(3, combinedRange.getMax().getValue());
    }

    @Test
    void mergeRangesWhenAreOverlapping() {
        Range range1 = new Range(new NumberValue(1), new NumberValue(4));
        Range range2 = new Range(new NumberValue(3), new NumberValue(7));

        List<Range> combinedRanges = Range.createUnion(List.of(range1), List.of(range2));

        assertEquals(1, combinedRanges.size());

        Range combinedRange = combinedRanges.get(0);
        assertEquals(1, combinedRange.getMin().getValue());
        assertEquals(7, combinedRange.getMax().getValue());

        combinedRanges = Range.createUnion(List.of(range2), List.of(range1));

        assertEquals(1, combinedRanges.size());

        combinedRange = combinedRanges.get(0);
        assertEquals(1, combinedRange.getMin().getValue());
        assertEquals(7, combinedRange.getMax().getValue());
    }

    @Test
    void combinesPointAndRange() {
        Range range1 = new Range(new NumberValue(10), new NumberValue(Double.POSITIVE_INFINITY));
        Range range2 = new Range(new NumberValue(5), new NumberValue(5));

        List<Range> combinedRanges = Range.createUnion(List.of(range1), List.of(range2));

        assertEquals(2, combinedRanges.size());

        Range combinedRange = combinedRanges.get(0);
        assertEquals(5, combinedRange.getMin().getValue());
        assertEquals(5, combinedRange.getMax().getValue());
        combinedRange = combinedRanges.get(1);
        assertEquals(10, combinedRange.getMin().getValue());
        assertEquals(Double.POSITIVE_INFINITY, combinedRange.getMax().getValue());

        // Switch order
        combinedRanges = Range.createUnion(List.of(range2), List.of(range1));

        assertEquals(2, combinedRanges.size());

        combinedRange = combinedRanges.get(0);
        assertEquals(5, combinedRange.getMin().getValue());
        assertEquals(5, combinedRange.getMax().getValue());
        combinedRange = combinedRanges.get(1);
        assertEquals(10, combinedRange.getMin().getValue());
        assertEquals(Double.POSITIVE_INFINITY, combinedRange.getMax().getValue());
    }

    @Test
    void combineWithEmptyRange() {
        Range range1 = new Range(new NumberValue(10), new NumberValue(Double.POSITIVE_INFINITY));
        List<Range> combinedRanges = Range.createUnion(List.of(range1), List.of(EMPTY_RANGE));

        assertEquals(1, combinedRanges.size());
        Range combinedRange = combinedRanges.get(0);
        assertEquals(10, combinedRange.getMin().getValue());
        assertEquals(Double.POSITIVE_INFINITY, combinedRange.getMax().getValue());
    }

    @Test
    void combinesMultipleRanges() {
        Range range1 = new Range(new NumberValue(-100), new NumberValue(20));
        Range range2 = new Range(new NumberValue(41), new NumberValue(42));
        Range range3 = new Range(new NumberValue(-10), new NumberValue(23));
        Range range4 = new Range(new NumberValue(40), new NumberValue(43));

        List<Range> combinedRanges = Range.createUnion(List.of(range1, range2), List.of(range3, range4));

        assertEquals(2, combinedRanges.size());
        Range combinedRange = combinedRanges.get(0);
        assertEquals(-100, combinedRange.getMin().getValue());
        assertEquals(23, combinedRange.getMax().getValue());
        combinedRange = combinedRanges.get(1);
        assertEquals(40, combinedRange.getMin().getValue());
        assertEquals(43, combinedRange.getMax().getValue());
    }

    @Test
    void checksIfRangeIsFullRange() {
        Range myFullRange = new Range(new NumberValue(Double.NEGATIVE_INFINITY), new NumberValue(Double.POSITIVE_INFINITY));

        assertTrue(Range.isFullRange(myFullRange));
    }

    @Test
    void fullRangeCheckerIsTypeSafe() {
        Range myRange = new Range(new StringValue("aaa"), new ColumnValue("xxx"));

        assertFalse(Range.isFullRange(myRange));
    }

    @Test
    void createsUnionOfAnyRangeAndFullRangeAsFullRange() {
        Range myRange = new Range(new StringValue("aaa"), new StringValue("xxx"));

        List<Range> union = Range.createUnion(List.of(myRange), List.of(Range.FULL_RANGE));

        assertEquals(1, union.size());
        assertTrue(Range.isFullRange(union.get(0)));
    }

    @Test
    void returnsFirstRangeWhenIntersectingWithFullRange() {
        Range myRange = new Range(new StringValue("aaa"), new StringValue("xxx"));

        List<Range> union = Range.createIntersection(List.of(myRange), List.of(Range.FULL_RANGE));

        assertEquals(1, union.size());
        assertEquals(myRange, union.get(0));
    }
}
package org.wytrych.WySQL.SQLParser.WhereParser;

import org.wytrych.WySQL.SQLParser.WhereParser.values.DatabaseValue;
import org.wytrych.WySQL.SQLParser.WhereParser.values.NumberValue;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Range {
    public static final Range EMPTY_RANGE = new Range(new NumberValue(Double.NEGATIVE_INFINITY), new NumberValue(Double.NEGATIVE_INFINITY));
    public static final Range FULL_RANGE = new Range(new NumberValue(Double.NEGATIVE_INFINITY), new NumberValue(Double.POSITIVE_INFINITY));

    public static boolean isFullRange(Range myFullRange) {
        return myFullRange.getMin().compareTo(FULL_RANGE.getMin()) == 0 &&
                myFullRange.getMax().compareTo(FULL_RANGE.getMax()) ==0;
    }

    public static List<Range> createIntersection(List<Range> ranges1, List<Range> ranges2) {
        List<Range> ranges = new ArrayList<>();

        for (Range rangeA : ranges1)
            for (Range rangeB : ranges2)
                ranges = createUnion(ranges, intersect(rangeA, rangeB));

        return ranges;
    }

    private static List<Range> intersect(Range range1, Range range2) {
        if (isFullRange(range1))
            return List.of(range2);
        if (isFullRange(range2))
            return List.of(range1);

        DatabaseValue min;
        DatabaseValue max;

        if (range1.min.compareTo(range2.min) >= 0)
            min = range1.min;
        else
            min = range2.min;

        if (range1.max.compareTo(range2.max) >= 0)
            max = range2.max;
        else
            max = range1.max;

        if (max.compareTo(min) < 0)
            return List.of(EMPTY_RANGE);

        return List.of(new Range(min, max));
    }

    public static List<Range> createUnion(List<Range> ranges1, List<Range> ranges2) {
        List<Range> allRanges = new ArrayList<>();
        allRanges.addAll(ranges1);
        allRanges.addAll(ranges2);

        allRanges.sort(Comparator.comparing(Range::getMin));

        List<Range> combinedRanges = new ArrayList<>();
        combinedRanges.add(EMPTY_RANGE);

        for (Range rangeToAdd : allRanges) {
            Range lastRange = combinedRanges.remove(combinedRanges.size() - 1);
            List<Range> union = unite(lastRange, rangeToAdd);
            combinedRanges.addAll(union);
        }

        return combinedRanges;
    }

    private static List<Range> unite(Range range1, Range range2) {
        if (isFullRange(range1) || isFullRange(range2))
            return List.of(FULL_RANGE);
        if (range1.equals(EMPTY_RANGE))
            return List.of(range2);
        if (range2.equals(EMPTY_RANGE))
            return List.of(range1);

        DatabaseValue min;
        DatabaseValue max;

        if (range1.max.compareTo(range2.min) >= 0 && range1.min.compareTo(range2.max) <= 0) {
            if (range1.min.compareTo(range2.min) >= 0)
                min = range2.min;
            else
                min = range1.min;

            if (range1.max.compareTo(range2.max) >= 0)
                max = range1.max;
            else
                max = range2.max;

            Range combinedRange = new Range(min, max);

            return List.of(combinedRange);
        }

        List<Range> combinedRanges = new ArrayList<>();
        combinedRanges.add(range1);
        combinedRanges.add(range2);

        return combinedRanges;
    }

    public DatabaseValue getMin() {
        return min;
    }

    public DatabaseValue getMax() {
        return max;
    }

    private final DatabaseValue min;
    private final DatabaseValue max;

    public Range(DatabaseValue min, DatabaseValue max) {
        this.min = min;
        this.max = max;
    }
}

package org.wytrych.WySQL.SQLParser.WhereParser;

import org.wytrych.WySQL.SQLParser.WhereParser.expressions.*;
import org.wytrych.WySQL.SQLParser.WhereParser.values.*;

import java.sql.Date;
import java.util.Map;
import java.util.Queue;

import static org.wytrych.WySQL.SQLParser.WhereParser.Lexer.*;

public class WhereParser {
    private Lexer lexer;
    private Queue<Object> parameters;
    private Map<String, String> columnReferenceToAliasMapping;
    private WhereExpression root;
    private Object currentSymbol;

    // Based on: https://unnikked.ga/how-to-build-a-boolean-expression-evaluator-518e9e068a65
    public WhereParser(Lexer lexer, Map<String,String> columnReferenceToAliasMapping) {
        this.lexer = lexer;
        this.columnReferenceToAliasMapping = columnReferenceToAliasMapping;
    }

    public WhereParser(Lexer lexer, Map<String, String> columnReferenceToAliasMapping, Queue<Object> parameters) {
        this.lexer = lexer;
        this.columnReferenceToAliasMapping = columnReferenceToAliasMapping;
        this.parameters = parameters;
    }

    public WhereExpression parse() {
        expression();
        return root;
    }

    private void expression() {
        term();
        while (currentSymbol != null && currentSymbol.equals(OR)) {
            OrExpression orExpression = new OrExpression();
            orExpression.setLeft(root);
            term();
            orExpression.setRight(root);
            root = orExpression;
        }
    }

    private void term() {
        factor();
        while (currentSymbol != null && currentSymbol.equals(AND)) {
            AndExpression andExpression = new AndExpression();
            andExpression.setLeft(root);
            factor();
            andExpression.setRight(root);
            root = andExpression;
        }
    }

    private void factor() {
        currentSymbol = lexer.nextSymbol();

        if (currentSymbol.equals(EOF))
            root = new TrueExpression();
        else if (currentSymbol.equals(OPENING_PAREN))
            expression();
        else
            condition();

        currentSymbol = lexer.nextSymbol();
    }

    private void condition() {
        DatabaseValue left = parseSymbol();
        Character comparison = (char) lexer.nextSymbol();

        currentSymbol = lexer.nextSymbol();
        DatabaseValue right = parseSymbol();

        switch (comparison) {
            case EQUALS:
                root = new EqualsExpression(left, right);
                break;
            case GT:
                root = new GreaterThanExpression(left, right);
                break;
            case GE:
                root = new GreaterThanOrEqualsExpression(left, right);
                break;
            case LT:
                root = new LessThanExpression(left, right);
                break;
            case LE:
                root = new LessThanOrEqualsExpression(left, right);
                break;
        }

    }

    private DatabaseValue parseSymbol() {
        switch (lexer.getCurrentTokenType()) {
            case NumberType:
                return new NumberValue((Number) currentSymbol);
            case StringType:
                return new StringValue(currentSymbol.toString());
            case ColumnType:
                return new ColumnValue(currentSymbol.toString(), columnReferenceToAliasMapping);
            case Parameter:
                Object value = parameters.remove();
                if (value instanceof Number)
                    return new NumberValue((Number) value);
                if (value instanceof String)
                    return new StringValue((String) value);
                if (value instanceof Date)
                    return new DateValue((Date) value);
        }

        return null;
    }
}

package org.wytrych.WySQL.SQLParser.WhereParser;

import org.junit.jupiter.api.Test;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.WhereExpression;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class WhereParserTest {

    private WhereExpression createExpression(String expression) {
        Map<String, String> emptyColumnMapping = Collections.EMPTY_MAP;
        Lexer lexer = new Lexer(expression);
        WhereParser parser = new WhereParser(lexer, emptyColumnMapping);
        return parser.parse();
    }

    @Test
    void createsExpressionsForEqualityCheck() {
        String equality = "x = 5";

        WhereExpression expression = createExpression(equality);

        Row row = new Row("x");
        row.addValues(5);

        assertTrue(expression.interpret(row));
    }

    @Test
    void createsRangeForEqualityCheck() {
        String equality = "x = 5";

        WhereExpression expression = createExpression(equality);

        List<Range> ranges = expression.interpretRange("x");
        assertEquals(1, ranges.size());

        Range range = ranges.get(0);
        Number min = (Number) range.getMin().getValue();
        Number max = (Number) range.getMax().getValue();
        assertEquals(5, min);
        assertEquals(5, max);
    }

    @Test
    void comparesStringValues() {
        String equality = "x = \"foo\"";

        WhereExpression expression = createExpression(equality);

        Row row = new Row("x");
        row.addValues("foo");

        assertTrue(expression.interpret(row));
    }

    @Test
    void createsRangeForStringComparison() {
        String equality = "x = \"foo\"";

        WhereExpression expression = createExpression(equality);

        List<Range> ranges = expression.interpretRange("x");
        assertEquals(1, ranges.size());

        Range range = ranges.get(0);
        String min = (String) range.getMin().getValue();
        String max = (String) range.getMax().getValue();
        assertEquals("foo", min);
        assertEquals("foo", max);
    }

    @Test
    void comparesTwoColumnValues() {
        String equality = "x = y";

        WhereExpression expression = createExpression(equality);

        Row row = new Row("x", "y");
        row.addValues("foo", "foo");

        assertTrue(expression.interpret(row));
    }

    @Test
    void returnsFullRangeForTwoColumnValues() {
        String equality = "a = b";
        WhereExpression expression = createExpression(equality);

        List<Range> ranges = expression.interpretRange("a");
        assertEquals(1, ranges.size());

        Range range = ranges.get(0);
        Number min = (Number) range.getMin().getValue();
        Number max = (Number) range.getMax().getValue();
        assertEquals(Double.NEGATIVE_INFINITY, min);
        assertEquals(Double.POSITIVE_INFINITY, max);
    }

    @Test
    void comparesFlippedPosition() {
        String equality = "5 = x";

        WhereExpression expression = createExpression(equality);

        Row row = new Row("x");
        row.addValues(5);

        assertTrue(expression.interpret(row));
    }

    @Test
    void comparesFloatValues() {
        String equality = "x = 5.5";

        WhereExpression expression = createExpression(equality);

        Row row = new Row("x");
        row.addValues(5.5);

        assertTrue(expression.interpret(row));
    }

    @Test
    void hasGreaterThanComparison() {
        String gt = "x > 10";

        WhereExpression expression = createExpression(gt);

        Row row = new Row("x");
        row.addValues(15);

        assertTrue(expression.interpret(row));
    }

    @Test
    void createsRangeForGraterThanComparison() {
        String gt = "x > 10";

        WhereExpression expression = createExpression(gt);

        List<Range> ranges = expression.interpretRange("x");
        assertEquals(1, ranges.size());

        Range range = ranges.get(0);
        Number min = (Number) range.getMin().getValue();
        Number max = (Number) range.getMax().getValue();
        assertEquals(10, min);
        assertEquals(Double.POSITIVE_INFINITY, max);
    }

    @Test
    void hasLessThanComparison() {
        String lt = "x < 10";

        WhereExpression expression = createExpression(lt);

        Row row = new Row("x");
        row.addValues(2);

        assertTrue(expression.interpret(row));
    }

    @Test
    void createsRangeForLessThanComparison() {
        String gt = "x < 10";

        WhereExpression expression = createExpression(gt);

        List<Range> ranges = expression.interpretRange("x");
        assertEquals(1, ranges.size());

        Range range = ranges.get(0);
        Number min = (Number) range.getMin().getValue();
        Number max = (Number) range.getMax().getValue();
        assertEquals(Double.NEGATIVE_INFINITY, min);
        assertEquals(10, max);
    }

    @Test
    void hasLessThanOrEqualsComparison() {
        String le = "x <= 10";

        WhereExpression expression = createExpression(le);

        Row row = new Row("x");
        row.addValues(10);

        assertTrue(expression.interpret(row));

        row = new Row("x");
        row.addValues(8);

        assertTrue(expression.interpret(row));
    }

    @Test
    void hasGreaterThanOrEqualsComparison() {
        String le = "x >= 10";

        WhereExpression expression = createExpression(le);

        Row row = new Row("x");
        row.addValues(10);

        assertTrue(expression.interpret(row));

        row = new Row("x");
        row.addValues(12);

        assertTrue(expression.interpret(row));
    }

    @Test
    void andCondition() {
        String complexCondition = "x > 10 AND x < 15";

        WhereExpression expression = createExpression(complexCondition);

        Row row = new Row("x");
        row.addValues(13);

        assertTrue(expression.interpret(row));

        row = new Row("x");
        row.addValues(17);

        assertFalse(expression.interpret(row));
    }

    @Test
    void returnsRangeForAndCondition() {
        String complexCondition = "x > 10 AND x < 15";

        WhereExpression expression = createExpression(complexCondition);

        List<Range> ranges = expression.interpretRange("x");
        assertEquals(1, ranges.size());

        Range range = ranges.get(0);
        Number min = (Number) range.getMin().getValue();
        Number max = (Number) range.getMax().getValue();
        assertEquals(10, min);
        assertEquals(15, max);
    }

    @Test
    void orCondition() {
        String complexCondition = "x > 10 OR y = 5";

        WhereExpression expression = createExpression(complexCondition);

        Row row = new Row("x", "y");
        row.addValues(13, 5);

        assertTrue(expression.interpret(row));

        row = new Row("x", "y");
        row.addValues(9, 10);

        assertFalse(expression.interpret(row));

        row = new Row("x", "y");
        row.addValues(11, 10);

        assertTrue(expression.interpret(row));
    }

    @Test
    void returnsRangeForOrCondition() {
        String complexCondition = "x > 10 OR x = 5";

        WhereExpression expression = createExpression(complexCondition);

        List<Range> ranges = expression.interpretRange("x");
        assertEquals(2, ranges.size());

        Range range = ranges.get(0);
        Number min = (Number) range.getMin().getValue();
        Number max = (Number) range.getMax().getValue();
        assertEquals(5, min);
        assertEquals(5, max);

        range = ranges.get(1);
        min = (Number) range.getMin().getValue();
        max = (Number) range.getMax().getValue();
        assertEquals(10, min);
        assertEquals(Double.POSITIVE_INFINITY, max);
    }

    @Test
    void withParenthesis() {
        String complexCondition = "x = 5 or (x < 10 and y = 3)";

        WhereExpression expression = createExpression(complexCondition);

        Row row = new Row("x", "y");
        row.addValues(5, 12);

        assertTrue(expression.interpret(row));

        row = new Row("x", "y");
        row.addValues(7, 3);

        assertTrue(expression.interpret(row));

        row = new Row("x", "y");
        row.addValues(13, 3);

        assertFalse(expression.interpret(row));
    }

    @Test
    void worksWithExclamationMark() {
        String conditionWithExclamationMark = "x=\"!!!\"";

        WhereExpression expression = createExpression(conditionWithExclamationMark);

        Row row = new Row("x");
        row.addValues("!!!");

        assertTrue(expression.interpret(row));
    }

    @Test
    void parsesQueryParameters() {
        String conditionWithParameter = "x=?";

        Map<String, String> emptyColumnMapping = Collections.EMPTY_MAP;
        Queue<Object> parameters = new LinkedList<>();
        parameters.add(10);
        Lexer lexer = new Lexer(conditionWithParameter);
        WhereParser parser = new WhereParser(lexer, emptyColumnMapping, parameters);

        WhereExpression whereExpression = parser.parse();

        Row row = new Row("x");
        row.addValues(10);

        assertTrue(whereExpression.interpret(row));
    }

}
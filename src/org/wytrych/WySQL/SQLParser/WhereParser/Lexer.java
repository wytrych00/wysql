package org.wytrych.WySQL.SQLParser.WhereParser;

import java.io.*;

public class Lexer {
    private StreamTokenizer input;
    final static String EOF = "EOF";
    public final static char EQUALS = '=';
    final static char GT = '>';
    final static char GE = '»';
    final static char LT = '<';
    final static char LE = '«';
    //final static char LE = '≤'; can't use because UTF
    public final static char QUESTION = '?';
    final static char OPENING_PAREN = '(';
    final static char CLOSING_PAREN = ')';
    final static char QUOTE = '"';
    final static char SINGLE_QUOTE = '\'';
    final static char AND = '&';
    final static char OR = '|';
    final static char UNDERSCORE = '_';

    private TokenType currentTokenType = null;

    TokenType getCurrentTokenType() {
        return currentTokenType;
    }

    public Lexer(String inputString) {
        inputString = inputString
                .replace(">=", Character.toString(GE))
                .replace("<=", Character.toString(LE))
                .replaceAll("(?i) and ", Character.toString(AND))
                .replaceAll("(?i) or ", Character.toString(OR));
        Reader reader = new BufferedReader(new StringReader(inputString));

        input = new StreamTokenizer(reader);
        input.resetSyntax();

        input.parseNumbers();
        input.wordChars('a', 'z');
        input.wordChars('A', 'Z');
        input.wordChars('!', '!');
        input.wordChars(UNDERSCORE, UNDERSCORE);
        input.whitespaceChars('\u0000', ' ');
        input.ordinaryChar(EQUALS);
        input.ordinaryChar(GT);
        input.ordinaryChar(GE);
        input.ordinaryChar(LT);
        input.ordinaryChar(LE);
        input.ordinaryChar(OPENING_PAREN);
        input.ordinaryChar(CLOSING_PAREN);
        input.ordinaryChar(QUESTION);
        input.quoteChar(QUOTE);
        input.quoteChar(SINGLE_QUOTE);
    }

    Object nextSymbol() {
        try {
            return parseSymbol();
        } catch (IOException e) {
            return EOF;
        }
    }

    private Object parseSymbol() throws IOException {
        switch (input.nextToken()) {
            case StreamTokenizer.TT_WORD:
                currentTokenType = TokenType.ColumnType;
                return input.sval;
            case SINGLE_QUOTE:
            case QUOTE:
                currentTokenType = TokenType.StringType;
                return input.sval;
            case StreamTokenizer.TT_NUMBER:
                currentTokenType = TokenType.NumberType;
                return input.nval;
            case EQUALS:
            case GT:
            case GE:
            case LT:
            case LE:
            case OPENING_PAREN:
            case CLOSING_PAREN:
            case AND:
            case OR:
                currentTokenType = TokenType.OperatorType;
                return (char) input.ttype;
            case QUESTION:
                currentTokenType = TokenType.Parameter;
                return QUESTION;
            case StreamTokenizer.TT_EOF:
            default:
                return EOF;
        }
    }

}

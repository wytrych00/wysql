package org.wytrych.WySQL.SQLParser.WhereParser;

enum TokenType {
    StringType,
    NumberType,
    ColumnType,
    OperatorType,
    Parameter,
}

package org.wytrych.WySQL.SQLParser.WhereParser.expressions;

import org.junit.jupiter.api.Test;
import org.wytrych.WySQL.SQLParser.WhereParser.values.ColumnValue;
import org.wytrych.WySQL.SQLParser.WhereParser.values.DateValue;
import org.wytrych.WySQL.SQLParser.WhereParser.values.NumberValue;
import org.wytrych.WySQL.SQLParser.WhereParser.Row;
import org.wytrych.WySQL.SQLParser.WhereParser.Range;
import org.wytrych.WySQL.db.IDatabase;

import java.sql.Date;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class WhereExpressionTest {


    @Test
    void checksForEquality() {
        WhereExpression equals = new EqualsExpression(new NumberValue(5), new ColumnValue("column1"));
        Row row1 = new Row("column1");
        row1.addValues(11);
        Row row2 = new Row("column1");
        row2.addValues(5);

        assertFalse(equals.interpret(row1));
        assertTrue(equals.interpret(row2));
    }

    @Test
    void returnsRangeForGraterThanExpression() {
        WhereExpression rangeExample = new GreaterThanExpression(new ColumnValue("column1"), new NumberValue(5));

        List<Range> ranges = rangeExample.interpretRange("column1");
        assertEquals(1, ranges.size());
        Range range = ranges.get(0);
        Number min = (Number) range.getMin().getValue();
        Number max = (Number) range.getMax().getValue();
        assertEquals(5, min);
        assertEquals(Double.POSITIVE_INFINITY, max);
    }

    @Test
    void returnsFullRangeIfColumnNameDoesntMatch() {
        WhereExpression equals = new EqualsExpression(new ColumnValue("column1"), new NumberValue(5));

        List<Range> ranges = equals.interpretRange("column2");
        assertEquals(1, ranges.size());
        Range range = ranges.get(0);

        Number min = (Number) range.getMin().getValue();
        Number max = (Number) range.getMax().getValue();
        assertEquals(Double.NEGATIVE_INFINITY, min);
        assertEquals(Double.POSITIVE_INFINITY, max);
    }

    @Test
    void returnsRangeIfColumnNamesMatch() {
        WhereExpression equals = new EqualsExpression(new ColumnValue("column1"), new NumberValue(5));

        List<Range> ranges = equals.interpretRange("column1");
        assertEquals(1, ranges.size());
        Range range = ranges.get(0);

        Number min = (Number) range.getMin().getValue();
        Number max = (Number) range.getMax().getValue();
        assertEquals(5, min);
        assertEquals(5, max);
    }

    @Test
    void comparesDates() {
        ZonedDateTime savedDate =  LocalDate.parse("11-11-1987", IDatabase.dateFormatter).atStartOfDay().atZone(IDatabase.projectTimeZone);
        Date boundaryDate = new Date(new java.util.Date(savedDate.toInstant().toEpochMilli()).getTime() - 90000000);

        WhereExpression gt = new GreaterThanExpression(new ColumnValue("date"), new DateValue(boundaryDate));

        Row row = new Row("date");
        row.addValues(savedDate);

        assertTrue(gt.interpret(row));
    }

    @Test
    void greaterThanOrEqualExpression() {
        NumberValue number1 = new NumberValue(1);
        NumberValue number2 = new NumberValue(2);

        WhereExpression ge = new GreaterThanOrEqualsExpression(number1, number1);
        assertTrue(ge.interpret(null));

        ge = new GreaterThanOrEqualsExpression(number2, number1);
        assertTrue(ge.interpret(null));

        ge = new GreaterThanOrEqualsExpression(number1, number2);
        assertFalse(ge.interpret(null));
    }

    @Test
    void greaterThanReturnsTrueForNullColumnValues() {
        ColumnValue columnValue = new ColumnValue("A.b");
        NumberValue numberValue = new NumberValue(10);

        WhereExpression g = new GreaterThanExpression(columnValue, numberValue);

        Row context = new Row("X.x");
        context.addValues(0);

        assertTrue(g.interpret(context));
    }

    @Test
    void lessThanReturnsTrueForNullColumnValues() {
        ColumnValue columnValue = new ColumnValue("A.b");
        NumberValue numberValue = new NumberValue(10);

        WhereExpression l = new LessThanExpression(columnValue, numberValue);

        Row context = new Row("X.x");
        context.addValues(20);

        assertTrue(l.interpret(context));
    }
}
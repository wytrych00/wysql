package org.wytrych.WySQL.SQLParser.WhereParser.expressions;

import org.wytrych.WySQL.SQLParser.WhereParser.*;
import org.wytrych.WySQL.SQLParser.WhereParser.values.ColumnValue;
import org.wytrych.WySQL.SQLParser.WhereParser.values.DatabaseValue;
import org.wytrych.WySQL.SQLParser.WhereParser.values.NumberValue;
import org.wytrych.WySQL.SQLParser.WhereParser.Range;

import java.util.List;

public class EqualsExpression implements WhereExpression {
    private final DatabaseValue value1;
    private final DatabaseValue value2;

    public EqualsExpression(DatabaseValue value1, DatabaseValue value2) {
        this.value1 = value1;
        this.value2 = value2;
    }

    @Override
    public Boolean interpret(Row context) {
        value1.passContext(context);
        value2.passContext(context);

        return value1.compareTo(value2) == 0;
    }

    @Override
    public List<Range> interpretRange(String columnName) {
        Range range = Range.FULL_RANGE;
        if (value1 instanceof ColumnValue && !(value2 instanceof ColumnValue)) {
            ColumnValue columnValue = (ColumnValue) value1;
            if (columnValue.columnName.equals(columnName))
                range = new Range(value2, value2);
        }

        return List.of(range);
    }
}

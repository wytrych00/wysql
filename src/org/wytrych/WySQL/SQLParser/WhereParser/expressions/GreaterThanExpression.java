package org.wytrych.WySQL.SQLParser.WhereParser.expressions;

import org.wytrych.WySQL.SQLParser.WhereParser.*;
import org.wytrych.WySQL.SQLParser.WhereParser.values.ColumnValue;
import org.wytrych.WySQL.SQLParser.WhereParser.values.DatabaseValue;
import org.wytrych.WySQL.SQLParser.WhereParser.values.NumberValue;
import org.wytrych.WySQL.SQLParser.WhereParser.Range;

import java.util.List;

public class GreaterThanExpression implements WhereExpression {
    private final DatabaseValue left;
    private final DatabaseValue right;

    public GreaterThanExpression(DatabaseValue left, DatabaseValue right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public Boolean interpret(Row context) {
        left.passContext(context);
        right.passContext(context);

        return left.isNull() || left.compareTo(right) > 0;
    }

    @Override
    public List<Range> interpretRange(String columnName) {
        Range range = Range.FULL_RANGE;
        if (left instanceof ColumnValue && !(right instanceof ColumnValue)) {
            ColumnValue columnValue = (ColumnValue) left;
            if (columnValue.columnName.equals(columnName))
                range = new Range(right, new NumberValue(Double.POSITIVE_INFINITY));
        }
        return List.of(range);
    }
}

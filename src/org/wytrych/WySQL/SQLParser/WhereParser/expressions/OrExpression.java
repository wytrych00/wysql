package org.wytrych.WySQL.SQLParser.WhereParser.expressions;

import org.wytrych.WySQL.SQLParser.WhereParser.Row;
import org.wytrych.WySQL.SQLParser.WhereParser.Range;

import java.util.List;

public class OrExpression implements WhereExpression {
    WhereExpression left;
    WhereExpression right;

    @Override
    public Boolean interpret(Row context) {
        return left.interpret(context) || right.interpret(context);
    }

    @Override
    public List<Range> interpretRange(String columnName) {
        List<Range> ranges1 = left.interpretRange(columnName);
        List<Range> ranges2 = right.interpretRange(columnName);
        return Range.createUnion(ranges1, ranges2);
    }

    public void setLeft(WhereExpression left) {
        this.left = left;
    }

    public void setRight(WhereExpression right) {
        this.right = right;
    }
}

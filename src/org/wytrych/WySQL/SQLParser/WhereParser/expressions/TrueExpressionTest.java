package org.wytrych.WySQL.SQLParser.WhereParser.expressions;

import org.junit.jupiter.api.Test;
import org.wytrych.WySQL.SQLParser.WhereParser.Range;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TrueExpressionTest {

    @Test
    void interpretRange() {
        WhereExpression trueExpression = new TrueExpression();
        List<Range> fullRange = trueExpression.interpretRange("xxx");
        assertEquals(1, fullRange.size());

        Range range = fullRange.get(0);

        assertEquals(Double.NEGATIVE_INFINITY, range.getMin().getValue());
        assertEquals(Double.POSITIVE_INFINITY, range.getMax().getValue());
    }
}
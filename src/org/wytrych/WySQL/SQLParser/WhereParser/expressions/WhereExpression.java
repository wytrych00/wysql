package org.wytrych.WySQL.SQLParser.WhereParser.expressions;

import org.wytrych.WySQL.SQLParser.WhereParser.Row;
import org.wytrych.WySQL.SQLParser.WhereParser.Range;
import org.wytrych.WySQL.SQLParser.WhereParser.values.NumberValue;

import java.util.List;

public interface WhereExpression {
    Boolean interpret(Row context);

    List<Range> interpretRange(String columnName);
}

package org.wytrych.WySQL.SQLParser.WhereParser.expressions;

import org.wytrych.WySQL.SQLParser.WhereParser.values.NumberValue;
import org.wytrych.WySQL.SQLParser.WhereParser.Row;
import org.wytrych.WySQL.SQLParser.WhereParser.Range;

import java.util.List;

public class TrueExpression implements WhereExpression {
    @Override
    public Boolean interpret(Row context) {
        return true;
    }

    @Override
    public List<Range> interpretRange(String columnName) {
        Range range = Range.FULL_RANGE;
        return List.of(range);
    }
}

package org.wytrych.WySQL.SQLParser.WhereParser.values;

import org.wytrych.WySQL.SQLParser.WhereParser.Row;

import java.io.Serializable;

public interface DatabaseValue extends Comparable<DatabaseValue>, Serializable {
    void passContext(Row context);
    Object getValue();
    default Boolean isNull() {
        return false;
    }
}

package org.wytrych.WySQL.SQLParser.WhereParser.values;

import org.junit.jupiter.api.Test;
import org.wytrych.WySQL.SQLParser.WhereParser.Row;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DatabaseValueTest {
    @Test
    void returnsValuesForSimpleValues() {
        Row row = new Row("column1");
        DatabaseValue number = new NumberValue(5);
        number.passContext(row);
        assertEquals(5, number.getValue());
    }

    @Test
    void getsValuesFromRow() {
        DatabaseValue cell = new ColumnValue("column1");
        Row row = new Row("column1");
        row.addValues(11);
        cell.passContext(row);
        assertEquals(11, cell.getValue());
    }

    @Test
    void throwsExceptionWhenComparingNulls() {
        DatabaseValue number = new NumberValue(5);

        assertThrows(NullPointerException.class, () -> number.compareTo(null));
    }

    @Test
    void columnValuesCompare() {
        DatabaseValue cell = new ColumnValue("column1");
        DatabaseValue number = new NumberValue(11.0);
        Row row = new Row("column1");
        row.addValues(11);
        cell.passContext(row);

        assertEquals(0, cell.compareTo(number));
    }

}
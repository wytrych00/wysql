package org.wytrych.WySQL.SQLParser.WhereParser.values;

import org.wytrych.WySQL.SQLParser.WhereParser.Row;

import java.util.List;

public class CompositeDatabaseValue implements DatabaseValue {
    private final List<DatabaseValue> primaryKeyValues;

    public CompositeDatabaseValue(List<DatabaseValue> primaryKeyValues) {
        this.primaryKeyValues = primaryKeyValues;
    }

    @Override
    public void passContext(Row context) {

    }

    @Override
    public Object getValue() {
        return null;
    }

    @Override
    public int compareTo(DatabaseValue o) {
        if (o instanceof CompositeDatabaseValue)
            return compareComposites((CompositeDatabaseValue) o);
        DatabaseValue value = primaryKeyValues.get(0);
        return value.compareTo(o);
    }

    private int compareComposites(CompositeDatabaseValue compared) {
        int comparison = 0;
        for (int i = 0; i < primaryKeyValues.size(); i++) {
            comparison = primaryKeyValues.get(i).compareTo(compared.primaryKeyValues.get(i));
            if (comparison != 0)
                return comparison;
        }

        return comparison;
    }
}

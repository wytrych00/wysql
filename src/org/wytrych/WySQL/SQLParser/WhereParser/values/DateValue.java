package org.wytrych.WySQL.SQLParser.WhereParser.values;

import org.wytrych.WySQL.SQLParser.WhereParser.Row;
import org.wytrych.WySQL.db.IDatabase;

import java.sql.Date;
import java.time.ZonedDateTime;

public class DateValue implements DatabaseValue {
    private final ZonedDateTime value;

    public DateValue(Date value) {
        this.value = value.toLocalDate().atStartOfDay().atZone(IDatabase.projectTimeZone);
    }

    @Override
    public void passContext(Row context) {
    }

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public int compareTo(DatabaseValue o) {
        ZonedDateTime s = (ZonedDateTime) o.getValue();

        return value.compareTo(s);
    }
}
package org.wytrych.WySQL.SQLParser.WhereParser.values;

import org.wytrych.WySQL.SQLParser.WhereParser.Row;

public class StringValue implements DatabaseValue {
    private final String value;

    public StringValue(String value) {
        this.value = value;
    }

    @Override
    public void passContext(Row context) {
    }

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public int compareTo(DatabaseValue o) {
        Object otherValue = o.getValue();

        if (!(otherValue instanceof String))
            return -1;

        String s = (String) otherValue;

        return value.compareTo(s);
    }
}

package org.wytrych.WySQL.SQLParser.WhereParser.values;

import org.wytrych.WySQL.SQLParser.WhereParser.Row;
import org.wytrych.WySQL.db.IDatabase;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.Map;

public class ColumnValue implements DatabaseValue {
    public final String columnName;
    private Object value;

    public ColumnValue(String columnName) {
        this.columnName = columnName;
    }

    public ColumnValue(String columnName, Map<String, String> aliasMapping) {
        if (columnName.contains(".")) {
            String[] splitColumnName = columnName.split("\\.");
            String tableName = aliasMapping.get(splitColumnName[0]);
            String originalColumnName = splitColumnName[1];
            this.columnName = tableName + "." + originalColumnName;
        } else if (aliasMapping.size() == 1) {
            String tableName = aliasMapping.values().iterator().next();
            this.columnName = tableName + "." + columnName;
        } else {
            this.columnName = columnName;
        }
    }

    public void passContext(Row context) {
        value = context.get(columnName);
    }

    @Override
    public int compareTo(DatabaseValue o) {
        if (value == null || o.getValue() == null)
            return 0;
        if (o instanceof NumberValue)
            return o.compareTo(this) * -1;
        if (value instanceof ZonedDateTime) {
            if (o.getValue() instanceof ZonedDateTime)
                return ((ZonedDateTime) value).compareTo((ZonedDateTime) o.getValue());
            ZonedDateTime otherValueAsZDT = ZonedDateTime.now(IDatabase.projectTimeZone);
            try {
                 otherValueAsZDT = LocalDate.parse(o.getValue().toString(), IDatabase.dateFormatter).atStartOfDay().atZone(IDatabase.projectTimeZone);
            } catch (DateTimeParseException ignored) {
            }
            return ((ZonedDateTime) value).compareTo(otherValueAsZDT);
        }

        return value.equals(o.getValue()) ? 0 : 1;
    }

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public Boolean isNull() {
        return value == null;
    }
}

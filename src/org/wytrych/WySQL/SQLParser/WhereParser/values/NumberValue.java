package org.wytrych.WySQL.SQLParser.WhereParser.values;

import org.wytrych.WySQL.SQLParser.WhereParser.Row;

public class NumberValue implements DatabaseValue {
    private final Number value;

    public NumberValue(Number value) {
        if (value.intValue() == value.doubleValue())
            this.value = value.intValue();
        else
            this.value = value;
    }

    @Override
    public void passContext(Row context) {
    }

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public int compareTo(DatabaseValue o) {
        if (!(o.getValue() instanceof Number))
            return -1;

        double comparedValue = ((Number) o.getValue()).doubleValue();

        if (comparedValue == value.doubleValue())
            return 0;

        double comparison = value.doubleValue() - comparedValue;

        if (comparison == 0)
            return 0;
        if (comparison > 0)
            return 1;

        return -1;
    }
}

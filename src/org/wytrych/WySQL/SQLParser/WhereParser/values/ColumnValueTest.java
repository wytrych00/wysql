package org.wytrych.WySQL.SQLParser.WhereParser.values;

import org.junit.jupiter.api.Test;
import org.wytrych.WySQL.SQLParser.WhereParser.Row;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ColumnValueTest {

    @Test
    void usesColumnAliasWhenNameReferencesTable() {
        Map<String, String> aliasMapping = new HashMap<>();
        aliasMapping.put("aliasedTable", "table");

        Map<String, Object> rawRow = new HashMap<>();
        rawRow.put("table.column", 10);
        Row context = new Row(rawRow);

        ColumnValue columnValue = new ColumnValue("aliasedTable.column", aliasMapping);
        columnValue.passContext(context);

        NumberValue comparedValue = new NumberValue(10);

        assertEquals(0, columnValue.compareTo(comparedValue));
    }

    @Test
    void returnsTrueWhenSecondColumnValueIsNull() {
        ColumnValue columnValue1 = new ColumnValue("A");
        ColumnValue columnValue2 = new ColumnValue("B");

        Map<String, Object> rawRow = new HashMap<>();
        rawRow.put("A", 10);
        Row context = new Row(rawRow);

        columnValue1.passContext(context);
        columnValue2.passContext(context);

        assertEquals(0, columnValue1.compareTo(columnValue2));
    }

    @Test
    void returnsTrueWhenFirstColumnValueIsNull() {
        ColumnValue columnValue1 = new ColumnValue("A");
        ColumnValue columnValue2 = new ColumnValue("B");

        Map<String, Object> rawRow = new HashMap<>();
        rawRow.put("B", 10);
        Row context = new Row(rawRow);

        columnValue1.passContext(context);
        columnValue2.passContext(context);

        assertEquals(0, columnValue1.compareTo(columnValue2));
    }

    @Test
    void returnsTrueWhenFirstColumnValueIsNullAndOtherIsANumber() {
        ColumnValue columnValue = new ColumnValue("A");
        DatabaseValue numberValue  = new NumberValue(20);

        Map<String, Object> rawRow = new HashMap<>();
        rawRow.put("B", 10);
        Row context = new Row(rawRow);

        columnValue.passContext(context);

        assertEquals(0, columnValue.compareTo(numberValue));
    }

}
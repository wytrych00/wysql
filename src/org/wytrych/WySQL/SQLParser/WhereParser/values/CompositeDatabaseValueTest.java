package org.wytrych.WySQL.SQLParser.WhereParser.values;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CompositeDatabaseValueTest {

    @Test
    void comparesToSimpleValues() {
        DatabaseValue givenValue = new NumberValue(5);
        DatabaseValue comparedValue = new NumberValue(5);
        CompositeDatabaseValue compositeDatabaseValue = new CompositeDatabaseValue(List.of(givenValue));

        assertEquals(0, compositeDatabaseValue.compareTo(comparedValue));
    }

    @Test
    void comparesMultipleEqualValues() {
        DatabaseValue givenValue1 = new NumberValue(5);
        DatabaseValue givenValue2 = new NumberValue(10);
        DatabaseValue givenValue3 = new NumberValue(2);
        DatabaseValue comparedValue1 = new NumberValue(5);
        DatabaseValue comparedValue2 = new NumberValue(10);
        DatabaseValue comparedValue3 = new NumberValue(2);

        CompositeDatabaseValue compositeDatabaseValue1 = new CompositeDatabaseValue(List.of(givenValue1, givenValue2, givenValue3));
        CompositeDatabaseValue compositeDatabaseValue2 = new CompositeDatabaseValue(List.of(comparedValue1, comparedValue2, comparedValue3));

        assertEquals(0, compositeDatabaseValue1.compareTo(compositeDatabaseValue2));
    }

    @Test
    void comparesMultipleUnequalValues() {
        DatabaseValue givenValue1 = new NumberValue(5);
        DatabaseValue givenValue2 = new NumberValue(12);
        DatabaseValue givenValue3 = new NumberValue(2);
        DatabaseValue comparedValue1 = new NumberValue(5);
        DatabaseValue comparedValue2 = new NumberValue(10);
        DatabaseValue comparedValue3 = new NumberValue(2);

        CompositeDatabaseValue compositeDatabaseValue1 = new CompositeDatabaseValue(List.of(givenValue1, givenValue2, givenValue3));
        CompositeDatabaseValue compositeDatabaseValue2 = new CompositeDatabaseValue(List.of(comparedValue1, comparedValue2, comparedValue3));

        assertEquals(1, compositeDatabaseValue1.compareTo(compositeDatabaseValue2));
    }

}
package org.wytrych.WySQL.SQLParser.WhereParser.values;

import org.wytrych.WySQL.SQLParser.WhereParser.Row;

public class NullValue implements DatabaseValue {
    @Override
    public void passContext(Row context) {
    }

    @Override
    public Object getValue() {
        return null;
    }

    @Override
    public int compareTo(DatabaseValue o) {
        return 0;
    }
}

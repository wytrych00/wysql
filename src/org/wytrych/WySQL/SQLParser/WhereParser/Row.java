package org.wytrych.WySQL.SQLParser.WhereParser;

import org.wytrych.QueryResult.QueryResult;

import java.util.HashMap;
import java.util.Map;

public class Row {
    private final QueryResult.RowAccessor row;
    private String[] columnNames;
    private Map<String, Object> columnValues = new HashMap<>();

    public Row(String... columnNames) {
        this.columnNames = columnNames;
        this.row = null;
    }

    public Row(Map<String, Object> columnValues) {
        this.row = null;
        this.columnValues = columnValues;
    }

    public Row(QueryResult.RowAccessor row) {
        this.row = row;
    }

    public Object get(String columnName) {
        if (row != null)
            return row.get(columnName);
        return columnValues.get(columnName);
    }

    public void addValues(Object... values) {
        for (int i = 0; i < values.length; i++)
            columnValues.put(columnNames[i], values[i]);
    }
}

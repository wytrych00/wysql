package org.wytrych.WySQL.SQLParser;

import org.wytrych.QueryResult.QueryStatement;
import org.wytrych.WySQL.Commands.Command;

public interface ISQLParser {
    Command parseQuery(String s) throws UnsupportedCommandException;

    Command parseStatement(QueryStatement statement) throws UnsupportedCommandException;

    class UnsupportedCommandException extends Exception {
        public final String command;

        public UnsupportedCommandException(String command) {
            this.command = command;
        }
    }
}

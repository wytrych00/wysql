package org.wytrych.WySQL.SQLParser;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StringParser {
    private static final char OPENING_PAREN = '(';
    private static final char CLOSING_PAREN = ')';
    private static final char SPACE = ' ';
    private static final char EOS = '\u0000';
    private static final char SEMICOLON = ';';
    private static final char WHERE_SYMBOL = '^';
    private static final char INNER_JOIN_SYMBOL = '+';
    private static final char COMMA = ',';
    private static final char QUOTE = '"';
    public static final char AS_DIVIDER = '|';
    private static final String EMPTY_STRING = "";
    private String string;
    private int position = 0;
    private char currentChar;


    public StringParser(String string) {
        this.string = string
                .trim()
                .replace(", ", ",")
                //.replace("'", "\"")
                .replace("= ", "=")
                .replace(" as ", AS_DIVIDER + "as" + AS_DIVIDER)
                .replace(" where ", Character.toString(WHERE_SYMBOL))
                .replace("inner join ", Character.toString(INNER_JOIN_SYMBOL));
    }

    public StringParser copy() {
        return new StringParser(string.substring(position));
    }

    public List<String> getParametersValues(OPERATION type) {
        String[] parameterGroups = string.split(String.valueOf(COMMA));
        Stream<String> splitParameters = Arrays.stream(parameterGroups).map(String::trim);
        return extractParameters(type, splitParameters);
    }

    private List<String> extractParameters(OPERATION type, Stream<String> splitParameters) {
        Function<String, String> transformation;

        switch (type) {
            case INSERT:
                transformation = this::stripQuotes;
                break;
            case CREATE:
                transformation = Function.identity();
                break;
            default:
                transformation = this::getFirstWord;
                break;
        }

        return splitParameters.map(transformation).collect(Collectors.toList());
    }

    private String stripQuotes(String quotedString) {
        return quotedString
                .replaceFirst("^'", EMPTY_STRING)
                .replaceFirst("'$", EMPTY_STRING)
                .replace("\"", EMPTY_STRING);
    }

    private String getFirstWord(String pairedString) {
        return pairedString.split(Character.toString(SPACE))[0];
    }

    private char getNextLetter() {
        if (position >= string.length())
            return EOS;
        char currentChar = string.charAt(position++);
        if (currentChar == SEMICOLON)
            return EOS;

        return currentChar;
    }

    public void parseElements(KeywordTarget... targets) {
        for (KeywordTarget target : targets) {
            switch (target.type) {
                case BLANK:
                    getNextWord();
                    break;
                case WORD:
                    target.setContents(getNextWord());
                    break;
                case PARAMETERS:
                    target.setContents(getStatementParameters());
                    break;
                case OPEN_PARAMETERS_LIST:
                    target.setContents(getUnenclosedParameters());
                    break;
                case WORD_WITH_PARAMETER:
                    target.setContents(getColumnTypeWithParameter());
                    break;
                case EXPRESSION:
                    target.setContents(getUpToCharacters(SEMICOLON));
                    break;
                case TABLE_NAMES_LIST:
                    target.setContents(getUpToCharacters(WHERE_SYMBOL, INNER_JOIN_SYMBOL));
                    break;
                case JOINS:
                    if (currentChar != INNER_JOIN_SYMBOL)
                        break;
                    target.setContents(getUpToCharacters(WHERE_SYMBOL).split("\\" + INNER_JOIN_SYMBOL));
                    break;
            }
        }
    }

    private String getStatementParameters() {
        int parensCount = 0;
        boolean isInsideValue = false;
        StringBuilder word = new StringBuilder();

        do {
            currentChar = getNextLetter();

            if (parensCount == 0 && currentChar != OPENING_PAREN)
                return word.toString();

            if (currentChar == QUOTE)
                isInsideValue = !isInsideValue;

            if (currentChar == CLOSING_PAREN && !isInsideValue)
                parensCount--;

            boolean isInsideBrackets = parensCount > 0;
            if (isInsideBrackets)
                word.append(currentChar);

            if (currentChar == OPENING_PAREN && !isInsideValue)
                parensCount++;

        } while (parensCount != 0);

        movePositionUntilNextWord();

        return word.toString();
    }

    private List<String> getColumnTypeWithParameter() {
        String type = getUpToCharacters(OPENING_PAREN).toUpperCase();
        String parameter = getUpToCharacters(CLOSING_PAREN);

        return Arrays.asList(type, parameter);
    }

    private String getUpToCharacters(char... chars) {
        StringBuilder word = new StringBuilder();
        String characters = String.valueOf(chars);
        characters += EOS;

        while (characters.indexOf(currentChar = getNextLetter()) == -1)
            word.append(currentChar);

        return word.toString();
    }

    String getNextWord() {
        return getUpToCharacters(SPACE);
    }

    private void movePositionUntilNextWord() {
        while (position < string.length() && getNextLetter() != SPACE) {}
    }

    private String getUnenclosedParameters() {
        return getNextWord();
    }

    public Boolean checkIfExistsAndReplace(String clause) {
        Pattern p = Pattern.compile(clause);
        Matcher m = p.matcher(string);
        Boolean clauseExists = m.find();
        string = m.replaceAll(EMPTY_STRING);
        return clauseExists;
    }
}

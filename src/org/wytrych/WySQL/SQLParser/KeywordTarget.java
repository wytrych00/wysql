package org.wytrych.WySQL.SQLParser;

import java.util.ArrayList;
import java.util.List;

public class KeywordTarget {
    public List<String> contents = new ArrayList<>();
    KeywordTypes type;

    public KeywordTarget(KeywordTypes type) {
        this.type = type;
    }

    void setContents(String newContents) {
        contents.add(newContents);
    }

    void setContents(String... newContents) {
        for (String contents : newContents)
            setContents(contents);
    }

    void setContents(List<String> newContents) {
        contents.addAll(newContents);
    }

    public void replaceContents(List<String> newContents) {
        contents = newContents;
    }
}

package org.wytrych.WySQL.WyServer;

import org.wytrych.QueryResult.QueryResult;
import org.wytrych.QueryResult.QueryStatement;
import org.wytrych.WySQL.shared.DBAccessor;
import org.wytrych.WySQL.shared.Logger;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

class ConnectionHandler implements Runnable {
    private final DBAccessor dbAccessor;
    private final Socket clientSocket;

    ConnectionHandler(Socket clientSocket, DBAccessor dbAccessor) {
        Logger.log(("ConnectionHandler constructor"));
        this.clientSocket = clientSocket;
        this.dbAccessor = dbAccessor;

        Logger.log(("ConnectionHandler finish"));
    }

    @Override
    public void run() {
        try (
                ObjectInputStream in = new ObjectInputStream(clientSocket.getInputStream());
                ObjectOutputStream out = new ObjectOutputStream(clientSocket.getOutputStream());
        ) {
            handleConnection(in, out);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            dbAccessor.executeQuery("rollback;");
        }
    }

    private void handleConnection(ObjectInputStream in, ObjectOutputStream out) throws IOException, ClassNotFoundException {
        Logger.log("Handing connection: " + Thread.currentThread().getName());
        out.writeObject(new QueryResult("OK", "Welcome to WySQL!", null));

        QueryResult result;
        QueryStatement statement;

        Logger.log("Waiting for input");

        while ((statement = (QueryStatement) in.readObject()) != null && !statement.sql.equals("x")) {
            Logger.log("Read line: " + statement.sql);
            Logger.log("Parameters: ");
            for (Object parameter : statement.parameters)
                Logger.log(parameter.toString());

            result = dbAccessor.executeQuery(statement);

            out.writeObject(result);
            Logger.log(result.status + " : " + result.message + " : " + result.size());
            Logger.log("Waiting for input");
        }

        dbAccessor.executeQuery("rollback;");

        if (!clientSocket.isClosed()) {
            out.writeObject(new QueryResult("CLOSE"));
            clientSocket.close();
        }
        Logger.log("Connection closed: " + Thread.currentThread().getName());
    }
}

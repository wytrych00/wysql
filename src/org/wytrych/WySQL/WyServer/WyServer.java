package org.wytrych.WySQL.WyServer;

import org.wytrych.WySQL.Commands.CommandFactory;
import org.wytrych.WySQL.SQLParser.ISQLParser;
import org.wytrych.WySQL.SQLParser.SQLParser;
import org.wytrych.WySQL.db.Database;
import org.wytrych.WySQL.shared.DBAccessor;
import org.wytrych.WySQL.db.IDatabase;
import org.wytrych.WySQL.shared.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class WyServer {
    public static void main (String... args) throws IOException {
        startDatabase();
        loadSchema();
        startServer();
    }

    private static void loadSchema() {
        String schema = "/song-fetcher-schema.sql";
        try {
            Files.lines(Paths.get(schema))
                    .forEach((sqlCommand) ->{
                        Logger.log(sqlCommand);
                        dbAccessor.executeQuery(sqlCommand);
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static DBAccessor dbAccessor;

    private static void startDatabase() {
        IDatabase db = new Database();
        ISQLParser parser = new SQLParser(new CommandFactory());
        dbAccessor = new DBAccessor(db, parser);
    }

    private static void startServer() throws IOException {
        Integer port = 9009;
        ServerSocket serverSocket = new ServerSocket(port);
        Logger.log("WySQL server listening on port " + port);

        while (true) {
            Logger.log("Waiting for connection");
            new Thread(new ConnectionHandler(serverSocket.accept(), dbAccessor)).start();
        }
    }

}

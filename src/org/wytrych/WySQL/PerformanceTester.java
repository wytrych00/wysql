package org.wytrych.WySQL;

import org.wytrych.QueryResult.QueryResult;
import org.wytrych.WySQL.Commands.CommandFactory;
import org.wytrych.WySQL.SQLParser.ISQLParser;
import org.wytrych.WySQL.SQLParser.SQLParser;
import org.wytrych.WySQL.db.Database;
import org.wytrych.WySQL.shared.DBAccessor;
import org.wytrych.WySQL.db.IDatabase;
import org.wytrych.WySQL.shared.Logger;


public class PerformanceTester {

    private static ISQLParser parser = new SQLParser(new CommandFactory());
    private static IDatabase db = new Database();
    private static DBAccessor dbAccessor = new DBAccessor(db, parser);

    public static void main(String[] args) {
        Long timestamp;
        Long delay;


        executeQuery("create table A (x integer, y integer, index column_index (x))");
        //executeQuery("create table A (x integer, y integer)");

        timestamp = System.currentTimeMillis();

        for (int i = 0; i < 1000000; i++) {
            Long x = Math.round(Math.random() * 1000);
            Long y = Math.round(Math.random() * 1000);
            executeQuery("insert into A values (" + x + ", " + y + ")");
        }

        delay = System.currentTimeMillis() - timestamp;
        Logger.log("Insert time: " + delay);

        timestamp = System.currentTimeMillis();
        QueryResult result = executeQuery("select Z.x from A Z where Z.x > 900");

        delay = System.currentTimeMillis() - timestamp;

        Logger.log("No of rows: " + result.size());
        Logger.log("Select time: " + delay);
    }

    private static QueryResult executeQuery(String sql) {
        return dbAccessor.executeQuery(sql);
    }
}


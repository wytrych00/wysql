package org.wytrych.WySQL.Commands;

import org.wytrych.QueryResult.QueryResult;
import org.wytrych.WySQL.db.IDatabase;

public class CreateIndexCommand implements Command {
    private final String tableName;
    private final String columnName;

    CreateIndexCommand(String tableName, String columnName) {
        this.tableName = tableName;
        this.columnName = columnName;
    }

    @Override
    public QueryResult execute(IDatabase db) {
        return db.createIndex(tableName, columnName);
    }
}

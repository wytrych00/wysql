package org.wytrych.WySQL.Commands;

import org.wytrych.QueryResult.QueryResult;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.WhereExpression;
import org.wytrych.WySQL.db.IDatabase;


public class DeleteCommand implements Command {
    private final String tableName;
    private final WhereExpression whereExpression;

    public DeleteCommand(String tableName, WhereExpression whereExpression) {
        this.tableName = tableName;
        this.whereExpression = whereExpression;
    }

    @Override
    public QueryResult execute(IDatabase db) {
        return db.delete(tableName, whereExpression);
    }
}

package org.wytrych.WySQL.Commands;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.wytrych.QueryResult.QueryResult;
import org.wytrych.WySQL.QueryExceptions.ClashingColumnException;
import org.wytrych.WySQL.QueryExceptions.IncorrectColumnValueException;
import org.wytrych.WySQL.QueryExceptions.TableNotExistsException;
import org.wytrych.WySQL.SQLParser.JoinDefinition;
import org.wytrych.WySQL.SQLParser.WhereParser.Row;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.EqualsExpression;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.GreaterThanExpression;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.TrueExpression;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.WhereExpression;
import org.wytrych.WySQL.SQLParser.WhereParser.values.ColumnValue;
import org.wytrych.WySQL.SQLParser.WhereParser.values.DatabaseValue;
import org.wytrych.WySQL.SQLParser.WhereParser.values.NumberValue;
import org.wytrych.WySQL.db.IDatabase;
import org.wytrych.WySQL.db.table.TableException;
import org.wytrych.WySQL.shared.Alias;
import org.wytrych.WySQL.shared.Column;
import org.wytrych.WySQL.shared.ColumnType;
import org.wytrych.WySQL.shared.TableWithAssignedColumns;

import java.io.FileNotFoundException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.*;
import static org.wytrych.WySQL.shared.COLUMN_TYPE.INTEGER;
import static org.wytrych.WySQL.shared.COLUMN_TYPE.VARCHAR;

class DeleteCommandTest {
    private Map<String, List<Column>> mockTableDescriptions;
    private IDatabase mockDb;
    private ArgumentCaptor<WhereExpression> whereExpressionArgumentCaptor;
    private ArgumentCaptor<String> stringCaptor;

    @BeforeEach
    void setUp() {
        mockTableDescriptions = new HashMap<>();
        mockDb = mock(IDatabase.class);
        when(mockDb.getTableDescriptions()).thenReturn(mockTableDescriptions);
        whereExpressionArgumentCaptor = ArgumentCaptor.forClass(WhereExpression.class);
        stringCaptor = ArgumentCaptor.forClass(String.class);
    }

    @Test
    void parsesDeleteCommand() throws TableNotExistsException, FileNotFoundException, ClashingColumnException, TableException, IncorrectColumnValueException {
        WhereExpression we = new TrueExpression();
        Command command = new DeleteCommand("myTable", we);
        command.execute(mockDb);

        verify(mockDb).delete(stringCaptor.capture(), whereExpressionArgumentCaptor.capture());

        assertEquals("myTable", stringCaptor.getValue());
        assertEquals(we, whereExpressionArgumentCaptor.getValue());
    }

}

package org.wytrych.WySQL.Commands;

import org.wytrych.QueryResult.QueryResult;
import org.wytrych.WySQL.db.IDatabase;

public class RollbackCommand implements Command {
    @Override
    public QueryResult execute(IDatabase db) {
        return db.releaseLocksFromThread();
    }
}

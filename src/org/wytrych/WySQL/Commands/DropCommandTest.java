package org.wytrych.WySQL.Commands;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.wytrych.WySQL.db.IDatabase;
import org.wytrych.WySQL.db.table.TableException;
import org.wytrych.WySQL.shared.*;
import org.wytrych.WySQL.QueryExceptions.ClashingColumnException;
import org.wytrych.WySQL.QueryExceptions.IncorrectColumnValueException;
import org.wytrych.WySQL.QueryExceptions.TableNotExistsException;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.*;
import static org.wytrych.WySQL.shared.COLUMN_TYPE.*;

class DropCommandTest {
    private Map<String, List<Column>> mockTableDescriptions;
    private IDatabase mockDb;

    @BeforeEach
    void setUp() {
        mockTableDescriptions = new HashMap<>();
        mockDb = mock(IDatabase.class);
        when(mockDb.getTableDescriptions()).thenReturn(mockTableDescriptions);
    }

    @Test
    void callsDropTable() throws Exception {
        mockTableDescriptions.put("A", List.of(new Column("a", new ColumnType(INTEGER))));
        mockTableDescriptions.put("B", List.of(new Column("a", new ColumnType(INTEGER))));
        List<String> tableNames = List.of("A", "B");
        Command command = new DropCommand(tableNames, false);
        command.execute(mockDb);

        verify(mockDb).dropTables(tableNames);
    }

    @Test
    void throwsErrorIfTableDoesntExists() {
        List<String> tableNames = List.of("A");
        Command command = new DropCommand(tableNames, false);
        assertThrows(TableNotExistsException.class, () -> command.execute(mockDb));
    }

    @Test
    void filtersNonExistingTablesWithIfExists() throws Exception {
        mockTableDescriptions.put("A", List.of(new Column("a", new ColumnType(INTEGER))));
        List<String> tableNames = List.of("A", "B");
        List<String> expectedNames = List.of("A");
        Command command = new DropCommand(tableNames, true);
        command.execute(mockDb);
        verify(mockDb).dropTables(expectedNames);
    }

}
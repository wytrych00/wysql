package org.wytrych.WySQL.Commands;

import org.wytrych.QueryResult.QueryResult;
import org.wytrych.WySQL.db.IDatabase;
import org.wytrych.WySQL.db.table.TableException;
import org.wytrych.WySQL.QueryExceptions.IncorrectColumnValueException;
import org.wytrych.WySQL.QueryExceptions.UncheckedIncorrectColumnValueException;
import org.wytrych.WySQL.shared.Column;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.wytrych.WySQL.Commands.ICommandFactory.validateColumn;

public class InsertCommand implements Command {
    private IDatabase db;
    private final String tableName;
    private Object[] columnValues;
    private Map<String, Object> values;


    InsertCommand(String tableName, Map<String,Object> values) {
        this.tableName = tableName;
        this.values = values;
    }

    InsertCommand(String tableName, Object[] columnValues) {
        this.tableName = tableName;
        this.columnValues = columnValues;
    }

    @Override
    public QueryResult execute(IDatabase db) throws TableException, IncorrectColumnValueException {
        this.db = db;
        if (columnValues == null)
            return validateInsertSome();

        try {
            return validateInsert();
        } catch (UncheckedIncorrectColumnValueException e) {
            throw new IncorrectColumnValueException(e.value, e.expectedType);
        }
    }

    private QueryResult validateInsert() throws TableException {
        Map<String, List<Column>> tableDescriptions = db.getTableDescriptions();
        List<Column> columns = tableDescriptions.get(tableName);

        Map<String, Object> columnsWithValues = IntStream.range(0, columns.size())
                .boxed()
                .collect(Collectors.toMap(
                        i -> columns.get(i).name,
                        i -> validateColumn(columnValues[i], columns.get(i).type)
                ));
        return db.insert(tableName, columnsWithValues);
    }

    private QueryResult validateInsertSome() throws TableException, IncorrectColumnValueException {
        Map<String, List<Column>> tableDescriptions = db.getTableDescriptions();
        List<Column> columns = tableDescriptions.get(tableName).stream()
                .filter(column -> values.containsKey(column.name))
                .collect(Collectors.toList());

        try {
            Map<String, Object> parsedValues = columns.stream()
                    .collect(Collectors.toMap(
                            column -> column.name,
                            column -> validateColumn(values.get(column.name), column.type)
                    ));
            return db.insert(tableName, parsedValues);
        } catch (UncheckedIncorrectColumnValueException e) {
            throw new IncorrectColumnValueException(e.value, e.expectedType);
        }
    }

}

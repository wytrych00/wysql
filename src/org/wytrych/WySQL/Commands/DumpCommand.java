package org.wytrych.WySQL.Commands;

import org.wytrych.QueryResult.QueryResult;
import org.wytrych.WySQL.db.IDatabase;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

public class DumpCommand implements Command {
    private final String path;

    DumpCommand(String path) {
        this.path = path;
    }

    @Override
    public QueryResult execute(IDatabase db) throws FileNotFoundException {
        OutputStream out = new FileOutputStream(path);
        return db.dump(out);
    }
}
package org.wytrych.WySQL.Commands;

import org.wytrych.WySQL.QueryExceptions.UncheckedIncorrectColumnValueException;
import org.wytrych.WySQL.SQLParser.JoinDefinition;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.WhereExpression;
import org.wytrych.WySQL.SQLParser.WhereParser.values.NullValue;
import org.wytrych.WySQL.db.IDatabase;
import org.wytrych.WySQL.shared.*;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Map;

public interface ICommandFactory {
    Command createCreate(String tableName, List<Column> columns);

    Command createInsertAll(String tableName, Object[] values);

    Command createInsertSome(String tableName, Map<String, Object> values);

    Command createSelect(String[] tableNames, List<Alias> columns, List<TableWithAssignedColumns> assignedColumns, WhereExpression whereExpression, List<JoinDefinition> joinList, Boolean shouldLockTable);

    Command createInterpretDrop(List<String> tableNames, boolean shouldCheck);

    Command createCreateIndex(String tableName, String columnName);

    Command createUpdate(String tableName, Map<String, Object> newValuesMapping, WhereExpression whereExpression);

    Command createRollback();

    Command createDump(String path);

    Command createDelete(String tableName, WhereExpression whereExpression);

    static Object validateColumn(Object columnValue, ColumnType columnType) {
        final String NULL = "null";

        if (columnValue == null || columnValue.equals(NULL))
            return new NullValue();

        switch (columnType.getBaseType()) {
            case BIT:
                if (columnValue.equals("0") || columnValue.equals(0))
                    return 0;
                if (columnValue.equals("1") || columnValue.equals(1))
                    return 1;
                throw new UncheckedIncorrectColumnValueException(columnValue, columnType);
            case INTEGER:
                try {
                    return Integer.valueOf(columnValue.toString());
                } catch (NumberFormatException e) {
                    throw new UncheckedIncorrectColumnValueException(columnValue, columnType);
                }
            case BIGINT:
                try {
                    return Long.valueOf(columnValue.toString());
                } catch (NumberFormatException e) {
                    throw new UncheckedIncorrectColumnValueException(columnValue, columnType);
                }
            case VARCHAR:
                if (columnValue.toString().length() > columnType.getLength())
                    throw new UncheckedIncorrectColumnValueException(columnValue, columnType);
                return columnValue;
            case DATE:
                if (columnValue instanceof Date) {
                    return ((Date) columnValue).toLocalDate().atStartOfDay().atZone(IDatabase.projectTimeZone);
                } else {
                    try {
                        return LocalDate.parse(columnValue.toString(), IDatabase.dateFormatter).atStartOfDay().atZone(IDatabase.projectTimeZone);
                    } catch (DateTimeParseException e) {
                        throw new UncheckedIncorrectColumnValueException(columnValue, columnType);
                    }
                }
            default:
                return columnValue;
        }
    }

}

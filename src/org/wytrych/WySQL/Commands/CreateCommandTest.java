package org.wytrych.WySQL.Commands;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.wytrych.WySQL.db.IDatabase;
import org.wytrych.WySQL.db.table.TableException;
import org.wytrych.WySQL.QueryExceptions.ClashingColumnException;
import org.wytrych.WySQL.QueryExceptions.IncorrectColumnValueException;
import org.wytrych.WySQL.QueryExceptions.TableNotExistsException;
import org.wytrych.WySQL.shared.Column;
import org.wytrych.WySQL.shared.ColumnType;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.wytrych.WySQL.shared.COLUMN_TYPE.VARCHAR;

class CreateCommandTest {
    private Map<String, List<Column>> mockTableDescriptions;
    private IDatabase mockDb;

    @BeforeEach
    void setUp() {
        mockTableDescriptions = new HashMap<>();
        mockDb = mock(IDatabase.class);
        when(mockDb.getTableDescriptions()).thenReturn(mockTableDescriptions);
    }

    @Test
    void doesntChangeCreateCommand() throws TableNotExistsException, ClashingColumnException, TableException, IncorrectColumnValueException, FileNotFoundException {
        List<Column> columns = List.of(new Column("columnA", new ColumnType(VARCHAR)));
        String tableName = "tableA";

        Command command = new CreateCommand(tableName, columns);
        command.execute(mockDb);

        verify(mockDb).createTable(tableName, columns);
    }
}
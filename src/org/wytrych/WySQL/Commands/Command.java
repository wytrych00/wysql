package org.wytrych.WySQL.Commands;

import org.wytrych.QueryResult.QueryResult;
import org.wytrych.WySQL.db.IDatabase;
import org.wytrych.WySQL.db.table.TableException;
import org.wytrych.WySQL.QueryExceptions.ClashingColumnException;
import org.wytrych.WySQL.QueryExceptions.IncorrectColumnValueException;
import org.wytrych.WySQL.QueryExceptions.TableNotExistsException;

import java.io.FileNotFoundException;

public interface Command {
    QueryResult execute(IDatabase db) throws TableNotExistsException, TableException, IncorrectColumnValueException, ClashingColumnException, FileNotFoundException;
}

package org.wytrych.WySQL.Commands;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.TrueExpression;
import org.wytrych.WySQL.db.IDatabase;
import org.wytrych.WySQL.QueryExceptions.IncorrectColumnValueException;
import org.wytrych.WySQL.shared.Column;
import org.wytrych.WySQL.shared.ColumnType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.wytrych.WySQL.shared.COLUMN_TYPE.INTEGER;

class UpdateCommandTest {
    private Map<String, List<Column>> mockTableDescriptions;
    private IDatabase mockDb;

    @BeforeEach
    void setUp() {
        mockTableDescriptions = new HashMap<>();
        mockDb = mock(IDatabase.class);
        when(mockDb.getTableDescriptions()).thenReturn(mockTableDescriptions);
    }

    @Test
    void checksColumnTypesInUpdate() {
        mockTableDescriptions.put("table", List.of(new Column("a", new ColumnType(INTEGER))));
        Map<String, Object> newValuesMapping = new HashMap<>();
        newValuesMapping.put("a", "bla");
        Command command = new UpdateCommand("table", newValuesMapping, new TrueExpression());

        assertThrows(IncorrectColumnValueException.class, () -> command.execute(mockDb));
    }

}
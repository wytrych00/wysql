package org.wytrych.WySQL.Commands;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.wytrych.WySQL.SQLParser.WhereParser.values.NullValue;
import org.wytrych.WySQL.db.IDatabase;
import org.wytrych.WySQL.db.table.TableException;
import org.wytrych.WySQL.QueryExceptions.ClashingColumnException;
import org.wytrych.WySQL.QueryExceptions.IncorrectColumnValueException;
import org.wytrych.WySQL.QueryExceptions.TableNotExistsException;
import org.wytrych.WySQL.shared.COLUMN_TYPE;
import org.wytrych.WySQL.shared.Column;
import org.wytrych.WySQL.shared.ColumnType;

import java.sql.Date;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.wytrych.WySQL.shared.COLUMN_TYPE.*;

class InsertCommandTest {
    private Map<String, List<Column>> mockTableDescriptions;
    private IDatabase mockDb;
    private ArgumentCaptor<Map<String, Object>> mapCaptor;
    private ArgumentCaptor<String> stringCaptor;

    @BeforeEach
    void setUp() {
        mockTableDescriptions = new HashMap<>();
        mockDb = mock(IDatabase.class);
        when(mockDb.getTableDescriptions()).thenReturn(mockTableDescriptions);
        mapCaptor = ArgumentCaptor.forClass((Class) Map.class);
        stringCaptor = ArgumentCaptor.forClass(String.class);
    }
    @Test
    void transformsColumnsInInsertCommand() throws Exception {
        mockTableDescriptions.put("tableA", List.of(new Column("a", new ColumnType(VARCHAR)), new Column("b", new ColumnType(VARCHAR)), new Column("c", new ColumnType(VARCHAR))));

        String tableName = "tableA";
        String[] values = new String[]{"1", "2", "3"};

        Command command = new InsertCommand(tableName, values);
        command.execute(mockDb);

        verify(mockDb).insert(stringCaptor.capture(), mapCaptor.capture());

        Map<String, Object> passedMap = mapCaptor.getValue();

        assertEquals(tableName, stringCaptor.getValue());
        assertEquals(3, passedMap.size());
        assertEquals("1", passedMap.get("a"));
        assertEquals("2", passedMap.get("b"));
        assertEquals("3", passedMap.get("c"));
    }

    @Test
    void transformsNullValues() throws Exception {
        mockTableDescriptions.put("tableA", List.of(new Column("a", new ColumnType(INTEGER))));

        String tableName = "tableA";
        String[] values = new String[]{"null"};
        Command command = new InsertCommand(tableName, values);
        command.execute(mockDb);

        verify(mockDb).insert(stringCaptor.capture(), mapCaptor.capture());

        Map<String, Object> passedMap = mapCaptor.getValue();

        assertEquals(tableName, stringCaptor.getValue());
        assertEquals(1, passedMap.size());
        assertTrue(passedMap.get("a") instanceof NullValue);
    }

    @Test
    void callsTheDatabaseInInsertSomeCommand() throws Exception {
        mockTableDescriptions.put("tableA", List.of(new Column("a", new ColumnType(VARCHAR)), new Column("b", new ColumnType(VARCHAR)), new Column("c", new ColumnType(VARCHAR))));

        String tableName = "tableA";
        Map<String, Object> values = new HashMap<>();
        values.put("a", "abc1");
        values.put("b", "abc2");
        values.put("c", "abc3");

        Command command = new InsertCommand(tableName, values);
        command.execute(mockDb);

        verify(mockDb).insert(stringCaptor.capture(), mapCaptor.capture());

        Map<String, Object> passedMap = mapCaptor.getValue();

        assertEquals(tableName, stringCaptor.getValue());
        assertEquals(3, passedMap.size());
        assertEquals("abc1", passedMap.get("a"));
        assertEquals("abc2", passedMap.get("b"));
        assertEquals("abc3", passedMap.get("c"));
    }

    @Test
    void throwsErrorForTypeMismatchInInsertSome() {
        mockTableDescriptions.put("tableA", List.of(new Column("a", new ColumnType(INTEGER))));

        String tableName = "tableA";
        Map<String, Object> values = new HashMap<>();
        values.put("a", "abc1");

        Command command = new InsertCommand(tableName, values);
        assertThrows(IncorrectColumnValueException.class, () -> command.execute(mockDb));
    }

    @Test
    void throwsErrorIfValueTypesMismatchForInt() {
        mockTableDescriptions.put("tableA", List.of(new Column("a", new ColumnType(INTEGER))));

        String tableName = "tableA";

        Command command = new InsertCommand(tableName, new String[]{"abc"});
        assertThrows(IncorrectColumnValueException.class, () -> command.execute(mockDb));
    }

    @Test
    void throwsErrorIfValueTypesMismatchForBit() {
        mockTableDescriptions.put("tableA", List.of(new Column("a", new ColumnType(BIT))));

        String tableName = "tableA";

        Command command = new InsertCommand(tableName, new String[]{"abc"});
        assertThrows(IncorrectColumnValueException.class, () -> command.execute(mockDb));
    }

    @Test
    void throwsErrorIfValueTypesMismatchForString() {
        ColumnType shortVarchar = new ColumnType(VARCHAR, 5);

        mockTableDescriptions.put("tableA", List.of(new Column("a", shortVarchar)));

        String tableName = "tableA";
        Command command = new InsertCommand(tableName, new String[]{"abcdef"});
        assertThrows(IncorrectColumnValueException.class, () -> command.execute(mockDb));
    }

    @Test
    void columnsCanHaveDifferentLengths() {
        ColumnType shortVarchar = new ColumnType(VARCHAR, 2);
        ColumnType longVarchar = new ColumnType(VARCHAR, 5);

        mockTableDescriptions.put("tableA", List.of(new Column("a", shortVarchar), new Column("b", longVarchar)));

        String tableName = "tableA";
        Command command = new InsertCommand(tableName, new String[]{"abc", "abcd"});
        assertThrows(IncorrectColumnValueException.class, () -> command.execute(mockDb));
    }

    @Test
    void throwsErrorIfValueTypesMismatchForDate() {
        mockTableDescriptions.put("tableA", List.of(new Column("a", new ColumnType(DATE))));

        String tableName = "tableA";
        Command command = new InsertCommand(tableName, new String[]{"1-2-3"});
        assertThrows(IncorrectColumnValueException.class, () -> command.execute(mockDb));
    }

    @Test
    void parsesDateInSimpleFormat() throws Exception {
        mockTableDescriptions.put("tableA", List.of(new Column("a", new ColumnType(DATE))));

        String tableName = "tableA";
        String dateInput = "19-11-2014";

        Command command = new InsertCommand(tableName, new String[]{dateInput});
        command.execute(mockDb);
        verify(mockDb).insert(stringCaptor.capture(), mapCaptor.capture());

        ZonedDateTime date = (ZonedDateTime) mapCaptor.getValue().get("a");
        assertEquals(19, date.getDayOfMonth());
        assertEquals(11, date.getMonthValue());
        assertEquals(2014, date.getYear());
    }

    @Test
    void parsesSqlDate() throws Exception {
        mockTableDescriptions.put("tableA", List.of(new Column("a", new ColumnType(DATE))));

        String tableName = "tableA";
        ZonedDateTime dateTime = ZonedDateTime.parse("2014-11-19T00:00:00+00:00[UTC]");
        Date dateInput = new Date(dateTime.toInstant().toEpochMilli());

        Command command = new InsertCommand(tableName, new Object[]{dateInput});
        command.execute(mockDb);
        verify(mockDb).insert(stringCaptor.capture(), mapCaptor.capture());

        ZonedDateTime date = (ZonedDateTime) mapCaptor.getValue().get("a");
        assertEquals(19, date.getDayOfMonth());
        assertEquals(11, date.getMonthValue());
        assertEquals(2014, date.getYear());
    }

}
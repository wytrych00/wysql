package org.wytrych.WySQL.Commands;

import org.wytrych.QueryResult.QueryResult;
import org.wytrych.WySQL.SQLParser.JoinDefinition;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.AndExpression;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.TrueExpression;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.WhereExpression;
import org.wytrych.WySQL.db.IDatabase;
import org.wytrych.WySQL.QueryExceptions.ClashingColumnException;
import org.wytrych.WySQL.QueryExceptions.TableNotExistsException;
import org.wytrych.WySQL.shared.Alias;
import org.wytrych.WySQL.shared.Column;
import org.wytrych.WySQL.shared.TableWithAssignedColumns;

import java.util.*;
import java.util.stream.Collectors;

public class SelectCommand implements Command {
    private final String[] tableNames;
    private final List<Alias> incomingColumns;
    private final List<TableWithAssignedColumns> assignedColumns;
    private final WhereExpression whereExpression;
    private final List<JoinDefinition> joinList;
    private final Boolean shouldLockTable;

    private List<TableWithAssignedColumns> tablesWithColumns;

    SelectCommand(String[] tableNames, List<Alias> incomingColumns, List<TableWithAssignedColumns> assignedColumns, WhereExpression whereExpression, List<JoinDefinition> joinList, Boolean shouldLockTable) {
        this.tableNames = tableNames;
        this.incomingColumns = incomingColumns;
        this.assignedColumns = assignedColumns;
        this.whereExpression = whereExpression == null ? new TrueExpression() : whereExpression;
        this.joinList = joinList;
        this.shouldLockTable = shouldLockTable;
    }

    @Override
    public QueryResult execute(IDatabase db) throws ClashingColumnException, TableNotExistsException {
        TableAndColumnValidator validator = new TableAndColumnValidator(tableNames, incomingColumns, assignedColumns, db.getTableDescriptions());
        tablesWithColumns = validator.validate();
        WhereExpression selectExpression = combineSelectAndJoin();
        String[] tablesToSelect = getTables();
        QueryResult result = db.select(tablesToSelect, selectExpression, shouldLockTable);

        return resultWithColumnMapping(result);
    }

    private WhereExpression combineSelectAndJoin() {
        WhereExpression joinConditions = parseJoins();
        return new AndExpression(whereExpression, joinConditions);
    }

    private String[] getTables() {
        return tablesWithColumns.stream()
                .map(table -> table.tableName)
                .toArray(String[]::new);
    }

    private WhereExpression parseJoins() {
        Map<String, Integer> existingTablesInList = new HashMap<>();

        for (int i = 0; i < tablesWithColumns.size(); i++) {
            String tableName = tablesWithColumns.get(i).tableName;
            existingTablesInList.putIfAbsent(tableName, i);
        }

        WhereExpression joinConditions = new TrueExpression();

        for (JoinDefinition joinDefinition : joinList) {
            addTablesFromJoinDefinition(existingTablesInList, joinDefinition);
            joinConditions = new AndExpression(joinConditions, joinDefinition.getWhereExpression());
        }

        return joinConditions;
    }

    private void addTablesFromJoinDefinition(Map<String, Integer> existingTablesInList, JoinDefinition joinDefinition) {
        Alias ownColumnAlias = new Alias(joinDefinition.getOwnColumnName());
        String tableName = joinDefinition.getTable().getName();

        if (existingTablesInList.containsKey(tableName)) {
            TableWithAssignedColumns existingEntry = tablesWithColumns.get(existingTablesInList.get(tableName));
            existingEntry.getColumnsWithAliases().add(ownColumnAlias);
        } else {
            TableWithAssignedColumns joinTable = new TableWithAssignedColumns(tableName, List.of(ownColumnAlias));
            tablesWithColumns.add(joinTable);
            existingTablesInList.put(tableName, tablesWithColumns.size() - 1);
        }
    }

    private QueryResult resultWithColumnMapping(QueryResult result) {
        Map<String, String> columnToNameMapping = new HashMap<>();
        for (TableWithAssignedColumns tableWithAssignedColumns : tablesWithColumns)
            for (Alias columnAlias : tableWithAssignedColumns.getColumnsWithAliases())
                columnToNameMapping.put(columnAlias.getAlias(), tableWithAssignedColumns.tableName + "." + columnAlias.getName());

        return new QueryResult(result.result, columnToNameMapping);
    }
}


class TableAndColumnValidator {
    private final String[] tableNames;
    private final List<Alias> incomingColumns;
    private final List<TableWithAssignedColumns> assignedColumns;
    private final Map<String, List<Column>> tableDescriptions;
    private final Set<String> matchedColumns = new HashSet<>();
    private final Map<String, ArrayList<Alias>> mappedColumns = new HashMap<>();

    TableAndColumnValidator(String[] tableNames, List<Alias> incomingColumns, List<TableWithAssignedColumns> assignedColumns, Map<String, List<Column>> tableDescriptions) {
        this.tableNames = tableNames;
        this.incomingColumns = incomingColumns;
        this.assignedColumns = assignedColumns;
        this.tableDescriptions = tableDescriptions;
    }

    List<TableWithAssignedColumns> validate() throws TableNotExistsException, ClashingColumnException {
        for (String tableName : tableNames)
            checkForColumnsInTable(tableName);

        for (TableWithAssignedColumns assignedColumn : assignedColumns)
            processAssignedColumn(assignedColumn);

        return getTablesWithoutEmptyColumns();
    }

    private void checkForColumnsInTable(String tableName) throws TableNotExistsException, ClashingColumnException {
        if (!tableDescriptions.containsKey(tableName))
            throw new TableNotExistsException();
        List<Column> tableColumns = tableDescriptions.get(tableName);

        ArrayList<Alias> columns;

        if (isAllSelector())
            columns = allColumnsFromTable(tableColumns);
        else
            columns = getColumnAliases(tableColumns);

        mappedColumns.put(tableName, columns);
    }

    private boolean isAllSelector() {
        String ALL = "*";
        return incomingColumns.size() == 1 && incomingColumns.get(0).getName().equals(ALL);
    }

    private ArrayList<Alias> allColumnsFromTable(List<Column> tableColumns) {
        return tableColumns.stream()
                .map(c -> new Alias(c.name))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    private ArrayList<Alias> getColumnAliases(List<Column> tableColumns) throws ClashingColumnException {
        ArrayList<Alias> columns;
        columns = new ArrayList<>();
        for (Alias column : incomingColumns) {
            if (tableColumns.stream().anyMatch(c -> c.name.equals(column.getName()))) {
                if (matchedColumns.contains(column.getName()))
                    throw new ClashingColumnException(column.getName());
                columns.add(column);
                matchedColumns.add(column.getName());
            }
        }
        return columns;
    }

    private void processAssignedColumn(TableWithAssignedColumns assignedColumn) {
        ArrayList<Alias> columns = mappedColumns.getOrDefault(assignedColumn.tableName, new ArrayList<>());
        columns.addAll(assignedColumn.getColumnsWithAliases());
        mappedColumns.putIfAbsent(assignedColumn.tableName, columns);
    }

    private List<TableWithAssignedColumns> getTablesWithoutEmptyColumns() {
        return mappedColumns.entrySet().stream()
                .map(TableWithAssignedColumns::new)
                .filter(item -> item.getColumnsWithAliases().size() > 0)
                .collect(Collectors.toList());
    }

}

package org.wytrych.WySQL.Commands;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.wytrych.QueryResult.QueryResult;
import org.wytrych.WySQL.SQLParser.JoinDefinition;
import org.wytrych.WySQL.SQLParser.WhereParser.Row;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.EqualsExpression;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.GreaterThanExpression;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.TrueExpression;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.WhereExpression;
import org.wytrych.WySQL.SQLParser.WhereParser.values.ColumnValue;
import org.wytrych.WySQL.SQLParser.WhereParser.values.DatabaseValue;
import org.wytrych.WySQL.SQLParser.WhereParser.values.NumberValue;
import org.wytrych.WySQL.db.IDatabase;
import org.wytrych.WySQL.db.table.TableException;
import org.wytrych.WySQL.QueryExceptions.ClashingColumnException;
import org.wytrych.WySQL.QueryExceptions.IncorrectColumnValueException;
import org.wytrych.WySQL.QueryExceptions.QueryException;
import org.wytrych.WySQL.QueryExceptions.TableNotExistsException;
import org.wytrych.WySQL.shared.Alias;
import org.wytrych.WySQL.shared.Column;
import org.wytrych.WySQL.shared.ColumnType;
import org.wytrych.WySQL.shared.TableWithAssignedColumns;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.*;
import static org.wytrych.WySQL.shared.COLUMN_TYPE.*;

class SelectCommandTest {
    private Map<String, List<Column>> mockTableDescriptions;
    private IDatabase mockDb;
    private ArgumentCaptor<String[]> stringArrayCaptor;
    private ArgumentCaptor<WhereExpression> whereExpressionArgumentCaptor;
    private ArgumentCaptor<Boolean> booleanArgumentCaptor;

    @BeforeEach
    void setUp() {
        mockTableDescriptions = new HashMap<>();
        mockDb = mock(IDatabase.class);
        when(mockDb.getTableDescriptions()).thenReturn(mockTableDescriptions);
        stringArrayCaptor = ArgumentCaptor.forClass(String[].class);
        whereExpressionArgumentCaptor = ArgumentCaptor.forClass(WhereExpression.class);
        booleanArgumentCaptor = ArgumentCaptor.forClass(Boolean.class);
    }

    @Test
    void parsesSelectCommand() throws Exception {
        List<Column> tableAColumns = List.of(new Column("a", new ColumnType(VARCHAR)), new Column("b", new ColumnType(VARCHAR)));
        mockTableDescriptions.put("tableA", tableAColumns);
        List<Column> tableBColumns = List.of(new Column("c", new ColumnType(VARCHAR)));
        mockTableDescriptions.put("tableB", tableBColumns);

        String[] tables = new String[]{"tableA", "tableB"};
        List<Alias> columns = List.of(new Alias("a"), new Alias("b"), new Alias("c"));

        when(mockDb.select(isA(String[].class), isA(WhereExpression.class), isA(Boolean.class))).thenReturn(new QueryResult("OK"));

        Command command = new SelectCommand(tables, columns, Collections.emptyList(), null, Collections.emptyList(), false);
        command.execute(mockDb);

        verify(mockDb).select(stringArrayCaptor.capture(), whereExpressionArgumentCaptor.capture(), booleanArgumentCaptor.capture());

        String[] passedList = stringArrayCaptor.getValue();

        Arrays.sort(passedList);

        String table1 = passedList[0];
        String table2 = passedList[1];

        assertEquals("tableA", table1);
        assertEquals("tableB", table2);
    }

    @Test
    void filtersOutEmptyTableSets() throws Exception {
        mockTableDescriptions.put("tableA", List.of(new Column("a", new ColumnType(VARCHAR))));
        mockTableDescriptions.put("tableB", List.of(new Column("x", new ColumnType(VARCHAR))));

        String[] tables = new String[]{"tableA", "tableB"};
        TableWithAssignedColumns assignedColumn = new TableWithAssignedColumns("tableA", List.of(new Alias("a")));
        List<TableWithAssignedColumns> tableWithAssignedColumns = List.of(assignedColumn);

        when(mockDb.select(isA(String[].class), isA(WhereExpression.class), isA(Boolean.class))).thenReturn(new QueryResult("OK"));
        Command command = new SelectCommand(tables, Collections.emptyList(), tableWithAssignedColumns, null, Collections.emptyList(), false);
        command.execute(mockDb);

        verify(mockDb).select(stringArrayCaptor.capture(), whereExpressionArgumentCaptor.capture(), booleanArgumentCaptor.capture());

        String[] passedList = stringArrayCaptor.getValue();

        assertEquals(1, passedList.length);
        assertEquals("tableA", passedList[0]);
    }

    @Test
    void passesTheWhereExpression() throws Exception {
        TableWithAssignedColumns assignedColumn = new TableWithAssignedColumns("tableA", List.of(new Alias("a")));
        List<TableWithAssignedColumns> tableWithAssignedColumns = List.of(assignedColumn);
        WhereExpression mockWhereExpression = mock(WhereExpression.class);
        when(mockDb.select(isA(String[].class), isA(WhereExpression.class), isA(Boolean.class))).thenReturn(new QueryResult("OK"));
        Command command = new SelectCommand(new String[0], Collections.emptyList(), tableWithAssignedColumns, mockWhereExpression, Collections.emptyList(), false);
        command.execute(mockDb);

        verify(mockDb).select(stringArrayCaptor.capture(), whereExpressionArgumentCaptor.capture(), booleanArgumentCaptor.capture());

        WhereExpression passedWhereExpression = whereExpressionArgumentCaptor.getValue();
        Row row = new Row("a");
        passedWhereExpression.interpret(row);
        verify(mockWhereExpression).interpret(row);
    }


    @Test
    void throwsErrorWhenSelectingFromNonExistentTableWithAllSelect() {
        String[] tableNames = new String[]{"nonExistent"};
        List<Alias> columns = List.of(new Alias("*"));
        Command command = new SelectCommand(tableNames, columns, Collections.emptyList(), null, null, false);
        assertThrows(TableNotExistsException.class, () -> command.execute(mockDb));
    }

    @Test
    void throwsErrorWhenSelectingFromNonExistentTableWithSpecificSelect() {
        String[] tableNames = new String[]{"nonExistent"};
        List<Alias> columns = List.of(new Alias("a"));
        Command command = new SelectCommand(tableNames, columns, Collections.emptyList(), null, null, false);
        assertThrows(TableNotExistsException.class, () -> command.execute(mockDb));
    }

    @Test
    void parsesSingleColumn() throws Exception {
        List<Column> tableAColumns = List.of(new Column("a", new ColumnType(VARCHAR)));
        mockTableDescriptions.put("tableA", tableAColumns);
        List<Column> tableBColumns = new ArrayList<>();
        mockTableDescriptions.put("tableB", tableBColumns);

        String[] tables = new String[0];

        TableWithAssignedColumns assignedColumn = new TableWithAssignedColumns("tableA", List.of(new Alias("a")));
        List<TableWithAssignedColumns> tableWithAssignedColumns = new ArrayList<>();
        tableWithAssignedColumns.add(assignedColumn);

        when(mockDb.select(isA(String[].class), isA(WhereExpression.class), isA(Boolean.class))).thenReturn(new QueryResult("OK"));
        Command command = new SelectCommand(tables, new ArrayList<>(), tableWithAssignedColumns, null, Collections.emptyList(), false);
        command.execute(mockDb);

        verify(mockDb).select(stringArrayCaptor.capture(), whereExpressionArgumentCaptor.capture(), booleanArgumentCaptor.capture());
        String[] passedList = stringArrayCaptor.getValue();

        assertEquals(1, passedList.length);
        assertEquals("tableA", passedList[0]);
    }

    @Test
    void handlesOpenAndAssignedColumns() throws Exception {
        List<Column> tableAColumns = List.of(new Column("a", new ColumnType(VARCHAR)));
        mockTableDescriptions.put("tableA", tableAColumns);
        List<Column> tableBColumns = List.of(new Column("c", new ColumnType(VARCHAR)));
        mockTableDescriptions.put("tableB", tableBColumns);

        String[] tables = new String[]{"tableA", "tableB"};
        List<Alias> columns = List.of(new Alias("a"), new Alias("c"));
        TableWithAssignedColumns assignedColumn = new TableWithAssignedColumns("tableA", List.of(new Alias("b")));
        List<TableWithAssignedColumns> tableWithAssignedColumns = new ArrayList<>();
        tableWithAssignedColumns.add(assignedColumn);

        when(mockDb.select(isA(String[].class), isA(WhereExpression.class), isA(Boolean.class))).thenReturn(new QueryResult("OK"));
        Command command = new SelectCommand(tables, columns, tableWithAssignedColumns, null, Collections.emptyList(), false);
        command.execute(mockDb);

        verify(mockDb).select(stringArrayCaptor.capture(), whereExpressionArgumentCaptor.capture(), booleanArgumentCaptor.capture());

        String[] passedList = stringArrayCaptor.getValue();
        Arrays.sort(passedList);

        String table1 = passedList[0];
        String table2 = passedList[1];

        assertEquals("tableA", table1);

        assertEquals("tableB", table2);
    }

    @Test
    void supportsAliasedColumns() throws Exception {
        List<Column> tableAColumns = List.of(new Column("a", new ColumnType(VARCHAR)), new Column("b", new ColumnType(VARCHAR)));
        mockTableDescriptions.put("tableA", tableAColumns);
        Alias aliasA = new Alias("a", "columnOne");

        String[] tables = new String[]{"tableA"};
        List<Alias> columns = List.of(new Alias("b"), aliasA);
        when(mockDb.select(isA(String[].class), isA(WhereExpression.class), isA(Boolean.class))).thenReturn(new QueryResult("OK"));
        Command command = new SelectCommand(tables, columns, new ArrayList<>(), null, Collections.emptyList(), false);
        command.execute(mockDb);

        verify(mockDb).select(stringArrayCaptor.capture(), whereExpressionArgumentCaptor.capture(), booleanArgumentCaptor.capture());

        String[] passedList = stringArrayCaptor.getValue();

        assertEquals(1, passedList.length);

        String table = passedList[0];

        assertEquals("tableA", table);
    }

    @Test
    void selectWithJoin() throws Exception {
        List<Column> tableAColumns = List.of(new Column("a", new ColumnType(INTEGER)), new Column("b", new ColumnType(INTEGER)));
        mockTableDescriptions.put("tableA", tableAColumns);
        Alias aliasA = new Alias("a", "columnOne");

        String[] tables = new String[]{"tableA"};
        List<Alias> columns = List.of(new Alias("b"), aliasA);

        DatabaseValue joinLeft = new ColumnValue("tableB.c_id");
        DatabaseValue joinRight = new ColumnValue("tableA.a");
        WhereExpression joinWhereExpression = new EqualsExpression(joinLeft, joinRight);
        JoinDefinition joinDefinition = new JoinDefinition(new Alias("tableB", "aliasB"), "c_id", new Alias("c_id", "joinColumn"), joinWhereExpression);

        DatabaseValue columnId = new ColumnValue("tableA.b");
        DatabaseValue boundary = new NumberValue(5);
        WhereExpression incomingWhereExpression = new GreaterThanExpression(columnId, boundary);

        Map<String, Object> mockResult = new HashMap<>();
        mockResult.put("tableA.a", 15);

        when(mockDb.select(isA(String[].class), isA(WhereExpression.class), isA(Boolean.class))).thenReturn(new QueryResult(List.of(mockResult)));
        Command command = new SelectCommand(tables, columns, new ArrayList<>(), incomingWhereExpression, List.of(joinDefinition), false);
        QueryResult result = command.execute(mockDb);

        assertEquals(15, result.get(0).get("columnOne"));

        verify(mockDb).select(stringArrayCaptor.capture(), whereExpressionArgumentCaptor.capture(), booleanArgumentCaptor.capture());

        String[] passedList = stringArrayCaptor.getValue();

        assertEquals(2, passedList.length);

        String table = passedList[0];

        assertEquals("tableA", table);

        table = passedList[1];

        assertEquals("tableB", table);

        WhereExpression joinedWhereExpression = whereExpressionArgumentCaptor.getValue();

        Map<String, Object> columnValues = new HashMap<>();
        columnValues.put("tableA.a", 10);
        columnValues.put("tableB.c_id", 10);
        columnValues.put("tableA.b", 10);
        Row context = new Row(columnValues);
        assertTrue(joinedWhereExpression.interpret(context));

        columnValues = new HashMap<>();
        columnValues.put("tableA.a", 10);
        columnValues.put("tableB.c_id", 10);
        columnValues.put("tableA.b", 2);
        context = new Row(columnValues);
        // tableA.b < 5
        assertFalse(joinedWhereExpression.interpret(context));

        columnValues = new HashMap<>();
        columnValues.put("tableA.a", 11);
        columnValues.put("tableB.c_id", 10);
        columnValues.put("tableA.b", 10);
        context = new Row(columnValues);
        // tableA.a /= tableB.c_id
        assertFalse(joinedWhereExpression.interpret(context));
    }

    @Test
    void throwsAnErrorWhenColumnsAreAmbiguous() {
        List<Column> tableAColumns = List.of(new Column("a", new ColumnType(VARCHAR)), new Column("b", new ColumnType(VARCHAR)));
        mockTableDescriptions.put("tableA", tableAColumns);
        List<Column> tableBColumns = List.of(new Column("a", new ColumnType(VARCHAR)));
        mockTableDescriptions.put("tableB", tableBColumns);

        String[] tables = new String[]{"tableA", "tableB"};
        List<Alias> columns = List.of(new Alias("a"), new Alias("b"));

        Command command = new SelectCommand(tables, columns, null, null, null, false);

        assertThrows(ClashingColumnException.class, () -> command.execute(mockDb));
    }

    @Test
    void unwrapsStarSelector() throws Exception {
        List<Column> tableAColumns = List.of(new Column("a", new ColumnType(VARCHAR)), new Column("b", new ColumnType(VARCHAR)), new Column("c", new ColumnType(VARCHAR)));
        mockTableDescriptions.put("tableA", tableAColumns);
        List<Column> tableBColumns = List.of(new Column("a", new ColumnType(VARCHAR)), new Column("x", new ColumnType(VARCHAR)), new Column("y", new ColumnType(VARCHAR)), new Column("z", new ColumnType(VARCHAR)));
        mockTableDescriptions.put("tableB", tableBColumns);

        String[] tables = new String[]{"tableA", "tableB"};
        List<Alias> columns = List.of(new Alias("*"));

        when(mockDb.select(isA(String[].class), isA(WhereExpression.class), isA(Boolean.class))).thenReturn(new QueryResult("OK"));
        Command command = new SelectCommand(tables, columns, Collections.emptyList(), null, Collections.emptyList(), false);
        command.execute(mockDb);

        verify(mockDb).select(stringArrayCaptor.capture(), whereExpressionArgumentCaptor.capture(), booleanArgumentCaptor.capture());

        String[] passedList = stringArrayCaptor.getValue();
        Arrays.sort(passedList);

        String table1 = passedList[0];
        String table2 = passedList[1];

        assertEquals("tableA", table1);
        assertEquals("tableB", table2);
    }

    @Test
    void supportsTableAliases() throws Exception {
        mockTableDescriptions.put("table", List.of(new Column("a", new ColumnType(INTEGER))));
        String[] tableNames = {"table"};
        TableWithAssignedColumns tableWithAssignedColumns = new TableWithAssignedColumns("table", List.of(new Alias("a", "myColumn")));
        List<TableWithAssignedColumns> tableWithAssignedColumnsList = List.of(tableWithAssignedColumns);

        Map<String, Object> row = new HashMap<>();
        row.put("table.a", 10);
        when(mockDb.select(isA(String[].class), isA(WhereExpression.class), isA(Boolean.class))).thenReturn(new QueryResult(List.of(row)));

        Command command = new SelectCommand(tableNames, Collections.emptyList(), tableWithAssignedColumnsList, new TrueExpression(), Collections.emptyList(), false);
        QueryResult result = command.execute(mockDb);

        assertEquals(1, result.size());
        assertEquals(10, result.get(0).get("myColumn"));
    }
}

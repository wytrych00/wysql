package org.wytrych.WySQL.Commands;

import org.wytrych.QueryResult.QueryResult;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.WhereExpression;
import org.wytrych.WySQL.db.IDatabase;
import org.wytrych.WySQL.QueryExceptions.IncorrectColumnValueException;
import org.wytrych.WySQL.QueryExceptions.UncheckedIncorrectColumnValueException;
import org.wytrych.WySQL.shared.COLUMN_TYPE;
import org.wytrych.WySQL.shared.Column;
import org.wytrych.WySQL.shared.ColumnType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.wytrych.WySQL.Commands.ICommandFactory.validateColumn;

public class UpdateCommand implements Command {
    private final String tableName;
    private final Map<String, Object> newValuesMapping;
    private final WhereExpression whereExpression;

    UpdateCommand(String tableName,  Map<String, Object> newValuesMapping, WhereExpression whereExpression) {
        this.tableName = tableName;
        this.newValuesMapping = newValuesMapping;
        this.whereExpression = whereExpression;
    }

    @Override
    public QueryResult execute(IDatabase db) throws IncorrectColumnValueException {
        Map<String, List<Column>> tableDescriptions = db.getTableDescriptions();
        List<Column> columns = tableDescriptions.get(tableName);

        Map<String, Object> validatedNewValuesMapping = new HashMap<>();

        for (Map.Entry<String, Object> newValueMapping : newValuesMapping.entrySet()) {
            String columnName = newValueMapping.getKey();
            Object newValue = newValueMapping.getValue();

            Column columnToUpdate = columns.stream()
                    .filter(column -> column.name.equals(columnName))
                    .findFirst().orElse(new Column("NO_COLUMN", new ColumnType(COLUMN_TYPE.BIT)));

            Object validatedNewValue;

            try {
                validatedNewValue = validateColumn(newValue, columnToUpdate.type);
            } catch (UncheckedIncorrectColumnValueException e) {
                throw new IncorrectColumnValueException(e.value, e.expectedType);
            }

            validatedNewValuesMapping.put(columnName, validatedNewValue);
        }

        return db.update(tableName, validatedNewValuesMapping, whereExpression);
    }
}

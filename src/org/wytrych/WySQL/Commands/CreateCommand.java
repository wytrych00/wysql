package org.wytrych.WySQL.Commands;

import org.wytrych.QueryResult.QueryResult;
import org.wytrych.WySQL.db.IDatabase;
import org.wytrych.WySQL.shared.Column;

import java.util.List;

public class CreateCommand implements Command {
    private final String tableName;
    private final List<Column> columns;

    CreateCommand(String tableName, List<Column> columns) {
        this.tableName = tableName;
        this.columns = columns;
    }

    @Override
    public QueryResult execute(IDatabase db) {
        return db.createTable(tableName, columns);
    }
}

package org.wytrych.WySQL.Commands;

import org.wytrych.WySQL.SQLParser.JoinDefinition;
import org.wytrych.WySQL.SQLParser.WhereParser.expressions.WhereExpression;
import org.wytrych.WySQL.shared.*;

import java.util.*;

public class CommandFactory implements ICommandFactory {
    @Override
    public Command createCreate(String tableName, List<Column> columns) {
        return new CreateCommand(tableName, columns);
    }

    @Override
    public Command createInsertAll(String tableName, Object[] values) {
        return new InsertCommand(tableName, values);
    }

    @Override
    public Command createInsertSome(String tableName, Map<String, Object> values) {
        return new InsertCommand(tableName, values);
    }

    @Override
    public Command createSelect(String[] tableNames, List<Alias> columns, List<TableWithAssignedColumns> assignedColumns, WhereExpression whereExpression, List<JoinDefinition> joinList, Boolean shouldLockTable) {
        return new SelectCommand(tableNames, columns, assignedColumns, whereExpression, joinList, shouldLockTable);
    }

    @Override
    public Command createInterpretDrop(List<String> tableNames, boolean shouldCheck) {
        return new DropCommand(tableNames, shouldCheck);
    }

    @Override
    public Command createCreateIndex(String tableName, String columnName) {
        return new CreateIndexCommand(tableName, columnName);
    }

    @Override
    public Command createUpdate(String tableName, Map<String, Object> newValuesMapping, WhereExpression whereExpression) {
        return new UpdateCommand(tableName, newValuesMapping, whereExpression);
    }

    @Override
    public Command createRollback() {
        return new RollbackCommand();
    }

    @Override
    public Command createDump(String path) {
        return new DumpCommand(path);
    }

    @Override
    public Command createDelete(String tableName, WhereExpression whereExpression) {
        return new DeleteCommand(tableName, whereExpression);
    }

}

package org.wytrych.WySQL.Commands;

import org.wytrych.QueryResult.QueryResult;
import org.wytrych.WySQL.db.IDatabase;
import org.wytrych.WySQL.QueryExceptions.TableNotExistsException;
import org.wytrych.WySQL.shared.Column;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DropCommand implements Command {
    private IDatabase db;
    private List<String> tableNames;
    private final boolean shouldCheck;

    DropCommand(List<String> tableNames, boolean shouldCheck) {
        this.tableNames = tableNames;
        this.shouldCheck = shouldCheck;
    }

    @Override
    public QueryResult execute(IDatabase db) throws TableNotExistsException {
        this.db = db;
        if (shouldCheck) {
            Map<String, List<Column>> tableDescriptions = db.getTableDescriptions();

            tableNames = tableNames.stream()
                    .filter(tableDescriptions::containsKey)
                    .collect(Collectors.toList());
        }

        return interpretDrop();
    }

    private QueryResult interpretDrop() throws TableNotExistsException {
        Map<String, List<Column>> tableDescriptions = db.getTableDescriptions();

        for (String tableName : tableNames)
            if (!tableDescriptions.containsKey(tableName))
                throw new TableNotExistsException();
        return db.dropTables(tableNames);
    }
}

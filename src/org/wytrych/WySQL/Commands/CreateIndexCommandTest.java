package org.wytrych.WySQL.Commands;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.wytrych.WySQL.db.IDatabase;
import org.wytrych.WySQL.db.table.TableException;
import org.wytrych.WySQL.QueryExceptions.ClashingColumnException;
import org.wytrych.WySQL.QueryExceptions.IncorrectColumnValueException;
import org.wytrych.WySQL.QueryExceptions.TableNotExistsException;
import org.wytrych.WySQL.shared.Column;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class CreateIndexCommandTest {
    private Map<String, List<Column>> mockTableDescriptions;
    private IDatabase mockDb;
    private ArgumentCaptor<String> stringCaptor;
    private ArgumentCaptor<String> stringCaptor2;

    @BeforeEach
    void setUp() {
        mockTableDescriptions = new HashMap<>();
        mockDb = mock(IDatabase.class);
        when(mockDb.getTableDescriptions()).thenReturn(mockTableDescriptions);
        stringCaptor = ArgumentCaptor.forClass(String.class);
        stringCaptor2 = ArgumentCaptor.forClass(String.class);
    }

    @Test
    void createIndex() throws Exception {
        Command command = new CreateIndexCommand("table", "column");
        command.execute(mockDb);
        verify(mockDb).createIndex(stringCaptor.capture(), stringCaptor2.capture());
        assertEquals("table", stringCaptor.getValue());
        assertEquals("column", stringCaptor2.getValue());
    }

}
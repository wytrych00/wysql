package org.wytrych.WySQL.QueryExceptions;

public class ClashingColumnException extends QueryException {
    private final String clashingColumnName;
    public ClashingColumnException(String clashingColumnName) {
        this.clashingColumnName = clashingColumnName;
    }

    public String getColumn() {
        return clashingColumnName;
    }
}

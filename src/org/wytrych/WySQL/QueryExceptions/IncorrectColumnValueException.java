package org.wytrych.WySQL.QueryExceptions;

import org.wytrych.WySQL.shared.COLUMN_TYPE;
import org.wytrych.WySQL.shared.Column;
import org.wytrych.WySQL.shared.ColumnType;

public class IncorrectColumnValueException extends QueryException {
    final public Object value;
    final public ColumnType expectedType;

    public IncorrectColumnValueException(Object value, ColumnType expectedType) {
        this.value = value;
        this.expectedType = expectedType;
    }
}

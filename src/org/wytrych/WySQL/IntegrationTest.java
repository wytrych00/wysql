package org.wytrych.WySQL;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.wytrych.QueryResult.QueryResult;
import org.wytrych.QueryResult.QueryStatement;
import org.wytrych.WySQL.Commands.CommandFactory;
import org.wytrych.WySQL.SQLParser.ISQLParser;
import org.wytrych.WySQL.SQLParser.SQLParser;
import org.wytrych.WySQL.SQLParser.WhereParser.values.NullValue;
import org.wytrych.WySQL.db.Database;
import org.wytrych.WySQL.shared.DBAccessor;
import org.wytrych.WySQL.db.IDatabase;

import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.Queue;

import static org.junit.jupiter.api.Assertions.*;
import static org.wytrych.QueryResult.QueryResult.QueryResultErrorCode.*;

class IntegrationTest {

    private DBAccessor dbAccessor;

    @BeforeEach
    void setUp() {
        IDatabase db = new Database();
        ISQLParser parser = new SQLParser(new CommandFactory());
        dbAccessor = new DBAccessor(db, parser);
    }

    private QueryResult executeCommand(String s) {
        return dbAccessor.executeQuery(s);
    }

    private QueryResult executeStatement(QueryStatement statement) {
        return  dbAccessor.executeQuery(statement);
    }

    @Test
    void integrationWithSelectAll() {
        executeCommand("create table X (x integer, y varchar(255));");
        executeCommand("insert into X values (123, \"another-thing\");");
        executeCommand("insert into X values (456, \"bla bla bla\");");
        QueryResult result = executeCommand("select * from X;");

        assertEquals(2, result.size());
        QueryResult.RowAccessor row1 = result.get(0);
        QueryResult.RowAccessor row2 = result.get(1);

        assertEquals(123, row1.get("x"));
        assertEquals("another-thing", row1.get("y"));
        assertEquals(456, row2.get("x"));
        assertEquals("bla bla bla", row2.get("y"));
    }

    @Test
    void integrationWithSelect() {
        executeCommand("create table X (x varchar(255), y varchar(255), z varchar(255));");
        executeCommand("insert into X values (\"value 123\", \"another-thing\", \"xxx\");");
        executeCommand("insert into X values (\"something else\", \"bla bla bla\", \"zzz\");");
        QueryResult result = executeCommand("select y, z from X;");

        assertEquals(2, result.size());
        QueryResult.RowAccessor row1 = result.get(0);
        QueryResult.RowAccessor row2 = result.get(1);

        assertFalse(row1.containsKey("x"));
        assertEquals("another-thing", row1.get("y"));
        assertEquals("xxx", row1.get("z"));
        assertFalse(row2.containsKey("x"));
        assertEquals("bla bla bla", row2.get("y"));
        assertEquals("zzz", row2.get("z"));
    }

    @Test
    void integrationWithSomeValuesInserted() {
        executeCommand("create table X (x integer, y varchar(255), z varchar(255));");
        executeCommand("insert into X (x, z) values (123, \"another-thing\");");
        QueryResult result = executeCommand("select * from X;");

        assertEquals(1, result.size());
        QueryResult.RowAccessor row1 = result.get(0);

        assertEquals(123, row1.get("x"));
        assertEquals("NULL", row1.get("y"));
        assertEquals("another-thing", row1.get("z"));
    }

    @Test
    void integrationWithTwoTables() {
        executeCommand("create table A (a bit, b bit));");
        executeCommand("create table B (c bit, d bit));");
        executeCommand("insert into A values (1, 1);");
        executeCommand("insert into A values (1, 0);");
        executeCommand("insert into B values (0, 0);");
        executeCommand("insert into B values (1, 1);");
        QueryResult result = executeCommand("select * from A, B;");

        assertEquals(4, result.size());
        QueryResult.RowAccessor row4 = result.get(3);

        assertEquals(1, row4.get("a"));
        assertEquals(0, row4.get("b"));
        assertEquals(1, row4.get("c"));
        assertEquals(1, row4.get("d"));
    }

    @Test
    void onePrefixedColumn() {
        executeCommand("create table A (a bit, b bit));");
        executeCommand("create table C (c bit, d bit));");
        executeCommand("insert into A values (1, 1);");
        executeCommand("insert into A values (0, 1);");
        executeCommand("insert into C values (1, 1);");
        executeCommand("insert into C values (0, 0);");
        QueryResult result = executeCommand("select A.a from A, C;");

        assertEquals(2, result.size());

        QueryResult.RowAccessor row1 = result.get(0);
        QueryResult.RowAccessor row2 = result.get(1);

        assertEquals(1, row1.get("A.a"));
        assertEquals(0, row2.get("A.a"));
    }

    @Test
    void columnAliases() {
        executeCommand("create table A (a integer, b integer);");
        executeCommand("create table B (a integer, c integer);");
        executeCommand("insert into A values (1, 4);");
        executeCommand("insert into B values (2, 3);");
        QueryResult result = executeCommand("select A.a as columnOne, B.a as columnTwo, c as columnThree, b from A, B;");

        assertEquals(1, result.size());

        QueryResult.RowAccessor row1 = result.get(0);

        assertEquals(1, row1.get("columnOne"));
        assertEquals(2, row1.get("columnTwo"));
        assertEquals(3, row1.get("columnThree"));
        assertEquals(4, row1.get("b"));
    }

    @Test
    void whereClause() {
        executeCommand("create table A (a integer, b integer);");
        executeCommand("create table B (a integer, c integer);");
        executeCommand("insert into A values (1, 4);");
        executeCommand("insert into A values (10, 2);");
        executeCommand("insert into A values (0, 3);");
        executeCommand("insert into B values (2, 3);");
        executeCommand("insert into B values (6, 6);");
        executeCommand("insert into B values (6, 8);");
        QueryResult result = executeCommand("select A.a as columnOne, B.a as columnTwo, c as columnThree, b from A, B where A.a > 9 AND B.c = 3;");

        assertEquals(1, result.size());
    }

    @Test
    void whereClauseReferencingColumnOnly() {
        executeCommand("create table X (id integer);");
        executeCommand("insert into X values (1);");
        executeCommand("insert into X values (2);");
        QueryResult result = executeCommand("select id from X where id > 1;");

        assertEquals(1, result.size());
        assertEquals(2, result.get(0).get("id"));
    }

    @Test
    void indexedColumn() {
        executeCommand("create table A (a integer, b integer, index a_index (a));");
        executeCommand("insert into A values (1, 2);");
        QueryResult result = executeCommand("select * from A;");

        assertEquals(1, result.size());
    }

    @Test
    void dropTable() {
        executeCommand("create table A (a integer);");
        executeCommand("drop table A;");
        QueryResult result = executeCommand("select * from A;");

        assertEquals("ERROR", result.status);
        assertEquals(QueryResult.QueryResultErrorCode.NO_TABLE, result.errorCode);
    }

    @Test
    void dropTableIfExistsDoesntErrorWhenTableDoesntExist() {
        QueryResult result = executeCommand("drop table if exists A;");
        assertEquals("OK", result.status);
    }

    @Test
    void dropTableIfExistsRemovesTable() {
        executeCommand("create table A (a integer);");
        executeCommand("drop table if exists A;");
        QueryResult result = executeCommand("select * from A;");

        assertEquals("ERROR", result.status);
        assertEquals(QueryResult.QueryResultErrorCode.NO_TABLE, result.errorCode);
    }

    @Test
    void primaryKey() {
        executeCommand("create table A (a integer, b integer, primary key (a));");
        executeCommand("insert into A values (1, 2);");
        QueryResult result = executeCommand("insert into A values (1, 2);");

        assertEquals("ERROR", result.status);
        assertEquals(DUPLICATE_ENTRY, result.errorCode);
    }

    @Test
    void nullValues() {
        executeCommand("create table A (a integer, b integer);");
        executeCommand("insert into A values (1, null);");
        QueryResult result = executeCommand("select * from A;");

        assertEquals(1, result.size());
        assertEquals(1, result.get(0).get("a"));
        assertTrue(result.get(0).get("b") instanceof NullValue);
    }

    @Test
    void notNullValidation() {
        executeCommand("create table A (a integer not null, b integer);");
        executeCommand("insert into A values (1, 2);");
        QueryResult result = executeCommand("insert into A values (null, 2);");

        assertEquals("ERROR", result.status);
        assertEquals(NULL_VIOLATION, result.errorCode);
    }

    @Test
    void allowsNonDuplicateValuesWithOneItemInDatabase() {
        executeCommand("create table TestEntity (id integer not null, primary key (id));");
        executeCommand("insert into TestEntity (id) values (100);");
        QueryResult result = executeCommand("insert into TestEntity (id) values (101);");

        assertEquals("OK", result.status);
    }

    @Test
    void tableAliases() {
        executeCommand("create table X (id integer);");
        executeCommand("insert into X (id) values (100);");
        QueryResult result = executeCommand("select aliasedTable.id as myId from X aliasedTable;");

        assertEquals(1, result.size());
        assertEquals(100, result.get(0).get("myId"));
    }

    @Test
    void dotNotationColumnReferenceInWhereClause() {
        executeCommand("create table X (id integer);");
        executeCommand("insert into X (id) values (100);");
        executeCommand("insert into X (id) values (105);");
        QueryResult result = executeCommand("select aliasedTable.id as myId from X aliasedTable where aliasedTable.id=100;");

        assertEquals(1, result.size());
        assertEquals(100, result.get(0).get("myId"));
    }

    @Test
    void worksWithCompositePrimaryKeys() {
        executeCommand("create table X (id integer, id2 integer, primary key (id, id2))");
        executeCommand("insert into X values (1, 2)");
        QueryResult result = executeCommand("insert into X values (3, 4)");

        assertEquals("OK", result.status);

        result = executeCommand("insert into X values (3, 4)");

        assertEquals("ERROR", result.status);
        assertEquals(DUPLICATE_ENTRY, result.errorCode);
    }

    @Test
    void createIndexCommand() {
        executeCommand("create table X (id integer);");
        QueryResult result = executeCommand("create index INDEX on X (id);");
        assertEquals("OK", result.status);
    }

    @Test
    void foreignConstraints() {
        QueryResult result;
        executeCommand("create table X (id integer, primary key (id));");
        executeCommand("create table Y (id integer, primary key (id));");
        executeCommand("create table Z (id_X integer, id_Y integer, constraint X_constraint foreign key (id_X) references X (id), constraint Y_constraint foreign key (id_Y) references Y (id));");

        result = executeCommand("insert into Z values (1, 2)");
        assertEquals("ERROR", result.status);
        assertEquals(FOREIGN_KEY_VIOLATION, result.errorCode);
        assertEquals("X_constraint", result.message);

        executeCommand("insert into X values (1);");

        result = executeCommand("insert into Z values (1, 2)");
        assertEquals("ERROR", result.status);
        assertEquals(FOREIGN_KEY_VIOLATION, result.errorCode);
        assertEquals("Y_constraint", result.message);

        executeCommand("insert into Y values (2);");

        result = executeCommand("insert into Z values (1, 2)");
        assertEquals("OK", result.status);
    }

    @Test
    void basicTestWithLimitOnNonIndexedValue() {
        executeCommand("create table X (a integer, b integer, primary key (a));");
        executeCommand("insert into X values (1, 2);");
        executeCommand("insert into X values (3, 1);");
        executeCommand("insert into X values (2, 10);");
        executeCommand("insert into X values (4, 1);");
        QueryResult result = executeCommand("select * from X where X.b > 5;");
        assertEquals(1, result.size());
        assertEquals(2, result.get(0).get("a"));
        assertEquals(10, result.get(0).get("b"));
    }

    @Test
    void whereExpressionWithoutColumnAliases() {
        executeCommand("create table X (a integer));");
        executeCommand("insert into X values (1)");
        executeCommand("insert into X values (10)");
        QueryResult result = executeCommand("select X.a from X where X.a > 5;");
        assertEquals(1, result.size());
        assertEquals(10, result.get(0).get("X.a"));
    }

    @Test
    void mixedAliasesAndNonAliases() {
        executeCommand("create table X (a integer, b integer, c integer, primary key (a, b));");
        executeCommand("insert into X values (1, 4, 5);");
        executeCommand("insert into X values (2, 6, 3);");
        executeCommand("insert into X values (2, 8, 7);");
        QueryResult result = executeCommand("select table.a, table.b, table.c as column from X table where table.a=2 and table.b=8;");

        assertEquals(1, result.size());
        assertEquals(2, result.get(0).get("table.a"));
        assertEquals(8, result.get(0).get("table.b"));
        assertEquals(7, result.get(0).get("column"));
    }

    @Test
    void joins() {
        executeCommand("create table X (word_id integer, word varchar(255));");
        executeCommand("create table Y (word_id integer, availability bit));");
        executeCommand("create table Z (word_id integer, availability bit, info varchar(255));");
        executeCommand("insert into X values (1, \"one\");");
        executeCommand("insert into X values (2, \"two\");");
        executeCommand("insert into X values (3, \"three\");");
        executeCommand("insert into X values (4, \"four\");");
        executeCommand("insert into Y values (1, 0);");
        executeCommand("insert into Y values (3, 1);");
        executeCommand("insert into Y values (4, 1);");
        executeCommand("insert into Z values (4, 0, \"new info\");");
        executeCommand("insert into Z values (3, 1, \"old info\");");
        QueryResult result = executeCommand(
                "select words.word as myWords, available_words_2.info as myInfo from X words " +
                   "inner join Y available_words on words.word_id=available_words.word_id " +
                   "inner join Z available_words_2 on words.word_id=available_words_2.word_id " +
                   "where available_words.availability=1 and available_words_2.availability=0"
        );

        assertEquals(1, result.size());
        assertEquals("four", result.get(0).get("myWords"));
        assertEquals("new info", result.get(0).get("myInfo"));
    }

    @Test
    void update() {
        executeCommand("create table X (id integer, word varchar(255));");
        executeCommand("insert into X values (1, \"one\");");
        executeCommand("insert into X values (2, \"two\");");
        executeCommand("update X set word=\"TWO\" where word=\"two\"");
        QueryResult result = executeCommand("select X.word, X.id from X where X.id=2;");

        assertEquals(1, result.size());
        assertEquals("TWO", result.get(0).get("X.word"));

        executeCommand("update X set id=10 where id=1");
        result = executeCommand("select X.word, X.id from X where X.id=10;");

        assertEquals(1, result.size());
        assertEquals("one", result.get(0).get("X.word"));
        assertEquals(10, result.get(0).get("X.id"));
    }

    @Test
    void updateWithMoreComplexWhere() {
        executeCommand("create table X (id integer, fk_id integer, word varchar(255));");
        executeCommand("insert into X values (1, 1, \"one\");");
        executeCommand("insert into X values (2, 3, \"one\");");
        executeCommand("insert into X values (3, 3, \"one\");");
        executeCommand("update X set word=\"TWO\" where id=3 and fk_id=3");
        QueryResult result = executeCommand("select X.word, X.id, X.fk_id from X where X.id=3;");

        assertEquals(1, result.size());
        assertEquals("TWO", result.get(0).get("X.word"));
    }

    @Test
    void whereWithStrings() {
        executeCommand("create table X (word varchar(255));");
        executeCommand("insert into X values (one);");
        executeCommand("insert into X values (two);");
        executeCommand("insert into X values (three);");
        QueryResult result = executeCommand("select table.word from X table where table.word=\"two\"");

        assertEquals(1, result.size());
        assertEquals("two", result.get(0).get("table.word"));
    }

    @Test
    void whereWithDates() {
        executeCommand("create table X (date date);");
        executeCommand("insert into X values (\"27-08-2018\");");
        executeCommand("insert into X values (\"30-08-2018\");");
        executeCommand("insert into X values (\"02-09-2018\");");
        QueryResult result = executeCommand("select table.date from X table where table.date>\"29-08-2018\";");
        assertEquals(2, result.size());
        assertTrue(result.get(0).get("table.date") instanceof ZonedDateTime);
        assertEquals("2018-08-30T00:00Z[UTC]", result.get(0).get("table.date").toString());
        assertEquals("2018-09-02T00:00Z[UTC]", result.get(1).get("table.date").toString());
    }

    @Test
    void aliases() {
        executeCommand("create table X (id integer, word varchar(255));");
        executeCommand("insert into X values (1, 'one');");
        executeCommand("insert into X values (2, 'two');");
        executeCommand("insert into X values (3, 'three');");
        QueryResult result = executeCommand("select words.id as word_id, words.word as word_word from X words where words.word='two';");

        assertEquals(1, result.size());
        assertEquals(2, result.get(0).get("word_id"));
        assertEquals("two", result.get(0).get("word_word"));
    }

    @Test
    void twoAliasesForSameColumn() {
        executeCommand("create table X (id integer);");
        executeCommand("insert into X values (1);");
        QueryResult result = executeCommand("select table.id as A, table.id as B from X table;");

        assertEquals(1, result.size());
        assertEquals(1, result.get(0).get("A"));
        assertEquals(1, result.get(0).get("B"));
    }

    @Test
    void valuesWithExclamationMarks() {
        executeCommand("create table X (id varchar(255));");
        executeCommand("insert into X values (\"!!!\");");
        QueryResult result = executeCommand("select table.id from X table where table.id=\"!!!\";");

        assertEquals(1, result.size());
        assertEquals("!!!", result.get(0).get("table.id"));
    }

    @Test
    void preparedStatementWithParameters() {
        executeCommand("create table X (id integer, word varchar(255));");

        String insertQuery = "insert into X values (?, ?);";
        Queue<Object> parameters1 = new LinkedList<>();
        parameters1.add(1);
        parameters1.add("hello");
        QueryStatement statement = new QueryStatement(insertQuery, parameters1);
        executeStatement(statement);

        Queue<Object> parameters2 = new LinkedList<>();
        parameters2.add(2);
        parameters2.add("not hello");
        statement = new QueryStatement(insertQuery, parameters2);
        executeStatement(statement);

        String selectQuery = "select * from X where X.word=?;";
        Queue<Object> parameters3 = new LinkedList<>();
        parameters3.add("hello");
        statement = new QueryStatement(selectQuery, parameters3);

        QueryResult result = executeStatement(statement);

        assertEquals(1, result.size());
        assertEquals(1, result.get(0).get("id"));

        String updateQuery = "update X set word=? where id=?";
        Queue<Object> parameters4 = new LinkedList<>();
        parameters4.add("hi");
        parameters4.add(2);
        statement = new QueryStatement(updateQuery, parameters4);
        executeStatement(statement);

        selectQuery = "select * from X";
        statement = new QueryStatement(selectQuery);
        result = executeStatement(statement);

        assertEquals(2, result.size());
        assertEquals("hi", result.get(1).get("word"));
    }

    class Updater implements Runnable {

        @Override
        public void run() {
            for (int i = 1; i <= 1000; i++)
                updateValue();
        }

        private void updateValue() {
            QueryResult result = executeCommand("select * from X where id=1 for update;");
            Integer value = (Integer) result.get(0).get("value");
            String sql = "update X set value=? where id=1";
            Queue<Object> parameters = new LinkedList<>();
            parameters.add(value + 1);
            QueryStatement queryStatement = new QueryStatement(sql, parameters);
            executeStatement(queryStatement);
            executeStatement(queryStatement);
        }
    }

    @Test
    void concurrentUpdates() throws InterruptedException {
        executeCommand("create table X (id integer, value integer);");
        executeCommand("insert into X values (1, 0);");
        Thread t1 = new Thread(new Updater());
        Thread t2 = new Thread(new Updater());
        t1.start();
        t2.start();

        t1.join();
        t2.join();

        QueryResult result = executeCommand("select * from X where id=1");

        assertEquals(2000, result.get(0).get("value"));
    }

    private class BrokenUpdater implements Runnable {
        public QueryResult result;

        @Override
        public void run() {
            executeCommand("select * from X where id=1 for update;");
            // Exception here
            result = executeCommand("rollback;");
        }
    }

    @Test
    void aDyingConnectionReleasesTableLock() throws InterruptedException {
        executeCommand("create table A (id integer, value integer);");
        executeCommand("create table B (id integer, value integer);");
        executeCommand("create table C (id integer, value integer);");
        executeCommand("create table X (id integer, value integer);");
        executeCommand("create table Y (id integer, value integer);");
        executeCommand("create table Z (id integer, value integer);");
        executeCommand("insert into X values (1, 0);");
        BrokenUpdater brokenUpdater = new BrokenUpdater();
        Thread t1 = new Thread(brokenUpdater);
        t1.start();

        Thread t2 = new Thread(new Updater());
        t2.start();

        t1.join();
        t2.join();

        QueryResult result = executeCommand("select * from X where id=1");

        assertEquals(1000, result.get(0).get("value"));
        assertEquals("OK", brokenUpdater.result.status);
    }

    @Test
    void returnsCorrectValuesForNonUniqueIndexedColumn() {
        executeCommand("create table X (id integer, value integer, index id_index (id))");

        executeCommand("insert into X values (1, 200)");
        executeCommand("insert into X values (1, 100)");
        executeCommand("insert into X values (2, 100)");
        executeCommand("insert into X values (1, 200)");
        executeCommand("insert into X values (1, 100)");

        QueryResult result = executeCommand("select * from X where id=1");
        assertEquals(4, result.size());

        result = executeCommand("select * from X where value=200");
        assertEquals(2, result.size());
    }

    @Test
    void delete() {
        executeCommand("create table X (id integer, value integer)");

        executeCommand("insert into X values (1, 200)");
        executeCommand("insert into X values (2, 300)");
        executeCommand("insert into X values (3, 400)");

        executeCommand("delete from X where id=2");

        QueryResult result = executeCommand("select * from X");

        assertEquals(2, result.size());
        assertEquals(200, result.get(0).get("value"));
        assertEquals(400, result.get(1).get("value"));
    }

}

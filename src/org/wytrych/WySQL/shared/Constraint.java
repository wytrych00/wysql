package org.wytrych.WySQL.shared;

import java.io.Serializable;

public class Constraint implements Serializable {
    public enum ConstraintType {
        FOREIGN_KEY
    }

    private final ConstraintType constraintType;
    private final String name;
    private final String ownColumn;
    private final String foreignTableName;
    private final String foreignColumnName;

    public Constraint(ConstraintType constraintType, String name, String ownColumn, String foreignTableName, String foreignColumnName) {
        this.constraintType = constraintType;
        this.name = name;
        this.ownColumn = ownColumn;
        this.foreignTableName = foreignTableName;
        this.foreignColumnName = foreignColumnName;
    }

    public String getName() {
        return name;
    }

    public String getOwnColumn() {
        return ownColumn;
    }

    public String getForeignTableName() {
        return foreignTableName;
    }

    public String getForeignColumnName() {
        return foreignColumnName;
    }

    public ConstraintType getConstraintType() {
        return constraintType;
    }
}


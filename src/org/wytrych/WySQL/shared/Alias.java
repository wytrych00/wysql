package org.wytrych.WySQL.shared;

public class Alias {
    private final String name;
    private final String alias;

    public Alias(String name, String alias) {
        this.name = name;
        this.alias = alias;
    }

    public Alias(String name) {
        this.name = name;
        this.alias = name;
    }

    public String getAlias() {
        return alias;
    }

    public String getName() {
        return name;
    }
}

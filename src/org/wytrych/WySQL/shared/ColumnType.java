package org.wytrych.WySQL.shared;

import java.io.Serializable;

public class ColumnType implements Serializable {
    private final COLUMN_TYPE type;
    private final Long length;

    public ColumnType(COLUMN_TYPE type, Long length) {
        this.type = type;
        this.length = length;
    }

    public ColumnType(COLUMN_TYPE type, String length) {
        this.type = type;
        if (length.length() > 0)
            this.length = Long.valueOf(length);
        else
            this.length = type.defaultLength();
    }

    public ColumnType(COLUMN_TYPE type, Integer length) {
        this.type = type;
        this.length = length.longValue();
    }

    public ColumnType(COLUMN_TYPE type) {
        this.type = type;
        this.length = type.defaultLength();
    }

    public COLUMN_TYPE getBaseType() {
        return type;
    }

    public Long getLength() {
        return length;
    }
}

package org.wytrych.WySQL.shared;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TableWithAssignedColumns {
    public final String tableName;
    private final List<Alias> columnsAliases;

    public TableWithAssignedColumns(String tableName, List<Alias> columnsAliases) {
        this.tableName = tableName;
        this.columnsAliases = columnsAliases;
    }

    public TableWithAssignedColumns(Map.Entry<String, ArrayList<Alias>> entry) {
        this.tableName = entry.getKey();
        this.columnsAliases = entry.getValue();
    }

    public List<Alias> getColumnsWithAliases() {
        return columnsAliases;
    }
}
package org.wytrych.WySQL.shared;

public enum COLUMN_TYPE {
    INTEGER(Integer.MAX_VALUE),
    VARCHAR(100),
    BIT(1),
    DATE(20),
    BIGINT(Long.MAX_VALUE),
    PRIMARY_KEY(100);

    private final Long defaultLength;

    COLUMN_TYPE(Long defaultLength) {
        this.defaultLength = defaultLength;
    }
    COLUMN_TYPE(Integer defaultLength) {
        this.defaultLength = defaultLength.longValue();
    }

    public Long defaultLength() {
        return defaultLength;
    }
}
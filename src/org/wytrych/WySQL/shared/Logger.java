package org.wytrych.WySQL.shared;

import java.time.ZonedDateTime;

public class Logger {
    public static void log(String log) {
        System.out.println(ZonedDateTime.now() + ": " + log);
    }
}
package org.wytrych.WySQL.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Column implements Serializable {
    public final String name;
    public final ColumnType type;
    private boolean indexed;
    private boolean isPrimaryKey;
    private boolean nullable = true;
    private List<Constraint> constraints = new ArrayList<>();

    public Column(String name, ColumnType type) {
        this.name = name;
        this.type = type;
    }

    public Column(String name, ColumnType type, boolean indexed) {
        this.name = name;
        this.type = type;
        this.indexed = indexed;
    }

    public void setIndexed(boolean indexed) {
        this.indexed = indexed;
    }

    public boolean isIndexed() {
        return indexed;
    }

    public boolean isPrimary() {
        return isPrimaryKey;
    }

    public void setPrimaryKey(boolean isPrimaryKey) {
        this.isPrimaryKey = isPrimaryKey;
    }

    public boolean isNullable() {
        return nullable;
    }

    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }

    public void addConstraint(Constraint constraint) {
        constraints.add(constraint);
    }

    public List<Constraint> getConstraints() {
        return constraints;
    }
}

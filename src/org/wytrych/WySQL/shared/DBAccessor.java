package org.wytrych.WySQL.shared;

import org.wytrych.QueryResult.QueryResult;
import org.wytrych.QueryResult.QueryStatement;
import org.wytrych.WySQL.Commands.Command;
import org.wytrych.WySQL.SQLParser.ISQLParser;
import org.wytrych.WySQL.db.IDatabase;
import org.wytrych.WySQL.db.table.DuplicateEntryException;
import org.wytrych.WySQL.db.table.ForeignKeyViolationException;
import org.wytrych.WySQL.db.table.NullViolationException;
import org.wytrych.WySQL.db.table.TableException;
import org.wytrych.WySQL.QueryExceptions.ClashingColumnException;
import org.wytrych.WySQL.QueryExceptions.IncorrectColumnValueException;
import org.wytrych.WySQL.QueryExceptions.TableNotExistsException;

import static org.wytrych.QueryResult.QueryResult.QueryResultErrorCode.*;

public class DBAccessor {
    private final IDatabase db;
    private final ISQLParser parser;

    public DBAccessor(IDatabase db, ISQLParser parser) {
        this.db = db;
        this.parser = parser;
    }

    public QueryResult executeQuery(String sql) {
        return executeQuery(new QueryStatement(sql));
    }

    public QueryResult executeQuery(QueryStatement queryStatement) {
        try {
            Command command = parser.parseStatement(queryStatement);
            return command.execute(db);
        } catch (TableNotExistsException e) {
            return new QueryResult("ERROR", "No table", NO_TABLE);
        } catch (ClashingColumnException e) {
            return new QueryResult("ERROR", e.getColumn(), null);
        } catch (IncorrectColumnValueException e) {
            return new QueryResult("ERROR", e.value + " " + e.expectedType.getBaseType(), null);
        } catch (DuplicateEntryException e) {
            return new QueryResult("ERROR", "Duplicate entry", DUPLICATE_ENTRY);
        } catch (NullViolationException e) {
            return new QueryResult("ERROR", "Null violation", NULL_VIOLATION);
        } catch (ForeignKeyViolationException e) {
            return new QueryResult("ERROR", e.getConstraintName(), FOREIGN_KEY_VIOLATION);
        } catch (ISQLParser.UnsupportedCommandException e) {
            return new QueryResult("ERROR", "Unsupported command: " + e.command, null);
        } catch (TableException e) {
            return new QueryResult("ERROR", "Unknown reason", null);
        } catch (Throwable e) {
            return new QueryResult("ERROR", e.getMessage(), null);
        }
    }
}

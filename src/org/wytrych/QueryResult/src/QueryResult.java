package org.wytrych.QueryResult;

import java.io.Serializable;
import java.util.*;

public class QueryResult implements Serializable {
    private static final long serialVersionUID = 1;
    public final List<Map<String,Object>> result;
    private final Map<String, String> columnMapping;
    public final String status;
    public final String message;
    public final QueryResultErrorCode errorCode;

    public class RowAccessor implements Serializable {
        private final Map<String, Object> row;
        private final Map<String, String> columnMapping;

        RowAccessor(Map<String, Object> row, Map<String, String> columnMapping) {
            this.row = row;
            this.columnMapping = columnMapping;
        }

        public Object get(String columnName) {
            if (columnMapping.equals(Collections.emptyMap()))
                return row.get(columnName);
            return row.get(columnMapping.get(columnName));
        }

        public Object get(Integer i) {
            Object[] values = row.values().toArray();
            return values[0];
        }

        public boolean containsKey(String key) {
            return row.containsKey(columnMapping.get(key));
        }
    };

    public Map<String, String> getColumnMapping() {
        return columnMapping;
    }

    public QueryResult(List<Map<String, Object>> result, Map<String, String> columnMapping) {
        this.result = result;
        this.columnMapping = columnMapping;
        status = "OK";
        message = null;
        errorCode = null;
    }

    public QueryResult(List<Map<String, Object>> result) {
        this.result = result;
        status = "OK";
        message = null;
        errorCode = null;
        this.columnMapping = Collections.emptyMap();
    }

    public QueryResult(String status) {
        this.status = status;
        result = new ArrayList<>();
        message = null;
        errorCode = null;
        this.columnMapping = Collections.emptyMap();
    }

    public QueryResult(String status, String message, QueryResultErrorCode errorCode) {
        this.message = message;
        this.status = status;
        result = new ArrayList<>();
        this.errorCode = errorCode;
        this.columnMapping = Collections.emptyMap();
    }

    public RowAccessor get(int index) {
        Map<String, Object> row = result.get(index);
        return new RowAccessor(row, columnMapping);
    }

    public int size() {
        return result.size();
    }

    public static QueryResult cartesian(QueryResult first, QueryResult second) {
        if (first.size() == 0)
            return second;

        List<Map<String, Object>> result = new ArrayList<>();

        for (int i = 0; i < second.size(); i++) {
            Map<String, Object> rowSecond = second.result.get(i);

            for (int j = 0; j < first.size(); j++) {
                Map<String, Object> rowFirst = first.result.get(j);

                Map<String, Object> combinedRow = new HashMap<>(rowSecond);
                combinedRow.putAll(rowFirst);
                result.add(combinedRow);
            }
        }


        return new QueryResult(result);
    }

    public enum QueryResultErrorCode {
        NO_TABLE,
        DUPLICATE_ENTRY,
        NULL_VIOLATION,
        FOREIGN_KEY_VIOLATION
    }

}

package org.wytrych.QueryResult;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.Queue;

public class QueryStatement implements Serializable {
    private static final long serialVersionUID = 1;
    public final String sql;
    public final Queue<Object> parameters;

    public QueryStatement(String sql, Queue<Object> parameters) {
        this.sql = sql;
        this.parameters = parameters;
    }

    public QueryStatement(String sql) {
        this.sql = sql;
        this.parameters = new LinkedList<>();
    }
}


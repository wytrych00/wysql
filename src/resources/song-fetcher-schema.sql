create table AppearanceDate (DATE_ID integer(11) not null, date date, primary key (DATE_ID));
create table Word (WORD_ID integer(11) not null, word varchar(255), primary key (WORD_ID), index word_index (word));
create table WordDateCount (count integer(11) not null, word_WORD_ID integer(11) not null, appearanceDate_DATE_ID integer(11) not null, primary key (appearanceDate_DATE_ID,word_WORD_ID), index word_id_index (word_WORD_ID), constraint date_id_foreign_key foreign key (appearanceDate_DATE_ID) references AppearanceDate (DATE_ID), constraint word_id_foreign_key foreign key (word_WORD_ID) references Word (WORD_ID));
create table date_sequence (next_val bigint(20))
insert into date_sequence values (1)
create table word_sequence (next_val bigint(20))
insert into word_sequence values (1)

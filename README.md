# WySQL

A limited, in-memory only implementation of a SQL database using MySQL syntax.

### Features:

* `SELECT FROM`
 * table aliases
 * column aliases
 * star selector
 * multiple tables
 * using indexes for faster table lookup
 * `INNER JOIN`
 * `WHERE`
 * `FOR UPDATE` - always locks whole table and unlocks after the next `UPDATE` by the same thread
* `CREATE TABLE` 
* Data types:
 * `integer`
 * `bigint`
 * `varchar(xxx)`
 * `bit`
 * `date`
* `INSERT INTO`
 * all values of table
 * only some values
 * primary key constraints
 * foreign key constraints
 * composite primary keys
 * not null
* `DROP TABLE`
* `UPDATE`
* `DELETE`
* `DUMP` - dumps serialized database to specified file

There is table locking implemented for `SELECT ... FOR UPDATE` clause, which is released after a subsequent updated by the same connection.
However, there is no transaction system in place, so the database works always as if `AUTOCOMMIT` was turned on. `ROLLBACK` is used only to remove locks
from tables in cases where there has not been an update.

To start up a server instance run `org.wytrych.WySQL.WyServer.main`. It will listen on port 9009 for connections coming from `org.wytrych.client.WySQLClient`.

The aim of this project wasn't to create a production ready database - it was to learn the basic internal mechanics, syntax parsing, indexing
and to make it work with a small project using Hibernate ORM. It is written mostly with pure TDD, and has a 95% test coverage.
